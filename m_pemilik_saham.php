<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen Anggota <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data Anggota.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master Anggota</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Anggota</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah Anggota Baru
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Anggota Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>No. NIK/NIP/NRP</label>
							  <input name="pemilik_saham_id" id="pemilik_saham_id" type="hidden">
							  <input name="pemilik_saham_nik" id="pemilik_saham_ktp_nomor" type="text" class="form-control" placeholder="Masukkan Nomor NIK/NIP/NRP">
							</div>
							<div class="form-group">
							  <label>Nama Lengkap (sesuai KTP)</label>
							  <input name="pemilik_saham_nama" id="pemilik_saham_nama" type="text" class="form-control" placeholder="Masukkan Nama pemilik_saham">
							</div>	
							<div class="form-group">
							  <label>Alamat</label>
							  <input name="pemilik_saham_alamat" id="pemilik_saham_alamat" type="text" class="form-control" placeholder="Masukkan Alamat">
							</div>	
							<div class="row">						
								<div class="form-group col-md-6">
								<label>Propinsi</label>
								<select name="pemilik_saham_propinsi" id="pemilik_saham_propinsi" class="form-control select2" placeholder="Pilih Propinsi" style="width: 100%;">
									<?php
										$qs = "SELECT * FROM d_provinces";
										$sqls = mysqli_query($conn_db,$qs);
										while($r = mysqli_fetch_array($sqls)){
									?>
										<option value="<?php echo $r['id'];?>"><?php echo $r['name'];?></option>
									<?php
										}
									?>
								</select>
								</div>
								<div class="form-group col-md-6">
								<label>Kabupaten</label>
								<select name="pemilik_saham_kabupaten" id="pemilik_saham_kabupaten" class="form-control select2" style="width: 100%;">
									
								</select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								<label>Kecamatan</label>
								<select name="pemilik_saham_kecamatan" id="pemilik_saham_kecamatan" class="form-control select2" style="width: 100%;">
									
								</select>
								</div>
								<div class="form-group  col-md-6">
								<label>Kelurahan/Desa</label>
								<input name="pemilik_saham_kelurahan" id="pemilik_saham_kelurahan" type="text" class="form-control" placeholder="Masukkan Kelurahan/Desa">
								</div>	
							</div>
							<div class="form-group">
							  <label>HP</label>
							  <input name="pemilik_saham_hp" id="pemilik_saham_hp" type="text" class="form-control" placeholder="Masukkan HP/WA">
							</div>						
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="modal fade" id="modal-genuser" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-genuser" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Generate User</h4>
						</div>
						<div class="modal-body">							
							<div class="form-group">
							  <label>Nama Lengkap (sesuai KTP)</label>
							  <input name="member_id" id="member_id" type="hidden">
							  <input name="mob_fullname" id="mob_fullname" type="text" class="form-control" placeholder="Masukkan Nama pemilik_saham">
							</div>								
							<div class="form-group">
							  <label>HP</label>
							  <input name="mob_hp" id="mob_hp" type="text" class="form-control" placeholder="Masukkan HP/WA">
							</div>	
							<div class="form-group">
							  <label>Password default sama dengan nomor HP, silahkan update melalui aplikasi mobile KIB (Kopsyah Ikhlas Beramal)</label>
							</div>					
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_gen_submit"><i class='fa fa-gear'></i> Generate </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>

			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>No. KTP</th>
							<th width="150">Nama</th>
							<th>Kecamatan</th>
							<th width="200">Alamat</th>	
							<th width="200">HP/WA</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Anggota pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		
		function loadKab(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();
			if(data_id){		
				return $.ajax({
					type: "POST",
					url: "m_pemilik_saham_aksi.php?act=load_kab",
					data: { 'data_id': data_id  },
					dataType: "json",
					success: function(data){
						if(data.msg=="OK"){
							// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
							for(var i = 0; i < data.record.length; i++) {
								$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
							}						
						}else{
							alert_custom(data.msg,"modal-danger");
						}
					}
				});
			}
		}
		
		function loadKec(data_id, wrap_out)
		{				
			$('#'+wrap_out).empty();	
			if(data_id){		
				return $.ajax({
					type: "POST",
					url: "m_pemilik_saham_aksi.php?act=load_kec",
					data: { 'data_id': data_id  },
					dataType: "json",
					success: function(data){
						if(data.msg=="OK"){
							// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
							for(var i = 0; i < data.record.length; i++) {
								$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
							}
						}else{
							alert_custom(data.msg,"modal-danger");
						}						
					}
				});
			}
		}
		
		function initial_select(){					
			var prop = $('#pemilik_saham_propinsi').val();			
			
			$.when(loadKab(prop,"pemilik_saham_kabupaten")).done(function(){
				var kab = $('#pemilik_saham_kabupaten').val();
				loadKec(kab, "pemilik_saham_kecamatan");
			});
		}
						
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{"bSearchable": true}	/* NIP */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{"sClass": "center"} /* Usergroup */ 	 
				,{"sClass": "center"} /* Usergroup */ 	 
							  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "m_pemilik_saham_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
			
		$(document).ready(function() { 		
			
			$('#pemilik_saham_propinsi').on('select2:select', function (e) {
				var data_id = $(this).val();				
				return $.when(
					loadKab(data_id,"pemilik_saham_kabupaten")
				).done(function(){				
					loadKec($("#pemilik_saham_kabupaten").val(),"pemilik_saham_kecamatan");				
				});
			});
			
			$('#pemilik_saham_kabupaten').on('select2:select', function (e) {
				var data_id = $(this).val();
				return loadKec(data_id,"pemilik_saham_kecamatan");
			});
			
			initial_select();			
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_pemilik_saham_aksi.php?act=delete", { a: id}
						,function( data ){
							alert(data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});

			$( "#sample_3 tbody" ).on( "click",".jq-generate", function(e) {
				var id = $(this).attr("lang");
				$.post( "m_pemilik_saham_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-genuser").modal("show");
							$("#member_id").val(data.record.pemilik_saham_id);
							$("#mob_fullname").val(data.record.pemilik_saham_nama);
							$("#mob_hp").val(data.record.pemilik_saham_hp);
						}else{
							alert_custom(data.msg,"modal-info");
						}	
					},"json");		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "m_pemilik_saham_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							
							$("#pemilik_saham_id").val(data.record.pemilik_saham_id);
							$("#pemilik_saham_nama").val(data.record.pemilik_saham_nama);
							$("#pemilik_saham_agama").val(data.record.pemilik_saham_agama);
							$("#pemilik_saham_ktp_nomor").val(data.record.pemilik_saham_ktp_nomor);
							$("#pemilik_saham_alamat").val(data.record.pemilik_saham_alamat);						
							$("#pemilik_saham_hp").val(data.record.pemilik_saham_hp);	
							$("#pemilik_saham_propinsi").val(data.record.pemilik_saham_propinsi);
							$('#pemilik_saham_propinsi').select2().trigger('change');
							
							$.when(
								loadKab(data.record.pemilik_saham_propinsi,"pemilik_saham_kabupaten")
							).done(function(){
								$("#pemilik_saham_kabupaten").val(data.record.pemilik_saham_kabupaten);
								$('#pemilik_saham_kabupaten').select2().trigger('change');
								$.when(									
									loadKec(data.record.pemilik_saham_kabupaten,"pemilik_saham_kecamatan")
								).done(function(){
									$("#pemilik_saham_kecamatan").val(data.record.pemilik_saham_kecamatan);
									$('#pemilik_saham_kecamatan').select2().trigger('change');
								});
							});							
																												
							$("#pemilik_saham_kelurahan").val(data.record.pemilik_saham_kelurahan);
				
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
			
			var frm = $('#f-add');			
			frm.submit(function (e) {
				e.preventDefault();				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});				
				alert_custom("Loading Process","modal-info");	
				$.post("m_pemilik_saham_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");							
			});
			
			var frm2 = $('#f-genuser');			
			frm2.submit(function (e) {
				e.preventDefault();				
				var dataform = frm2.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});				
				alert_custom("Loading Process","modal-info");	
				$.post("m_pemilik_saham_aksi.php?act=genuser", dataform, function(data){
					$("#modal-info").modal("hide");					
					if(data.success===1){
						alert_custom(data.msg,"modal-success");
					}
					oTable.ajax.reload();
				},"json");							
			});

			function clear_form(){
				$("#pemilik_saham_id").val("");
				$("#pemilik_saham_nama").val("");
				$("#pemilik_saham_agama").val("");
				$("#pemilik_saham_ktp_nomor").val("");
				$("#pemilik_saham_alamat").val("");
				$("#pemilik_saham_kelurahan").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			$("#btn_gen_submit").click(function(e){
				$("#modal-genuser").modal("hide");							
				frm2.submit();		
				oTable.ajax.reload();
			});
		});
	</script>