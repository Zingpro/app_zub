<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];
	
	$sql = "SELECT * FROM s_user WHERE USER_ID = '".$user_id."'";
	$q = mysql_query($sql);
	$r = mysql_fetch_array($q);
	
	$id_lokasi = $r['USER_POINTING_GPS'];
	$sql_lokasi = "SELECT * FROM m_lokasi WHERE lokasi_id = '".$id_lokasi."'";
	$q_lokasi = mysql_query($sql_lokasi);
	$r_lokasi = mysql_fetch_array($q_lokasi);
	
	$lokasi_lat = $r_lokasi['lokasi_lat'];
	$lokasi_lng = $r_lokasi['lokasi_lng'];
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Absensi Mobile : <?php //echo $r['USER_FULLNAME'];?>
        <small>Pastikan GPS Smartphone Anda Diaktifkan dengan mode tingkat presisi yang tinggi.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Absensi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Absensi2</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
		<style>
			#map
			{
				height: 200px !important;
				width: 100% !important;
				margin: 10px auto;
			}
		</style>
		
        <div class="box-body">
			<div class="row">
				<div class="col-md-4">
					<center>
						<video id="player" width="100%" controls autoplay >
						</video>	
					</center>
					<button id="capture" type="button" class="btn btn-block btn-primary btn-lg">
						<i class="fa fa-camera"></i> Capture
					</button>
					<center>
						<canvas id="canvas" width="150" height="210" >
						</canvas>
						<div id="imgs">
        
						</div>
					</center>
					
					<img src="" id="screenshot"/>
					
					<div id="output" width="100%">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div id="map">
					</div>
					<button id="myLoc" type="button" class="btn btn-block btn-primary btn-lg">
						<i class="fa fa-sign-in"></i> Absen Masuk
					</button>
					<button id="absen_pulang" type="button" class="btn btn-block btn-primary btn-lg">
						<i class="fa fa-sign-out"></i> Absen Pulang
					</button>
					<input type="hidden" id="lat_destination" name="lat_destination" value="<?php echo $lokasi_lat; ?>" disabled />
					<input type="hidden" id="lng_destination" name="lng_destination" value="<?php echo $lokasi_lng; ?>" disabled />
					<input type="hidden" id="lat_send" name="lat_send" disabled />
					<input type="hidden" id="lang_send" name="lang_send" disabled />
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['USER_ID']; ?>" disabled />
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<button id="btn_logout" type="button" class="btn btn-block btn-warning btn-lg">
						<i class="fa fa-power-off text-red"></i> Logout
					</button>
				</div>			
			</div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Absensi Akan dikirimkan jika pointing lokasi sesuai
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    	
	<script src="main_absensi.min.js"></script>	
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnYAUXXlgmPkHYfjrKQnVnDfnHBV7aCHA&callback=initMap"
    async defer>
	</script>  
	
	