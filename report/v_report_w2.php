<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Akta Ikrar Wakaf</title>
<?php
	error_reporting(E_ALL); 
	session_start();
    include '../config/koneksi.php';    
    include '../config/all_function.php';    
    
	$pejabat_nama = $_SESSION['user_an_pejabat'];
	
	$id = @$_GET['id'];
		
	$tanggal_surat = tanggal_indo(date('Y-m-d'));
	$bulan_surat = date('m');
	$tahun_surat = date('Y');
	
	
	$q = "SELECT
		m_wakaf.wakaf_id,
		m_wakaf.wakaf_niw,
		m_wakaf.wakaf_obyek,
		m_wakaf.wakaf_sertifikat,
		m_wakaf.wakaf_alamat,
		m_wakaf.wakaf_kecamatan,
		m_wakaf.wakaf_kelurahan,
		m_wakaf.wakaf_kabupaten,
		m_wakaf.wakaf_propinsi,
		m_wakaf.wakaf_lahan_luas,
		m_wakaf.wakaf_lahan_lebar,
		m_wakaf.wakaf_lahan_panjang,
		m_wakaf.wakaf_batas_timur,
		m_wakaf.wakaf_batas_barat,
		m_wakaf.wakaf_batas_selatan,
		m_wakaf.wakaf_batas_utara,
		m_wakaf.wakaf_keperluan,
		m_wakaf.wakaf_wakif_id,
		m_wakaf.wakaf_nazhir_id,
		m_wakaf.wakaf_saksi_id_1,
		m_wakaf.wakaf_saksi_id_2,
		m_wakaf.wakaf_created_date,
		m_wakaf.wakaf_created_by,
		d_districts.`name` AS wakaf_kecamatan_nama,
		d_regencies.`name` AS wakaf_kabupaten_nama,
		d_provinces.`name` AS wakaf_propinsi_nama,
		m_nazhir.nazhir_id,
		m_nazhir.nazhir_nama,
		m_nazhir.nazhir_agama,
		m_nazhir.nazhir_pekerjaan,
		m_nazhir.nazhir_tanggal_lahir,
		m_nazhir.nazhir_tempat_lahir,
		m_nazhir.nazhir_jabatan,
		m_nazhir.nazhir_kewarganegaraan,
		m_nazhir.nazhir_alamat,
		m_nazhir.nazhir_nik,
		m_nazhir.nazhir_propinsi,
		m_nazhir.nazhir_kabupaten,
		m_nazhir.nazhir_kecamatan,
		m_nazhir.nazhir_kelurahan,
		m_nazhir.nazhir_created_by,
		m_nazhir.nazhir_created_date,
		m_wakif.wakif_id,
		m_wakif.wakif_nama,
		m_wakif.wakif_agama,
		m_wakif.wakif_ktp_nomor,
		m_wakif.wakif_pekerjaan,
		m_wakif.wakif_jabatan,
		m_wakif.wakif_alamat,
		m_wakif.wakif_tempat_lahir,
		m_wakif.wakif_tanggal_lahir,
		m_wakif.wakif_propinsi,
		m_wakif.wakif_kabupaten,
		m_wakif.wakif_kecamatan,
		m_wakif.wakif_kelurahan,
		m_wakif.wakif_created_by,
		m_wakif.wakif_created_date,
		m_wakif.wakif_update_by,
		m_wakif.wakif_update_date,
		m_saksi.saksi_id,
		m_saksi.saksi_nik,
		m_saksi.saksi_nama,
		m_saksi.saksi_agama,
		m_saksi.saksi_tanggal_lahir,
		m_saksi.saksi_tempat_lahir,
		m_saksi.saksi_pekerjaan,
		m_saksi.saksi_jabatan,
		m_saksi.saksi_kewarganegaraan,
		m_saksi.saksi_propinsi,
		m_saksi.saksi_kabupaten,
		m_saksi.saksi_kecamatan,
		m_saksi.saksi_kelurahan,
		m_saksi.saksi_alamat,
		m_saksi.saksi_telp,
		m_saksi.saksi_created_by,
		m_saksi.saksi_created_date
		FROM
		m_wakaf
		LEFT JOIN d_districts ON d_districts.id = m_wakaf.wakaf_kecamatan
		LEFT JOIN d_regencies ON d_districts.regency_id = d_regencies.id
		LEFT JOIN d_provinces ON d_regencies.province_id = d_provinces.id
		LEFT JOIN m_nazhir ON m_nazhir.nazhir_id = m_wakaf.wakaf_nazhir_id
		LEFT JOIN m_wakif ON m_wakif.wakif_id = m_wakaf.wakaf_wakif_id
		INNER JOIN m_saksi ON m_saksi.saksi_id = m_wakaf.wakaf_saksi_id_1 
 WHERE wakaf_id='".$id."'";
	$sql = mysql_query($q);
	$r = mysql_fetch_array($sql);
?>
<style type="text/css" media="screen,print">
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:black;
		color:white;
		font:normal 8pt/100% Arial,tahoma,sans-serif;
	}
	div.page{
		background-color:white;
		color:black;
		/*min-height:10cm;*/
		margin:0.5cm auto;
		padding: 0.5cm 1.5cm 1cm 1.5cm;
		width:22.5cm;		
	}
	div.header{
		background-color:white;
		/*border-bottom:3px solid black;*/
		padding-bottom:.3cm;
		text-align:center;
		color:black;
	}
	div.title{
		text-align:center;
		font-size:13pt;
		font-weight:bold;
		line-height:120%;
		padding-top:0.2cm;
	}
	div.header div.h1{
		font-size:18pt;
		font-family:arial;
		font-weight:bold;
		line-height:18pt;	
	}
	div.header div.h2{
		font-size:16pt;	
		font-family:arial;
		font-weight:bold;
		line-height:16pt;
		text-decoration:underline;
		margin:3px;
	}
	div.header div.h3{
		font-family:arial;
		font-size:12pt;	
		line-height:12pt;	
	}
	div.ttd_left{
		float:left;
		text-align:center;
		font-size:14px;
		font-family:arial;
		line-height:14pt;
		width:400px;
	}
	div.ttd_right{
		float:right;
		text-align:center;
		font-size:14px;
		font-family:arial;
		line-height:14pt;
		width:400px;
	}
	div.content{
		font-family:arial;
		margin-top:.4cm;
		font-size:14px;
		line-height:14pt;	
	}
	div.content p{
		margin-top:.3cm;
		text-align:justify;
	}

	table.data{	
		border-collapse:collapse;
		width:100%;
	}
	table.data th{
		padding:3px;
		border:1px solid black;
		font-size:small;
	}
	table.data td{
		padding:2px 2px 2px 3px;		
	}
	span.pilihan{
		border-bottom:1px dotted black;
	}

	footer {
		page-break-after: always;
	}

</style>

</head>

<body>
	<div class="page">
	
		<div class="header" style="margin-top:10px"> 
			<div style="float:right">
				Bentuk W.2
			</div>
			<div style="clear:both;"></div>
			<center>
				<img src="../assets/img/logo-kemenag.png" alt="Kemenag" width="75"/>
			</center>
			<!--
			<div class="h1">KEMENTERIAN AGAMA REPUBLIK INDONESIA</div>			
			-->
			<br/>
			
			<div class="h2">AKTA IKRAR WAKAF</div>
			<div class="h3">Nomor : .... /V/02 Tahun : 2019</div>
		</div>
		<?php 
			$tgl_indo = tanggal_indo(date('Y-m-d'));
			//echo $tgl_indo;
		?>
		
		<div class="content">
			<div>
				<p>&ensp;&ensp;&ensp;&ensp;Pada hari ini, hari <?php echo hari_ini(); ?> tanggal <?php echo $tgl_indo; ?> datang menghadap kepada kami, nama .... Kepala Kantor Urusan Agama Kecamatan <?php echo $r['wakaf_kecamatan_nama'];?> 
				Kabupaten/Kota <?php echo $r['wakaf_kabupaten_nama'];?> yang oleh Menteri Agama dengan peraturannya Nomor 1 Tahun 1978 pasal 5 ayat (1) ditunjuk sebagai Pejabat Pembuat Akta Ikrar Wakaf
				 yang dimaksudkan dalam pasal 9 ayat (1) Peraturan Pemerintah No. 28 Tahun 1977 tentang Perwakafan Tanah Milik, untuk wilayah Kecamatan <?php echo $r['wakaf_kecamatan_nama'];?> dengan dihadiri dan disaksikan 
				 oleh saksi-saksi yang kami kenal/diperkenalkan kepada kami dan Nazhir yang kami kenal/diperkenalkan kepada kami dan akan disebutkan dalam akta ini.
				</p>
				<p>	
					<table style="margin-left:30px">
						<tr><td>I.&nbsp;&nbsp;</td><td>Nama Lengkap</td><td>: <?php echo $r['nazhir_nama'];?></td></tr>
						<tr><td>&nbsp;</td><td>Tempat, Tanggal Lahir</td><td>: <?php echo $r['nazhir_tempat_lahir'].", ".$r['nazhir_tanggal_lahir'];?></td></tr>	
						<tr><td>&nbsp;</td><td>Agama</td><td>: <?php echo $r['nazhir_agama'];?></td></tr>
						<tr><td>&nbsp;</td><td>Pekerjaan</td><td>: <?php echo $r['nazhir_pekerjaan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Jabatan (bagi wakif badan hukum)</td><td>: <?php echo $r['nazhir_jabatan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Kewarganegaraan</td><td>: <?php echo $r['nazhir_kewarganegaraan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Tempat Tinggal</td><td>: <?php echo $r['nazhir_alamat'];?></td></tr>
					</table>					
				</p>
				<p>&ensp;&ensp;&ensp;&ensp;Selanjutnya disebut WAKIF</p>
				<p>	
					<table style="margin-left:30px">
						<tr><td>II.&nbsp;&nbsp;</td><td>Nama Lengkap</td><td>: <?php echo $r['wakif_nama'];?></td></tr>
						<tr><td>&nbsp;</td><td>Tempat, Tanggal Lahir</td><td>: <?php echo $r['wakif_tempat_lahir'].", ".$r['wakif_tanggal_lahir'];?></td></tr>	
						<tr><td>&nbsp;</td><td>Agama</td><td>: <?php echo $r['wakif_agama'];?></td></tr>
						<tr><td>&nbsp;</td><td>Pekerjaan</td><td>: <?php echo $r['wakif_pekerjaan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Jabatan (bagi wakif badan hukum)</td><td>: <?php echo $r['wakif_jabatan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Kewarganegaraan</td><td>: <?php echo $r['wakif_kewarganegaraan'];?></td></tr>
						<tr><td>&nbsp;</td><td>Tempat Tinggal</td><td>: <?php echo $r['wakif_alamat'];?></td></tr>
					</table>					
				</p>
				<p>&ensp;&ensp;&ensp;&ensp;Selanjutnya disebut NAZHIR</p>
				<p>Menerangkan bahwa Wakif telah mengikrarkan wakaf kepada Nazhir atas sebidang tanah hak miliknya.</p>
				<p>	
					<table style="margin-left:50px">
						<tr><td colspan="2">Berupa</td><td>: <?php echo $r['wakaf_obyek'];?></td></tr>
						<tr><td colspan="2">Sertifikat/Persil Nomor</td><td>: <?php echo $r['wakaf_sertifikat'];?></td></tr>	
						<tr><td colspan="2">Kelas Desa</td><td>: <?php echo $r['wakaf_kelurahan'];?></td></tr>
						<tr><td>Ukuran</td><td>Panjang</td><td>: <?php echo $r['wakaf_lahan_panjang'];?></td></tr>
						<tr><td>&nbsp;</td><td>Lebar</td><td>: <?php echo $r['wakaf_lahan_lebar'];?></td></tr>
						<tr><td>&nbsp;</td><td>Luas</td><td>: <?php echo $r['wakaf_lahan_luas'];?></td></tr>
						<tr><td colspan="2">Terletak di</td><td>: <?php echo $r['wakaf_alamat'];?></td></tr>
						<tr><td colspan="2">Kelurahan/Desa</td><td>: <?php echo $r['wakaf_kelurahan'];?></td></tr>
						<tr><td colspan="2">Kecamatan</td><td>: <?php echo $r['wakaf_kecamatan_nama'];?></td></tr>
						<tr><td colspan="2">Kabupaten/Kota</td><td>: <?php echo $r['wakaf_kabupaten_nama'];?></td></tr>
						<tr><td colspan="2">Propinsi</td><td>: <?php echo $r['wakaf_propinsi_nama'];?></td></tr>
						<tr><td colspan="4">Dengan batas-batas :</td></tr>
						<tr><td>Sebelah</td><td>Timur</td><td>: <?php echo $r['wakaf_batas_timur'];?></td></tr>
						<tr><td>&nbsp;</td><td>Barat</td><td>: <?php echo $r['wakaf_batas_barat'];?></td></tr>
						<tr><td>&nbsp;</td><td>Utara</td><td>: <?php echo $r['wakaf_batas_utara'];?></td></tr>
						<tr><td>&nbsp;</td><td>Selatan</td><td>: <?php echo $r['wakaf_batas_selatan'];?></td></tr>
						<tr><td colspan="2">Untuk Keperluan</td><td>: <?php echo $r['wakaf_keperluan'];?></td></tr>
					</table>					
				</p>
				<p>dengan dihadiri dan disaksikan oleh :</p>
				<?php
					$saksi_id_2 = $r['wakaf_saksi_id_2'];
					$qs = "SELECT * FROM m_saksi WHERE saksi_id = '".$saksi_id_2."'";
					$sqls = mysql_query($qs);
					//echo $qs;
					$rs = mysql_fetch_array($sqls);
				?>
				<p>	
					<table style="margin-left:30px">
						<tr><td>III.&nbsp;&nbsp;</td><td>1.&nbsp;</td><td>Nama Lengkap</td><td>: <?php echo $r['saksi_nama'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Tanggal Lahir / Umur</td><td>: <?php echo $r['saksi_tanggal_lahir'];?></td></tr>	
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Agama</td><td>: <?php echo $r['saksi_agama'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Pekerjaan</td><td>: <?php echo $r['saksi_pekerjaan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Jabatan</td><td>: <?php echo $r['saksi_jabatan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Kewarganegaraan</td><td>: <?php echo $r['saksi_kewarganegaraan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Tempat Tinggal</td><td>: <?php echo $r['saksi_alamat'];?></td></tr>
						
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
						
						<tr><td>&nbsp;&nbsp;</td><td>2.&nbsp;</td><td>Nama Lengkap</td><td>: <?php echo $rs['saksi_nama'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Tanggal Lahir / Umur</td><td>: <?php echo $rs['saksi_tanggal_lahir'];?></td></tr>	
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Agama</td><td>: <?php echo $rs['saksi_agama'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Pekerjaan</td><td>: <?php echo $rs['saksi_pekerjaan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Jabatan</td><td>: <?php echo $rs['saksi_jabatan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Kewarganegaraan</td><td>: <?php echo $rs['saksi_kewarganegaraan'];?></td></tr>
						<tr><td>&nbsp;</td><td>&nbsp;</td><td>Tempat Tinggal</td><td>: <?php echo $rs['saksi_alamat'];?></td></tr>
						
					</table>					
				</p>				
			</div>
		</div>
		<footer></footer>
		
		<div class="header" style="margin-top:10px"> 
			<br/>&nbsp;
			<br/>&nbsp;
		</div>
		<div class="content">
			<p>
				Salinan Akta Ikrar Wakaf ini dibuat rangkap 7 (tujuh) :
				<ul style="margin-left:30px;">
					<li>Lembar pertama untuk wakif.</li>
					<li>Lembar kedua untuk nazhir.</li>
					<li>Lembar ketiga untuk mauquf 'alaih.</li>
					<li>Lembar keempat untuk Kepala Kantor Kementerian Agama Kabupaten/Kota.</li>
					<li>Lembar kelima untuk Kantor Pertanahan Kabupaten/Kota dalam hal benda wakaf berupa tanah.</li>
					<li>Lembar keenam Badan Wakaf Indonesia dan,</li>
					<li>Lembar ketujuh untuk instansi berwenang.</li>
				</ul>
			</p>
			<p>&ensp;&ensp;&ensp;&ensp;</p>
		</div>
        <div class="ttd_left">			
			<div>
				<br/>Wakif<br/>
			</div>
			<div style="margin-top:25px;">
				<br/><br/>
				<br/><br/>
				<span style="text-decoration:underline;"><?php echo $r['wakif_nama'];?></span>
			</div>
		</div>       
		<div class="ttd_right">			
			<div>
				Kepala Kantor Urusan Agama Kecamatan/<br/>
				Pejabat Pembuat Akta Ikrar Wakaf,
				<br/><br/>[Materai 6.000,-]
			</div>
			<div style="margin-top:25px;">
				<br/><br/>
				<span style="text-decoration:underline;"><?php echo $pejabat_nama;?></span>
			</div>
		</div>
		<div style="clear:both;">&nbsp;</div>
		<div style="clear:both;">&nbsp;</div>
		<div style="clear:both;">&nbsp;</div>
		<div class="ttd_left">			
			<div>
				<br/>Nazhir<br/>
			</div>
			<div style="margin-top:30px;">
				<br/><br/>
				<span style="text-decoration:underline;"><?php echo $r['nazhir_nama'];?></span>
			</div>
		</div> 
		<div class="ttd_right">			
			<div>
				<br/>Saksi-saksi :<br/>
			</div>
			<div style="margin-top:30px;">
				<br/><br/>
				1. <span style="text-decoration:underline;"><?php echo $r['saksi_nama'];?></span>
			</div>
			<div style="margin-top:30px;">
				<br/><br/>
				2. <span style="text-decoration:underline;"><?php echo $rs['saksi_nama'];?></span>
			</div>
		</div>
		<div style="clear:both;">&nbsp;</div>
		<br/>		
		<br/>
        <div class="content">
			<p>Keterangan : <br/>
				<ol>
				<li>Diisi salah satu dari sawah, pekarangan, kebun, atau tambak.</li>			
				<li>Coret yang tidak perlu.</li>
				<li>diisi salah satu dari tujuan wakaf,<br/>
				a. Pembangunan tempat peribadatan, termasuk didalamnya masjid, langgar dan musholla.<br/>
				b. Keperluan umum, termasuk didalamnya bidang pendidikan dari tingkat kanak-kanak, tingkat dasar sampai tingkat tinggi serta tempat penyantunan anak yatim piatu, tuna netra, tuna wisma atau keperluan umum lainnya sesuai dengan ajaran agama Islam.<br/>
				</li>
				</ol>
			</p>
		</div>
		<br/>
		<br/>	
		
		<table>
			<tr>
			<td align="center" border="1px solid">
				<?php			
					include("../plugins/phpqrcode/phpqrcode.php");
					QRcode::png('http://hajibangkalan.xyz/?code=Uji Coba','QR_gen.png');
				?>
				<br/>
				<img alt="Uji Coba" src="QR_gen.png"/>
			</td>
			</tr>
		</table>
		
		<div style="clear:both;"></div>
		
		<footer></footer>
		
	</div>
	
	
</body>
</html>