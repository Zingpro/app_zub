<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Surat Pengesahan Nazhir</title>
<?php
	session_start();
	error_reporting(E_ALL); 
    include '../config/koneksi.php';    
    include '../config/all_function.php';    
    
	$id = @$_GET['id'];
		
	$tanggal_surat = tanggal_indo(date('Y-m-d'));
	$bulan_surat = date('m');
	$tahun_surat = date('Y');
	
	
	$user_id = $_SESSION['USER_ID'];
	
	$q = "SELECT * FROM v_user WHERE USER_ID = '".$user_id."'";
	$sql = mysql_query($q);
	$r_user = mysql_fetch_array($sql);
	
	$q = "SELECT * FROM m_yayasan WHERE yayasan_id='".$id."'";
	$sql = mysql_query($q);
	$r = mysql_fetch_array($sql);
?>
<style type="text/css" media="screen,print">
	*{
		margin:0;
		padding:0;
	}
	body{
		background-color:black;
		color:white;
		font:normal 8pt/100% Arial,tahoma,sans-serif;
	}
	div.page{
		background-color:white;
		color:black;
		/*min-height:10cm;*/
		margin:0.5cm auto;
		padding: 0.5cm 1.5cm 1cm 1.5cm;
		width:22.5cm;		
	}
	div.header{
		background-color:white;
		/*border-bottom:3px solid black;*/
		padding-bottom:.3cm;
		text-align:center;
		color:black;
	}
	div.title{
		text-align:center;
		font-size:13pt;
		font-weight:bold;
		line-height:120%;
		padding-top:0.2cm;
	}
	div.header div.h1{
		font-size:18pt;
		font-family:arial;
		font-weight:bold;
		line-height:18pt;	
	}
	div.header div.h2{
		font-size:16pt;	
		font-family:arial;
		font-weight:bold;
		line-height:16pt;
		text-decoration:underline;
		margin:3px;
	}
	div.header div.h3{
		font-family:arial;
		font-size:12pt;	
		line-height:12pt;	
	}
	div.ttd_left{
		float:left;
		text-align:center;
		font-size:14px;
		font-family:arial;
		line-height:14pt;
		width:400px;
	}
	div.ttd_right{
		float:right;
		font-size:14px;
		font-family:arial;
		line-height:14pt;
		width:400px;
	}
	div.content{
		font-family:arial;
		margin-top:.4cm;
		font-size:14px;
		line-height:14pt;	
	}
	div.content p{
		margin-top:.3cm;
		text-align:justify;
	}

	table.data{	
		border-collapse:collapse;
		width:100%;
	}
	table.data th{
		padding:3px;
		border:1px solid black;
		font-size:small;
	}
	table.data td{
		padding:2px 2px 2px 3px;		
	}
	span.pilihan{
		border-bottom:1px dotted black;
	}

	footer {
		page-break-after: always;
	}

</style>

</head>

<body>
	<div class="page">
	
		<div class="header" style="margin-top:10px"> 
			<div style="float:right">
				Bentuk W.5a
			</div>
			<div style="clear:both;"></div>
			<center>
				<img src="../assets/img/logo-kemenag.png" alt="Kemenag" width="75"/>
			</center>
			<!--
			<div class="h1">KEMENTERIAN AGAMA REPUBLIK INDONESIA</div>			
			-->
			<br/>
			
			<div class="h2">Surat Pengesahan Nazhir</div>
			<div class="h3">Nomor : B - .... /Kk.13.20.17/W.5a/05/2019</div>
		</div>
		<?php 
			$tgl_indo = tanggal_indo(date('Y-m-d'));
			//echo $tgl_indo;
		?>
		
		<div class="content">
			<div>
				<p>&ensp;&ensp;&ensp;&ensp;Pada hari ini, hari <?php echo hari_ini(); ?> tanggal <?php echo $tgl_indo; ?>, Kami Kepala Kantor Urusan Agama Kecamatan <?php echo $r['user_kecamatan_nama'];?> 
				/ Pejabat Pembuat Akta Ikrar Wakaf Wilayah Kecamatan <?php echo $r_user['user_kecamatan_nama']; ?> Kabupaten/Kota <?php echo $r_user['user_kabupaten_nama']; ?>, setelah mengadakan penelitian seperlunya, mengesahkan :
				</p>
				<p>	
					<table style="margin-left:30px">
						<tr><td>1. </td><td>Nama Badan Hukum</td><td>: <?php echo $r['yayasan_nama'];?></td></tr>
						<tr><td>2. </td><td>Badan Hukum Akta Notaris</td><td>: <?php echo $r['yayasan_akta_notaris'];?></td></tr>	
						<tr><td>3. </td><td>Pimpinan Pusat Berkedudukan di</td><td>: <?php echo $r['yayasan_alamat'];?></td></tr>
						<tr><td>4. </td><td>Cabang/Ranting/Pusat</td><td>: <?php echo $r['yayasan_tipe'];?></td></tr>
						<tr><td>5. </td><td>Pengurus sekarang </td><td>:</td></tr>
						<?php
							$q = "SELECT * FROM v_pengurus WHERE pengurus_yayasan_id = '".$r['yayasan_id']."'";
							$sql = mysql_query($q);
							while($r_pengurus = mysql_fetch_array($sql)):
						?>
						<tr><td>&nbsp;</td><td><?php echo $r_pengurus['pengurus_jabatan'];?></td><td>: <?php echo $r_pengurus['nazhir_nama'];?></td></tr>						
						<?php
							endwhile;
						?>
					</table>					
				</p>
				<p>	
					Kegiatan Organisasi antara lain :
					<table style="margin-left:30px">
						<tr><td>1.</td><td>Kegiatan Sosial</td><td>:</td><td><?php echo $r['yayasan_kegiatan_sosial'];?></td></tr>
						<tr><td>2.</td><td>Kegiatan Kemanusiaan</td><td>:</td><td><?php echo $r['yayasan_kegiatan_kemanusiaan'];?></td></tr>	
						<tr><td>3.</td><td>Kegiatan Keagamaan</td><td>:</td><td><?php echo $r['yayasan_kegiatan_keagamaan'];?></td></tr>						
					</table>					
				</p>
				<p>	
					Sebagai Nazhir atas tanah wakaf yang terletak di :
					<table style="margin-left:30px">
						<tr><td>1.</td><td>Desa/Kelurahan</td><td>:</td><td><?php echo $r['yayasan_kelurahan'];?></td></tr>
						<tr><td>2.</td><td>Kecamatan</td><td>:</td><td><?php echo $r_user['user_kecamatan_nama'];?></td></tr>	
						<tr><td>3.</td><td>Kabupaten/Kota</td><td>:</td><td><?php echo $r_user['user_kabupaten_nama'];?></td></tr>
						<tr><td>4.</td><td>Propinsi</td><td>:</td><td><?php echo $r_user['user_propinsi_nama'];?></td></tr>						
					</table>					
				</p>
				<p>Luas tanah yang diurusnya / tanggung jawabnya : ...</p>
			</div>
		</div>
		<div class="ttd_right">			
			<div>
				<table>
					<tr><td align="left">Disahkan di</td><td>:</td><td><?php echo $r_user['user_kecamatan_nama'];?></td></tr>
					<tr><td align="left">Tanggal</td><td>:</td><td><?php echo $tgl_indo;?></td></tr>						
				</table>
				Kepala Kantor Urusan Agama Kecamatan/<br/>
				Pejabat Pembuat Akta Ikrar Wakaf,				
			</div>
			<div style="margin-top:25px;">
				<br/><br/>
				<span style="text-decoration:underline;"><?php echo $r_user['user_an_pejabat'];?></span>
			</div>
		</div>
		<div style="clear:both;"></div>
		<div class="content">
			<p>	
				Keterangan :
				<ol>
					<li>Asli Surat Pengesahan ini diberikan kepada Nazhir yang bersangkutan</li>
					<li>Lembar ke-2 (kedua) tembusan kepada Kepala Kantor Kemenag Kabupaten</li>
					<li>Arsip</li>
				</ol>					
			</p>
		</div>
		<div class="ttd_right">	
			<table>
				<tr>
				<td align="center" border="1px solid">
					<?php			
						include("../plugins/phpqrcode/phpqrcode.php");
						QRcode::png('http://hajibangkalan.xyz/?code=Uji Coba','QR_gen.png');
					?>
					<br/>
					<img alt="Uji Coba" src="QR_gen.png"/>
				</td>
				</tr>
			</table>
		</div>
		<div style="clear:both;"></div>
		<footer></footer>
			
		
	</div>
	
	
</body>
</html>