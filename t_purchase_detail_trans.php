    <?php
        $purchase_id = $_GET['pid'];

        $q = "SELECT * FROM t_purchase WHERE purchase_id = '".$purchase_id."'";
        $sql = mysqli_query($conn_db,$q);
        $rec = mysqli_fetch_array($sql);
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Transaction of (<?php echo $purchase_id; ?>)
        <small>Detail purchasing</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="v_main.php?module=<?php echo md5("t_purchase"); ?>"><i class="fa fa-dashboard"></i> Purchase Transaction</a></li>
        <li class="active">Detail Transaction</li>
      </ol>
    </section>
	
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6  col-xs-6">
        <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-shopping-bag"></i>
              <h3 class="box-title">Purchasing Info</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">

              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
                  <table>
                    <tr>
                      <td width="200px">Transaction ID</td>
                      <td width="20px">:</td>
                      <td><?php echo $rec['purchase_id'];?></td>
                    </tr>
                    <tr>
                      <td width="200px">Transaction Date</td>
                      <td width="20px">:</td>
                      <td><?php echo $rec['purchase_date'];?></td>
                    </tr>
                    <tr>
                      <td width="200px">Transaction Description</td>
                      <td width="20px">:</td>
                      <td><?php echo $rec['purchase_description'];?></td>
                    </tr>
                  </table>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
              <?php
                $q = "SELECT SUM(purchasedetail_subtotal) AS purchasedetail_sum 
                    FROM t_purchasedetail 
                    WHERE purchasedetail_purchase_id='".$purchase_id."'";
                $sql = mysqli_query($conn_db,$q);	
                $rec2 = mysqli_fetch_array($sql);
              ?>
              <sup style="font-size: 16px">Rp. </sup>
              <span id="purchasedetail_sum"><?php echo number_format($rec2['purchasedetail_sum'],2);?></span>              
              </h3>
              <p>Total Transaksi</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>            
          </div>
        </div>
                
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
          
          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-plus"></i>

              <h3 class="box-title">Tambahkan Item Barang</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                <div class="btn-group">                  
                </div>
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-striped table-hover table-bordered" id="datatable_product"
                      width="100%" cellspacing="0" role="grid" aria-describedby="datatable_product" style="width: 100%;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th align="center">Aksi</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Stock</th>					
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                  <form id="f-add" method="post" action="#">
                    <input name="purchasedetail_purchase_id" id="purchasedetail_purchase_id" type="hidden" value="<?php echo $purchase_id;?>">
                    <input name="purchasedetail_product_id" id="purchasedetail_product_id" type="hidden" value="">
                    <div class="form-group">
                      <label>Product Name</label>                      
                      <input name="purchasedetail_product_name" id="purchasedetail_product_name" type="text" class="form-control" readonly="readonly">
                    </div>
                    <div class="form-group">
                      <label>Price</label>                      
                      <input name="purchasedetail_product_price" id="purchasedetail_product_price" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Count</label>                      
                      <input name="purchasedetail_count" id="purchasedetail_count" type="numeric" class="form-control" value="1">
                    </div>
                    <button type="button" class="btn btn-success btn-sm" id="btn_addcart"><i class="fa fa-plus"> Add Purchase Item</i>
                  </form>
                </div>
                <!-- /.col -->                
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          
          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-cart-arrow-down"></i>

              <h3 class="box-title">Daftar Item</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
                        title="Print">
                  <i class="fa fa-print"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
                  <table class="table table-striped table-hover table-bordered" id="datatable_detail"
                      width="100%" cellspacing="0" role="grid" aria-describedby="datatable_detail" style="width: 100%;">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th align="center">Aksi</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Count</th>		
                        <th>Price</th>
                        <th>Subtotal</th>			
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                  <hr/>
                  <div class="row">
                    <div class="col-sm-8">
                      <form id="f-purchase-post" method="post" action="#">
                        <input name="purchasedetail_purchase_id2" id="purchasedetail_purchase_id2" type="hidden" value="<?php echo $purchase_id;?>">              
                        <div class="form-group">
                          <label>Amount of Payment</label>                      
                          <input name="purchasedetail_payment_amount" id="purchasedetail_payment_amount" type="numeric" class="form-control" value="<?php echo $rec['purchase_payment_amount']; ?>">
                        </div>                    
                      </form>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label>--</label>
                        <button type="button" class="form-control btn btn-warning" id="btn_set_lunas"> 
                          <i class="fa fa-check"></i> Set Lunas                        
                        </button>
                      </div> 
                    </div>
                  </div>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-primary" id="btn_purchase_save"> 
                Simpan
                <i class="fa fa-save"></i>
              </button>
              <button type="button" class="pull-right btn btn-default" id="btn_purchase_draft">
                <i class="fa fa-arrow-circle-right"></i> 
                Draft            
              </button>
            </div>
          </div>

        </section>
        <!-- /.Left col -->
        
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

    <script>
										
		// Tampilkan tabel
		var oTable01 = $('#datatable_product').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 1. no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 2. aksi */
				,{"bSearchable": true}	/* 3. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 4. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 5. */	
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [5,10,15,20],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "t_purchase_product_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		        	  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});

    var oTable02 = $('#datatable_detail').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 1. no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 2. aksi */
				,{"bSearchable": true}	/* 3. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 4. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 5. */        
        ,{ "sClass": "center" }	  /* 6. */
        ,{ "sClass": "center" }	  /* 7. */		  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,15,20],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "t_purchase_detail_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );	
        aoData.push( { "name": "purchase_id", "value": "<?php echo $purchase_id; ?>" } );	  
			  $.ajax( {
          "dataType": 'json', 
          "type": "POST", 
          "url": sSource, 
          "data": aoData, 
          "success": fnCallback
			  } );          
			}
		});
			
		$(document).ready(function() { 		
			
			$( "#datatable_detail tbody" ).on( "click",".jq-delete-detil", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "t_purchase_aksi.php?act=delete_detail", { a: id, purchase_id : "<?php echo $purchase_id; ?>"}
						,function( data ){
              if(data.msg=="OK"){
                $("#modal-info").modal("hide");            
                oTable02.ajax.reload();                
                $("#purchasedetail_sum").number(data.purchasedetail_sum,2);
              }else{
                alert_custom(data.response,"modal-success");
              }
						},"json");			
					oTable02.ajax.reload();			
				}		
			});
			
			$( "#datatable_product tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "t_purchase_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							oTable01.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});

      $( "#datatable_product tbody" ).on( "click",".jq-detail", function(e) {
				var id = $(this).attr("lang");
				document.location.href = "v_main.php?module=<?php echo md5("t_purchase_detail_trans"); ?>&pid="+id;		
			});			
			
			var frm = $('#f-add');			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("t_purchase_aksi.php?act=addcart", dataform, function(data){
					if(data.msg=="OK"){
            $("#modal-info").modal("hide");            
            oTable02.ajax.reload();
            oTable01.ajax.reload();
            $("#purchasedetail_sum").number(data.purchasedetail_sum,2);
          }else{
            alert_custom(data.response,"modal-success");
          }
				},"json");							
			});
			
			function clear_form(){

			}
			
      $("#datatable_product tbody").on("click",".jq-radio",function(e){        
        $("#purchasedetail_product_id").val($(this).attr("value"));
        $("#purchasedetail_product_price").val($(this).attr("pprice"));							
        $("#purchasedetail_product_name").val($(this).attr("pname") );
			});
			
			$("#btn_addcart").click(function(e){							
				frm.submit();				
				clear_form();
			});

      $("#btn_purchase_draft").click(function(e){						
				document.location.href = "v_main.php?module=<?php echo md5("t_purchase"); ?>";		
			});

      var frm2 = $('#f-purchase-post');			
			frm2.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm2.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("t_purchase_aksi.php?act=purchase_post", dataform, function(data){
					if(data.msg=="OK"){
            $("#modal-info").modal("hide");	
            document.location.href = "v_main.php?module=<?php echo md5("t_purchase"); ?>";
          }else{
            alert_custom(data.response,"modal-success");
          }
				},"json");							
			});

      $("#btn_purchase_save").click(function(e){        					
				frm2.submit();	
			});

      $("#btn_set_lunas").click(function(e){     
        var sum_str = $("#purchasedetail_sum").text();
        sum_str = sum_str.replace(",","");  					
				$("#purchasedetail_payment_amount").val(sum_str);
			});
			
		});
	</script>
