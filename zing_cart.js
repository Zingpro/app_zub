// Inisialisasi keranjang belanja (gunakan array untuk menyimpan item)
const zingcart = [];

// Fungsi untuk menambahkan item ke keranjang
function addItemToCart(itemName, subtotal, price) {
    zingcart.push({ name: itemName, subtotal:subtotal, price: price });
    console.log(itemName+' ditambahkan ke keranjang.');
    showCart();
}

// Fungsi untuk menghitung total harga
function calculateTotalPrice() {
    let total = 0;
    for (const item of zingcart) {
        total += (item.price*item.subtotal);
    }
    return total;
}

function calculateSubtotalItem() {
    let count_item = 0;
    for (const item of zingcart) {
        count_item += item.subtotal;
    }
    return count_item;
}
// Contoh penggunaan
// addItemToCart('Buku', 1, 5000); // Menambahkan buku dengan harga 25 ke keranjang
// addItemToCart('Pensil',2, 2000); // Menambahkan pensil dengan harga 5 ke keranjang
function showCart(){
    const totalHarga = calculateTotalPrice();
    const totalItem = calculateSubtotalItem();
    console.log('Total harga: '+totalHarga);
    console.log('Total Item: '+totalItem);
}