<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi Saham <?php //echo $r['USER_FULLNAME'];?>
        <small>Penerbitan Saham Baru.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Transaksi Saham</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Form Penerbitan Saham Baru</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
						title="Collapse">
				  <i class="fa fa-minus"></i>
				  </button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				  <i class="fa fa-times"></i>
				  </button>
			  </div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<form id="f-add" method="post" action="#">
					<div class="col-md-12">
						<h3>Inputkan secara lengkap data penerbitan Saham</h3>
						
						  <div class="row">
							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-graduation-cap"></i>

								  <h3 class="box-title">Data Anggota</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">								  
								  <div class="callout callout-success">									
									<h4><i class="icon fa fa-info-circle"></i> Pilih Anggota pemberi Saham</h4>
									pencarian menggunakan nomor KTP.
								  </div>
									Pilih Anggota : <select name="anggota_id" id="anggota_id" class="form-control select2" placeholder="Pilih Wakif" style="width: 100%;">
									</select>
									<br/>
									<br/>
									<div class="callout">
										<table class="dtp">
											<tr><td>Nama Lengkap</td><td>:</td><td><span id="anggota_nama"></span></td></tr>
											<tr><td>Kelompok</td><td>:</td><td><span id="anggota_kelompok"></span></td></tr>
											<tr><td>Tempat Tinggal</td><td>:</td><td><span id="anggota_alamat"></span></td></tr>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->
							
							<div class="col-md-6">
								<div class="box box-default">
									<div class="box-header with-border">
									  <i class="fa fa-bank"></i>
									  <h3 class="box-title">Obyek Saham</h3>
									</div>
								</div>
								<div class="box-body">
									<div class="callout">
										<div class="form-group">
										  <label>Jumlah Saham</label>
										  <input type="text" class="form-control" value="1" name="saham_jumlah" id="saham_jumlah" placeholder="Jumlah Lembar Saham ...">
										</div>
										<div class="form-group">
										  <label>Harga Satuan Saham</label>
										  <input type="text" class="form-control" name="saham_harga" id="saham_harga" placeholder="Harga Satuan Saham ...">
										</div>
									</div>
									
									<center>
										<button type="button" class="btn btn-primary" id="btn_submit">
											<i class='fa fa-save'></i> Simpan 
										</button>
									</center>									
								</div>
								
							</div>
							<!-- /.col -->
						  </div>
						  		
					</div>
					</form>
				</div>
			</div>
        <!-- /.box-body -->
        <div class="box-footer">
          Form Pendataan Saham Baru
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
	
	<style>
		.dtp td{
			padding : 3px;
		}
	</style>
	
	<script>
		
		
		function load_anggota(wrap_out)
		{			
			$('#'+wrap_out).empty();
			return $.ajax({
				type: "POST",
				url: "trx_saham_aksi.php?act=load_anggota",
				data: { 'data_id': 'a'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].pemilik_saham_id + '">' + data.record[i].pemilik_saham_id+ '\n | '+data.record[i].pemilik_saham_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_data_anggota(id)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_saham_aksi.php?act=load_data_anggota",
				data: { 'data_id': id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						$("#anggota_nama").text(data.record.pemilik_saham_nama);
						$("#anggota_alamat").text(data.record.pemilik_saham_alamat);
						$("#anggota_kelompok").text(data.record.kelompok_nama);
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
						
		function initial_load(){			
			$.when(
				load_anggota("anggota_id")
			).done(function(){
				var data_id = $('#anggota_id').val();
				load_data_anggota(data_id);				
			});			
		}
		
		$(document).ready(function() { 		
			initial_load();
			
			$('#anggota_id').on('select2:select', function (e) {
				var data_id = $(this).val();
				load_data_anggota(data_id);
			});
						
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("trx_saham_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
					
				},"json");
								
			});
			
			$("#btn_submit").click(function(e){						
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			function clear_form(){				
				$("#Saham_id").val("");
				$("#Saham_niw").val("");
				$("#Saham_obyek").val("");
				$("#Saham_sertifikat").val("");
				$("#Saham_alamat").val("");				
			}
		});
	</script>