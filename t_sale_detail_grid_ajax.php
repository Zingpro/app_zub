<?php
	include("config/datatables_product.php");
	
	$where = "";
        
	$sale_id = $_POST['sale_id'];
	$where = (empty($sale_id))? "" : " AND saledetail_sale_id='".$sale_id."' ";
	
	$aColumns = array( 
		'saledetail_id',
        'product_code',
        'product_name',
		'saledetail_count',
		'saledetail_price',
        'saledetail_subtotal',
		'item_status',
		);
	$sIndexColumn = "saledetail_id";
	
	$sQuery = "SELECT
                t_saledetail.saledetail_id,                
                CONCAT(m_product.product_id,'|',m_product.product_code) as product_code,
                m_product.product_name,
                t_saledetail.saledetail_count,
                t_saledetail.saledetail_price,
                t_saledetail.saledetail_subtotal,
				t_sale.sale_status as item_status
			FROM t_saledetail
            LEFT JOIN m_product ON m_product.product_id = t_saledetail.saledetail_product_id 			
			LEFT JOIN t_sale ON t_sale.sale_id = t_saledetail.saledetail_sale_id 
			WHERE 1=1 ".$where." ";
	// echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('saledetail_id','item_status');
	
	//untuk format
	$sFunctions = array();

	$actions = array(
		'delete_detil'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>