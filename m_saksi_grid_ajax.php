<?php
	include("config/datatables_wakif.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'saksi_id',
		'saksi_nik',
		'saksi_nama',
		'kecamatan_name',
		'saksi_alamat'
		);
	$sIndexColumn = "saksi_id";
	
	$sQuery = "SELECT *
			FROM `v_saksi` 
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('saksi_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>