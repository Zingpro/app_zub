    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
	
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
				<h3>
				<?php
					$q = "SELECT * FROM m_pemilik_saham";
					$sql = mysqli_query($conn_db,$q);
					$num = mysqli_num_rows($sql);
					echo $num;
				?>
				<sup style="font-size: 16px">Orang</sup>
				</h3>
              <p>Anggota</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="v_main.php?module=<?php echo md5("m_pemilik_saham"); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
				<?php
					$q = "SELECT * FROM m_saham";
					$sql = mysqli_query($conn_db,$q);
					$num = mysqli_num_rows($sql);
					echo $num;
				?>
				<sup style="font-size: 16px">Lembar</sup>
				</h3>
              <p>Saham</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="v_main.php?module=<?php echo md5("m_nazhir"); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <?php 
          if($_SESSION['USERGROUP_ID']=='1'):
        ?>
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">          
          <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Administrator Functional Menu</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button> -->
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <button type="button" class="btn btn-primary" id="btn_calculate_stock">
                <i class="fa fa-list-alt"></i>
                 Calculated Stock of Product 
                <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
            <div class="box-footer clearfix">
              Ini adalah kumpulan tombol fungsi administrator
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </section>
        <!-- /.Left col -->
        <?php 
          endif;
        ?>
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
          
          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                <div class="btn-group">
                  <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bars"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#">Add new event</a></li>
                    <li><a href="#">Clear events</a></li>
                    <li class="divider"></li>
                    <li><a href="#">View calendar</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button> -->
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-6">
                  <!-- Progress bars -->
                  <div class="clearfix">
                    <span class="pull-left">Task #1</span>
                    <small class="pull-right">90%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                  </div>

                  <div class="clearfix">
                    <span class="pull-left">Task #2</span>
                    <small class="pull-right">70%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                  <div class="clearfix">
                    <span class="pull-left">Task #3</span>
                    <small class="pull-right">60%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                  </div>

                  <div class="clearfix">
                    <span class="pull-left">Task #4</span>
                    <small class="pull-right">40%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
      <div class="row">
        
        <section class="col-lg-7 connectedSortable">          
          <!-- quick email widget -->
          <div class="box box-solid">
            <div class="box-header">
              <i class="fa fa-gear"></i>
              <h3 class="box-title">Quick Menu Anggota</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button> -->
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3>Harta</h3>
                      <div class="icon"><i class="ion ion-stats-bars"></i></div>
                    </div>                    
                    <a href="v_main.php?module=<?php echo md5("m_pemilik_saham"); ?>" class="small-box-footer">More info <i class="fa fa-bar-chart"></i></a>
                  </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3>Pinjam</h3>
                      <div class="icon"><i class="ion ion-android-attach"></i></div>
                    </div>                    
                    <a href="v_main.php?module=<?php echo md5("m_pemilik_saham"); ?>" class="small-box-footer">More info <i class="fa fa-money"></i></a>
                  </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3>SHU</h3>
                      <div class="icon"><i class="ion ion-android-favorite"></i></div>
                    </div>                    
                    <a href="v_main.php?module=<?php echo md5("m_pemilik_saham"); ?>" class="small-box-footer">More info <i class="fa fa-heart"></i></a>
                  </div>
                </div>

                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3>PROFIL</h3>
                      <div class="icon"><i class="ion ion-android-contact"></i></div>
                    </div>                    
                    <a href="v_main.php?module=<?php echo md5("m_user_profil"); ?>" class="small-box-footer">More info <i class="fa fa-user"></i></a>
                  </div>
                </div>
            </div>
            <div class="box-footer clearfix">
              Fitur bagi Anggota
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </section>
        
      </div>
    </section>
    <!-- /.content -->
<script>
  $(document).ready(function() { 
    $("#btn_calculate_stock").click(function(e){							
        e.preventDefault();
								
				alert_custom("Loading Process","modal-info");	
				$.post("m_product_aksi.php?act=calculated_stock", { a: "post"}, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
				},"json");
    });	
  });
</script>