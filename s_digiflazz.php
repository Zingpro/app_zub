<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	

?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Konfigurasi Koneksi Digiflazz<?php //echo $r['USER_FULLNAME'];?>
        <small>API Connection.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Konfigurasi Koneksi API</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Form Konfigurasi</h3>
			  <div class="box-tools pull-right">
				
			  </div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<form id="f-add" method="post" action="#">
					<div class="col-md-12">
						<h3>Sesuaikan data konfigurasi untuk koneksi ke akun Digiflazz Anda</h3>
						
						  <div class="row">
							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-gear"></i>

								  <h3 class="box-title">Informasi API Connection</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">	
									<div class="callout">
                                        <div class="form-group">
										  <label>Host/IP Endpoint</label>
										  <input type="text" class="form-control" value="<?php echo get_config_val('digiflazz_host'); ?>" name="digiflazz_host" id="digiflazz_host" placeholder="Isi Host End Point">
										</div>
										<div class="form-group">
										  <label>User/Akun Digiflazz</label>
										  <input type="text" class="form-control" name="digiflazz_username" id="digiflazz_username" value="<?php echo get_config_val('digiflazz_username'); ?>" placeholder="Isi dengan User pada akun Digiflazz">
										</div>
                                        <div class="form-group">
										  <label>Api-Key Digiflazz</label>
										  <input type="text" class="form-control" name="digiflazz_apikey" id="digiflazz_apikey" value="<?php echo get_config_val('digiflazz_apikey'); ?>" placeholder="Api-key akun Digiflazz">
										</div>
                                        <center>
                                            <button type="button" class="btn btn-primary" id="btn_submit">
                                                <i class='fa fa-save'></i> Simpan 
                                            </button>
                                        </center>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->
							
							<div class="col-md-6">
								<div class="box box-default">
									<div class="box-header with-border">
									  <i class="fa fa-bank"></i>
									  <h3 class="box-title">Menu Digiflazz</h3>
									</div>
								</div>
								<div class="box-body">
									<div class="callout">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary" id="btn_saldo">
                                                <i class='fa fa-check'></i> Cek Saldo 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-warning" id="btn_listharga">
                                                <i class='fa fa-list'></i> Daftar Harga 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-danger" id="btn_cektagihan">
                                                <i class='fa fa-money'></i> Cek Tagihan 
                                            </button>
                                        </div>
										<div><table id="digi-grid"></table></div>
									</div>								
								</div>
								
							</div>
							<!-- /.col -->
						  </div>
						  		
					</div>
					</form>
				</div>
			</div>
        <!-- /.box-body -->
        <div class="box-footer">
          Form Konfigurasi API Connection untuk Digiflazz
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
	
	<style>
		.dtp td{
			padding : 3px;
		}
	</style>
	
	<script>
		$(document).ready(function() { 	
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("s_digiflazz_act.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
					
				},"json");
								
			});
			
			$("#btn_submit").click(function(e){						
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			$("#btn_saldo").click(function(e){	
				var dataform = {"ok":"ok"};					
				$.post("s_digiflazz_act.php?act=request_ceksaldo", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data,"modal-success");										
				},"json");
			});
			
			$("#btn_listharga").click(function(e){	
				var dataform = {"ok":"ok"};					
				$.post("s_digiflazz_act.php?act=request_listharga", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data,"modal-success");	
					to_grid(data);									
				},"json");
			});

			function to_grid(datagrid){
				$('#digi-grid').DataTable( {
					data: datagrid.data,
					columns: [
						{ data: 'buyer_sku_code' },
						{ data: 'product_name' },
						{ data: 'price' },
						{ data: 'category' }
					]
				});
			}
		});
	</script>