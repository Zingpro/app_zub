<?php 
	include 'config/koneksi_li.php';
    include 'config/all_function.php';

	$limit		= ($_GET['limit']);
	$halaman	= ($_GET['halaman']);
 	$key = ($_GET['key']);			

    if(!empty($key)){
        $q = "SELECT * FROM m_product WHERE product_stock > 0 AND product_name LIKE '%".$key."%'";
    }else{
	    $q = "SELECT * FROM m_product WHERE product_stock > 0";
    }
	//echo $q;
	$page_link = get_pagination_page($q,$limit,$halaman);		
	echo $page_link;
	
	$start = ($halaman-1)*$limit;
?>

    <div class="row">
        <?php
            if(!empty($key)){
                $q = "SELECT * FROM m_product WHERE product_stock > 0 AND product_name LIKE '%".$key."%' LIMIT ".$start.",".$limit;
            }else{
                $q = "SELECT * FROM m_product WHERE product_stock > 0 LIMIT ".$start.",".$limit;
            }
            $sql = mysqli_query($conn_db,$q);	
            while($r = mysqli_fetch_array($sql)):	
        ?>
        <div class="col-md-3 col-xs-6 col-6 m-auto">
            <div class="panel panel-default" >
                <div class="panel-body">
                    <div class="text-center">
                        <?php
                            if(!empty($r['product_thumbnail'])):
                        ?>
                            <img src="<?php echo $r['product_thumbnail'];?>" width="80%" height="120px"><br/>
                        <?php
                            else:
                        ?>
                            <img src="./uploads/product/default.jpg" width="80%" height="120px"><br/>
                        <?php
                            endif;
                        
                        ?>
                        <div class="col-sm-2 pull-left">
                            <button class="btn btn-sm btn-default btn-jqupload" lang="<?php echo $r['product_id']; ?>">
                                <span class="glyphicon glyphicon-open"></span>
                            </button> 
                        </div>
                        <div class="col-sm-2 pull-right">
                            <button class="btn btn-sm btn-primary" type="button" onclick="addItemToCart('<?php echo $r['product_name']; ?>',1,<?php echo $r['product_price_sale']; ?>);">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </div>
                        <hr/>
                    </div>
                    <div>
                        <?php echo $r['product_name']; ?><br/>
                        Harga : Rp. <?php echo number_format($r['product_price_sale'],2); ?><br/>
                        Stock : <?php echo $r['product_stock']; ?><br/>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php
            endwhile;
        ?>
    </div>
    
<script>
	$(document).ready(function(){		
		$(".per_page").click(function(){
			var halaman = $(this).attr("lang");
            var key = "<?php echo $key; ?>";
			$("#product_page")
				.load("list_product_page.php?limit=50&halaman="+halaman+"&key="+key);
		});
		$(".btn-jqupload").click(function(e){
            var id = $(this).attr("lang");
            $("#parent_id").val(id);
            $("#modal-uploadfile").modal("show");
        });

        $("#btn_submit_upload").click(function(e){	
            var frm_upload = $('#f-upload');
            frm_upload.submit();
            $("#modal-uploadfile").modal("hide");
            alert_custom("Uploading Process","modal-info");
        });
	});
</script>
<div style="clear:both"></div>
<script src="./zing_upload_product_foto_public.js"></script>