<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];


function post_data(){
	include "config/koneksi_li.php";
	session_start();
	$parent_id = $_POST['parent_id'];
	$trx_date = date('Y-m-d H:i:s');
	$trx_nominal = $_POST['trx_nominal'];
	$created_by = $_SESSION['USER_ID'];

	if(empty($created_by)):		
		$data['msg'] = "OK";
		$data['response'] = "Session berakhir, Silahkan Login Kembali";
	else:
		$q = "UPDATE m_pemilik_saham SET pemilik_saham_sisa_gaji = '".$trx_nominal."', 
				pemilik_saham_sisa_gaji_updated_date = '".$trx_date."',
				pemilik_saham_sisa_gaji_update_by = '".$created_by."' 
			WHERE pemilik_saham_id='".$parent_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil tersimpan.";//.mysqli_error($conn_db).$q;
	endif;
	
	echo json_encode($data);
}

function load_member(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM m_pemilik_saham 
			   WHERE pemilik_saham_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = "OK";
	$data['record'] = mysqli_fetch_array($sql);
	echo json_encode($data);
}


function cek_username_exist($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM s_user 
				WHERE USER_NAME = '".$username."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

function generate_excel_value($inputFileName,$result){          
	include "config/koneksi_li.php"; 
	// error_reporting(E_ALL);
	session_start();           
	$start_at = 2;
	$kolom = 'A';
	// echo "1. ".$inputFileName."<br/>";
	
	/** Include path **/
	set_include_path(get_include_path() . PATH_SEPARATOR . 'plugins/PHPExcel/Classes/');

	/** PHPExcel_IOFactory */
	include 'plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	$result['msg'] .= 'File '.pathinfo($inputFileName,PATHINFO_BASENAME).' has been identified as an '.$inputFileType.' file<br/>';
	$result['msg'] .= 'Loading file '.pathinfo($inputFileName,PATHINFO_BASENAME).' using IOFactory with the identified reader type<br/>';
	
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($inputFileName);
	$result['msg'] .= "================================================<br/>";
	
	$search = true;
	$idx = $start_at;			
	$count_update = 0;
	$count_insert = 0;
	$dt = $objPHPExcel->getActiveSheet()->getCell('A'.$idx)->getValue();
	// echo $dt;
	while($dt!=''){
		// echo "loop".$dt;
		$dt = $objPHPExcel->getActiveSheet()->getCell('A'.$idx)->getValue();
		$record_id 	= date('YmdHis')."_".$dt;
		$member_id 	= mysqli_real_escape_string($conn_db,$objPHPExcel->getActiveSheet()->getCell('A'.$idx)->getValue());
		$nominal 	= mysqli_real_escape_string($conn_db,$objPHPExcel->getActiveSheet()->getCell('C'.$idx)->getValue());
		$created_date = date('Y-m-d H:i:s');
		$trx_date = date('Y-m-d');
		$created_by	= $_SESSION['USER_ID'];		              
		$q = "UPDATE m_pemilik_saham SET pemilik_saham_sisa_gaji = '".$nominal."', 
				pemilik_saham_sisa_gaji_updated_date = '".$trx_date."' 
			WHERE pemilik_saham_id='".$member_id."'";
		//echo $q."<br/>";
		$sql_insert = mysqli_query($conn_db,$q);

		if($sql_insert){
			$count_insert++;
		}else{
			$result['msg'] .= $dt.":".mysqli_error($conn_db)."<br/>";
		}	
		$idx++;            
	}      
	
	$result['msg'] .= "Updated : ".$count_insert."<br/>";
}

function upload_dokumen($uploaddir = "./uploads/"){
	include "config/koneksi_li.php";
	$result = array();	 
	$files = array();
	
	foreach($_FILES as $file)
	{
		$nama_file = date('Y.m.d')."_".rand(1,100)."_".basename($file['name']);
		$inputFileName = $uploaddir.$nama_file;

		if (!file_exists($uploaddir)) {
			mkdir($uploaddir, 0777, true);
		}

		if(move_uploaded_file($file['tmp_name'], $inputFileName))
		{
			$result['msg'] .= 'File terupload :'.$nama_file.'<br/>';
			$result['success'] = 1;
			
			if (file_exists($inputFileName)) {
				chmod($inputFileName, 0777);
			}
			generate_excel_value($inputFileName,$result);
		}
		else
		{
			$result['success'] = 0;
			$result['msg'] .= "Terjadi Error Upload<br/>";
		}				
	}
	
	header('Content-Type: application/json');
	echo json_encode($result);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='edit'){
	load_member();
}else if ($act=='load_member'){
	load_member();
}else if ($act=='upload'){
	upload_dokumen();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>