<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];

function post_data(){
	include "config/koneksi_li.php";
	session_start();

	$product_id			= mysqli_real_escape_string($conn_db,$_POST['product_id']);
	$product_code 		= mysqli_real_escape_string($conn_db,$_POST['product_code']);
	$product_name 		= mysqli_real_escape_string($conn_db,$_POST['product_name']);
	$product_description= mysqli_real_escape_string($conn_db,$_POST['product_description']);
	$product_price_sale	= mysqli_real_escape_string($conn_db,$_POST['product_price_sale']);
	$product_stock		= mysqli_real_escape_string($conn_db,$_POST['product_stock']);
	$product_created_at	= date("Y-m-d H:i:s");
	$product_created_by	= $_SESSION['USER_ID'];

	if(empty($product_id)):
		$product_id = date('YmdHis')."_".rand(1,200);
		$q = "INSERT INTO m_product (
					product_id
					,product_code
					,product_name
					,product_description
					,product_price_sale
					,product_stock
					,product_created_at
					,product_created_by
				) VALUES (
					'".$product_id."'
					,'".$product_code."'
					,'".$product_name."'				
					,'".$product_description."'
					,'".$product_price_sale."'
					,'".$product_stock."'
					,'".$product_created_at."'
					,'".$product_created_by."'
				)";
		// echo $q;
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE m_product SET
					product_code 			= '".$product_code."'
					,product_name 			= '".$product_name."'
					,product_description 	= '".$product_description."'
					,product_price_sale 	= '".$product_price_sale."'
					,product_stock 			= '".$product_stock."'					
				WHERE product_id 			= '".$product_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah";
	endif;
	
	echo json_encode($data);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "DELETE FROM m_product 
			   WHERE product_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = mysql_affected_rows();
	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM m_product 
			   WHERE product_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function calculated_stock(){
	include "config/koneksi_li.php";
	
	//Kosongi data stock product
	$q_update = "UPDATE m_product SET 
							product_stock = '0'";
	$sql_update = mysqli_query($conn_db,$q_update);
	
	//Loop detail purchase;
	$q = "SELECT purchasedetail_product_id
				,SUM(purchasedetail_count) as purchasedetail_count 
			FROM t_purchasedetail 
			GROUP BY purchasedetail_product_id";	
	$sql = mysqli_query($conn_db,$q);

	while($row = mysqli_fetch_array($sql)):
		$product_id = $row['purchasedetail_product_id'];
		$count = $row['purchasedetail_count'];
		
		$q_update = "UPDATE m_product SET 
							product_stock = '".$count."' 
						WHERE product_id = '".$product_id."'";
		$sql_update = mysqli_query($conn_db,$q_update);
	endwhile;

	//Loop detail sale;
	$q = "SELECT saledetail_product_id
				,SUM(saledetail_count) as saledetail_count 
			FROM t_saledetail 
			GROUP BY saledetail_product_id";	
	$sql = mysqli_query($conn_db,$q);
	while($row = mysqli_fetch_array($sql)):
		$product_id = $row['saledetail_product_id'];
		$count = $row['saledetail_count'];
		
		$q_update = "UPDATE m_product SET 
							product_stock = (product_stock - ".$count.")  
						WHERE product_id = '".$product_id."'";
		$sql_update = mysqli_query($conn_db,$q_update);
	endwhile;

	$data['msg'] = "OK";
	$data['response'] = "Calculated Stock Done.";
	
	echo json_encode($data);
}

function upload_foto($uploaddir = "./uploads/product/"){
	include "config/koneksi_li.php";
	$data = array();
	 
	$error = false;
	$files = array();

	if (!file_exists($uploaddir)) {
		mkdir($uploaddir, 0777, true);
	}

	foreach($_FILES as $file)
	{
		$nama_file = date('Y.m.d')."_".basename($file['name']);
		if(move_uploaded_file($file['tmp_name'], $uploaddir .$nama_file))
		{
			$files[] = $uploaddir.$file['name'];
			$parent_id = $_GET['parent_id'];
			$gen_id = generate_dateid();
			$media_file = mysqli_real_escape_string($conn_db,$uploaddir.$nama_file);
			$q = "INSERT INTO m_product_media(id,product_id,media_file) VALUES ('".$gen_id."','".$parent_id."','".$media_file."')";
			// echo $q;
			$sql = mysqli_query($conn_db,$q);
			if ($sql === false) {
				$error = true;
			}
		}
		else
		{
			$error = true;
		}				
	}
	
	$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
	$data = array(
		'success' => 'OK'
		,'msg' => 'Foto Telah Di Upload'
		,'formData' => $_POST
	);	

	echo json_encode($data);
}

function upload_foto_2($uploaddir = "./uploads/product/"){
	include "config/koneksi_li.php";
	$data = array();
	
	if($_POST['password']=='ok2024'){
		$error = false;
		$files = array();

		if (!file_exists($uploaddir)) {
			mkdir($uploaddir, 0777, true);
		}

		foreach($_FILES as $file)
		{
			$parent_id = $_GET['parent_id'];
			
			$q = "SELECT * FROM m_product WHERE product_id='".$parent_id."'";
			$sql = mysqli_query($conn_db,$q);
			$r = mysqli_fetch_array($sql);
			if(!empty($r['product_thumbnail'])):
				unlink($r['product_thumbnail']);
			endif;

			$nama_file = date('Y.m.d')."_".basename($file['name']);			
			if(move_uploaded_file($file['tmp_name'], $uploaddir .$nama_file))
			{
				$files[] = $uploaddir.$file['name'];				
				$gen_id = generate_dateid();
				$media_file = mysqli_real_escape_string($conn_db,$uploaddir.$nama_file);
				$q = "UPDATE m_product SET product_thumbnail='".$media_file."' WHERE product_id = '".$parent_id."'";
				//echo $q;
				$sql = mysqli_query($conn_db,$q);
				if ($sql === false) {
					$error = true;
				}
			}
			else
			{
				$error = true;
			}				
		}
		
		$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
		$data = array(
			'success' => 'OK'
			,'msg' => 'Foto Telah Di Upload'
			,'formData' => $_POST
		);	
	}else{
		$data = array(
			'success' => 'ERROR'
			,'msg' => 'password salah'
			,'formData' => $_POST
		);	
	}

	echo json_encode($data);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='load_kab'){
	load_kab();
}else if ($act=='load_kec'){
	load_kec();
}else if ($act=='calculated_stock'){
	calculated_stock();
}else if ($act=='upload_foto'){
	upload_foto();
}else if ($act=='upload_foto_2'){
	upload_foto_2();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>