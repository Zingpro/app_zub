<?php
	include("config/datatables_purchase.php");
	session_start();
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'purchase_id',
        'purchase_date',
		'purchase_description',
		'purchase_total',
		'purchase_payment_amount',
		'purchase_status',
		);
	$sIndexColumn = "purchase_id";
	
	$sQuery = "SELECT 
			t_purchase.purchase_id,
            t_purchase.purchase_date,
			t_purchase.purchase_description,
			t_purchase.purchase_total,
			t_purchase.purchase_payment_amount,
			t_purchase.purchase_status,
			t_purchase.purchase_created_by 
			FROM
			t_purchase
			WHERE 1=1 ".$where." ";
	//echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array();
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
        ,'detail'
		);
		
	if($_SESSION['USERGROUP_ID']=='1'):
		array_push($actions,'reset_status');
	endif;
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>