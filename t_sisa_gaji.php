<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Informasi Sisa Gaji <?php //echo $r['USER_FULLNAME'];?>
        <small>Anggota.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Sisa Pendapatan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Sisa Gaji Anggota</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#modal-uploadfile">
						<i class='fa fa-upload'></i>
						 Upload Data Sisa Gaji
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Sisa Gaji</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>NIP/NIK</label>
							  <input name="parent_id" id="parent_id" type="hidden">
							  <input name="user_nip" id="user_nip" type="text" class="form-control">
							</div>
							<div class="form-group">
							  <label>Nama</label>
							  <input name="user_name" id="user_name" type="text" class="form-control">
							</div>
							<div class="form-group">
							  <label>Nominal</label>
							  <input name="trx_nominal" id="trx_nominal" type="text" class="form-control" placeholder="Masukkan Nominal Uang">
							</div>							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Save</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="modal fade" id="modal-uploadfile" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-upload" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Pilih File yang Sesuai Template</h4>
						</div>
						<div class="modal-body">							
							<div class="form-group">
							  <label>Pilih File</label>
							  <input type="file" name="file_pendukung" id="file_pendukung">
							</div>							
							<a href="uploads/template/template_gaji.xlsx" target="_blank"><i class='fa fa-download'></i> Download Template</a>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit_upload">
								<i class='fa fa-upload'></i> Upload
							</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>


			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<h4 class="text-center"><b>DATA SISA GAJI ANGGOTA</b></h4>
					<hr/>
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>NIP</th>
							<th width="150">Nama Lengkap</th>
							<th>Kecamatan</th>
							<th width="200">Sisa Gaji</th>	
							<th width="200">Tanggal Update</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Sisa Gaji Anggota
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		
		
		$(document).ready(function() { 		
			
			// Tampilkan tabel
			var oTable = $('#sample_3').DataTable( {
				responsive : true,
				//"bJQueryUI": true,				
				"aoColumns": [
					{
						"sClass": "center",
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* no */
					,{
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* aksi */
					,{"bSearchable": true}	/* NIP */
					,{
						"bSearchable": true,
						"sClass": "center"
					} /* Nama Lengkap */
					,{
						"bSearchable": true,
						"sClass": "center"
					} /* Nama Lengkap */
					,{"sClass": "center"} /* gaji */ 	
					,{"sClass": "center"} /* tgl */ 									  
				],      
				"sPaginationType": "full_numbers",
				"aLengthMenu" : [10,20,30,40],
				"bRetrieve" : true,
				"bStateSave": false,
				"bProcessing": true,
				"bServerSide": true,
				"language": {
					"search": "Pencarian :",
					"sInfoEmpty": "Data Tidak Tersedia",
					"sZeroRecords": "Data Tidak Tersedia",
					"sInfo": "Jumlah Data _TOTAL_ ",
					"sInfoFiltered": " de _MAX_ registros"         
				},
				
				"sAjaxSource": "t_sisa_gaji_grid_ajax.php",
				"fnServerData": function ( sSource, aoData, fnCallback ) {
				  aoData.push( { "name": "aksi", "value": "table" } );		  
				  $.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				  } );          
				}
			});
						
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "t_sisa_gaji_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");							
							$("#parent_id").val(data.record.pemilik_saham_id);
							$("#user_name").val(data.record.pemilik_saham_nama);					
							$("#user_nip").val(data.record.pemilik_saham_nik);	
							$("#trx_nominal").val(data.record.pemilik_saham_sisa_gaji);							
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});

			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("t_sisa_gaji_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();	
				},"json");
						
			});
			
			function clear_form(){
				$("#user_nip").val("");
				$("#user_name").val("");
				$("#parent_id").val("");
				$("#trx_nominal").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});

			// function resetFile() {
			// 	const fl = document.querySelector('#file_pendukung');
			// 	fl.value = '';
			// }

			$("#modal-uploadfile").on("hidden.bs.modal", function () {
				// put your default event here
				// resetFile();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			$("#btn_submit_upload").click(function(e){	
				var frm_upload = $('#f-upload');
				frm_upload.submit();
				$("#modal-uploadfile").modal("hide");
				alert_custom("Uploading Process","modal-info");
			});
		});
	</script>
	<script src="./zing_upload_sisa_gaji.js"></script>