<?php
	include("config/datatables_kerja_harian.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'kegiatan_id',
		'kegiatan_date',
		'kegiatan_description',
		);
	$sIndexColumn = "kegiatan_id";
	
	$sQuery = "SELECT `kegiatan`.kegiatan_id
				,`kegiatan`.kegiatan_date
				,`kegiatan`.kegiatan_description
				,`s_user`.USER_FULLNAME
				,`s_user`.USER_ID
			FROM `kegiatan` LEFT JOIN 
			`s_user` ON `kegiatan`.kegiatan_user_id = `s_user`.USER_ID 
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('kegiatan_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'upload'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>