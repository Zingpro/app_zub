<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];

function post_data(){	
	include "config/koneksi_li.php";
	session_start();
	
	$purchase_id			= mysqli_real_escape_string($conn_db,$_POST['purchase_id']);
	$purchase_description	= mysqli_real_escape_string($conn_db,$_POST['purchase_description']);
	$purchase_date			= mysqli_real_escape_string($conn_db,$_POST['purchase_date']);
	
	$created_by = $_SESSION['USER_ID'];
    $created_at = date('Y-m-d H:i:s');
	
	if(empty($purchase_id)):
        $purchase_id = date('YmdHis')."_".rand(1,200);

		$q = "INSERT INTO t_purchase (
					purchase_id					
					,purchase_description
					,purchase_status
					,purchase_created_at
					,purchase_created_by
					,purchase_date
				) VALUES (
					'".$purchase_id."'
					,'".$purchase_description."'				
					,'new'
					,'".$created_at."'
					,'".$created_by."'
					,'".$purchase_date."'
				)";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE t_purchase SET
					purchase_description    = '".$purchase_description."'
					,purchase_date    = '".$purchase_date."'				
				WHERE purchase_id   = '".$purchase_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah";
	endif;
	
	echo json_encode($data);
}

function addcart(){	
	include "config/koneksi_li.php";
	session_start();
	
	$purchasedetail_id			= date("YmdHis")."_".rand(1,200);
	$purchasedetail_purchase_id	= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_purchase_id']);
	$purchasedetail_product_id	= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_product_id']);
	$purchasedetail_count		= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_count']);
	$purchasedetail_price		= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_product_price']);
	$purchasedetail_description	= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_description']);
	$purchasedetail_subtotal	= $purchasedetail_price * $purchasedetail_count;
	
	$created_by = $_SESSION['USER_ID'];
    $created_at = date('Y-m-d H:i:s');
	
	if(!empty($purchasedetail_purchase_id)):
        $purchasedetail_id = date('YmdHis')."_".rand(1,200);

		$q = "INSERT INTO t_purchasedetail (
					purchasedetail_id
					,purchasedetail_purchase_id					
					,purchasedetail_product_id
					,purchasedetail_count
					,purchasedetail_price
					,purchasedetail_subtotal
					,purchasedetail_description
				) VALUES (
					'".$purchasedetail_id."'
					,'".$purchasedetail_purchase_id."'
					,'".$purchasedetail_product_id."'				
					,'".$purchasedetail_count."'
					,'".$purchasedetail_price."'
					,'".$purchasedetail_subtotal."'
					,'".$purchasedetail_description."'
				)";
		$sql = mysqli_query($conn_db,$q);

		$q = "SELECT SUM(purchasedetail_subtotal) AS purchasedetail_sum 
				FROM t_purchasedetail 
				WHERE purchasedetail_purchase_id='".$purchasedetail_purchase_id."'";
		$sql = mysqli_query($conn_db,$q);	
		$rec = mysqli_fetch_array($sql);

		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
		$data['purchasedetail_sum'] = $rec['purchasedetail_sum'];
		
	endif;
	
	echo json_encode($data);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM t_purchasedetail WHERE purchasedetail_purchase_id = '".$data_id."'";
	$sql = mysqli_query($conn_db,$q);
	$num = mysqli_num_rows($sql);

	if($num > 0){
		$data['msg'] = "Maaf purchase memiliki detil item";
	}else{
		$q = "DELETE FROM t_purchase 
				   WHERE purchase_id = '".$data_id."'";		
		$sql = mysqli_query($conn_db,$q);
		$data['msg'] = "Transaksi Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	}

	echo json_encode($data);
}

function data_delete_detail(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];	
	$purchasedetail_purchase_id = $_POST['purchase_id'];

	$q = "DELETE FROM t_purchasedetail 
				WHERE purchasedetail_id = '".$data_id."'";		
	$sql = mysqli_query($conn_db,$q);

	$q = "SELECT SUM(purchasedetail_subtotal) AS purchasedetail_sum 
				FROM t_purchasedetail 
				WHERE purchasedetail_purchase_id='".$purchasedetail_purchase_id."'";
	$sql = mysqli_query($conn_db,$q);	
	$rec = mysqli_fetch_array($sql);

	$data['msg'] = "OK";
	$data['response'] = "Transaksi Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	$data['purchasedetail_sum'] = $rec['purchasedetail_sum'];

	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['purchase_id'];
	
	$q = "SELECT * FROM t_purchase 
			   WHERE purchase_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function cek_ktp($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM t_purchase 
				WHERE purchase_nik = '".$purchase_nik."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

function purchase_post(){
	include "config/koneksi_li.php";
	session_start();
	
	$purchasedetail_purchase_id	= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_purchase_id2']);
	$purchasedetail_payment_amount	= mysqli_real_escape_string($conn_db,$_POST['purchasedetail_payment_amount']);

	$q = "SELECT SUM(purchasedetail_subtotal) AS purchasedetail_sum 
				FROM t_purchasedetail 
				WHERE purchasedetail_purchase_id='".$purchasedetail_purchase_id."'";
	$sql = mysqli_query($conn_db,$q);	
	$rec = mysqli_fetch_array($sql);

	$money_changes = $rec['purchasedetail_sum'] - $purchasedetail_payment_amount;
	
	$q = "UPDATE t_purchase SET
				purchase_total    = '".$rec['purchasedetail_sum']."'
				,purchase_status    = 'post'
				,purchase_payment_amount = '".$purchasedetail_payment_amount."'	
				,purchase_money_changes = '".$money_changes."'				
			WHERE purchase_id   = '".$purchasedetail_purchase_id."'";
	// echo $q;
	$sql = mysqli_query($conn_db,$q);

	$data['msg'] = "OK";
	$data['response'] = "Posting Data Berhasil";

	echo json_encode($data);
}

function reset_status(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
		
	$q = "UPDATE t_purchase SET purchase_status='new' 
				WHERE purchase_id = '".$data_id."'";		
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = "Reset Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	
	echo json_encode($data);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='delete_detail'){
	data_delete_detail();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='addcart'){
	addcart();
}else if ($act=='purchase_post'){
	purchase_post();
}else if ($act=='reset_status'){
	reset_status();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>