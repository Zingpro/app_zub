<?php
	include("config/datatables_sale.php");
	session_start();
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'sale_id',
        'sale_date',
		'sale_description',
		'sale_total',
		'sale_payment_amount',
		'sale_status',
		);
	$sIndexColumn = "sale_id";
	
	$sQuery = "SELECT 
			t_sale.sale_id,
            t_sale.sale_date,
			t_sale.sale_description,
			t_sale.sale_total,
			t_sale.sale_payment_amount,
			t_sale.sale_status,
			t_sale.sale_created_by 
			FROM
			t_sale
			WHERE 1=1 ".$where." ";
	//echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array();
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
        ,'detail'
		);
	if($_SESSION['USERGROUP_ID']=='1'):
		array_push($actions,'reset_status');
	endif;
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>