
<?php
	include "config/koneksi_li.php";
	include "config/all_function.php";
?>
<html>
    <head>	
        <meta charset="utf-8">
        <title>List Produk</title>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    </head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="panel panel-default col-md-12 text-center">
                <div class="panel-body">
				<div class="pull-left">
                    <h2>List Produk Toko Kopsyah Ikhmal</h2></div>
					<div class="pull-right">
						<button class="btn btn-danger btn-lg" type="button">
							Pesanan Anda <span class="badge" id="zingcart_count">0</span>
						</button>
						<img src="dist/img/cart.png" width="50px">
					</div>
                </div>
            </div>                           
        </div>
        <div class="row text-center">
            
            <div class="form-group col-md-12">
                <label>Pencarian Produk :</label>
                <input type="text" name="keyword" id="keyword">
                <button class="btn btn-md btn-primary" id="btn_search"><i class="fa fa-search"></i> Cari</button>
            </div>
        </div> 
        <div id="product_page">

        </div>
    </div>

    <div class="modal fade" id="modal-uploadfile" style="display: none;">
        <div class="modal-dialog">
        <div class="modal-content">
            <form id="f-upload" method="post" action="#">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Pilih File yang berekstensi (jpg/png/gif) </h4>
                </div>
                <div class="modal-body">							
                    <div class="form-group">
                        <label>Pilih File</label>
                        <input type="file" name="file_pendukung" id="file_pendukung">
                        <input type="hidden" name="parent_id" id="parent_id">
                    </div>	
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password_id" id="password_id">                        
                    </div>	
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_submit_upload">
                        <i class='fa fa-upload'></i> Upload
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</body>
</html>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script>
    function loadPageProduct(str){
        $("#product_page")
				.load("list_product_page.php?limit=50&halaman=1&key="+str);	
    }
    
    $(document).ready(function() {
        loadPageProduct("");

        $("#btn_search").click(function(e){
            var str = $("#keyword").val();	
            loadPageProduct(str);
        });
    });
</script>
<!-- =============================================== -->
<div class="modal modal-info fade" id="modal-info" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Info</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
		
	<div class="modal modal-danger fade" id="modal-danger" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Error</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>	
		
	<div class="modal modal-success fade" id="modal-success" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Success Modal</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
		
	<script>
	
		function alert_custom(str,type){
			var mymodal = $('#'+type);
			mymodal.find('.modal-body').html(str);
			mymodal.modal('show');
		}
	</script>
	<script src="zing_cart.js"></script>
