<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen User <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan User dan Pegawai.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master User</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data User</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah User
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form User Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>NIP</label>
							  <input name="user_id" id="user_id" type="hidden">
							  <input name="user_nip" id="user_nip" type="text" class="form-control" placeholder="Masukkan NIP">
							</div>
							<div class="form-group">
							  <label>Username</label>
							  <input name="user_name" id="user_name" type="text" class="form-control" placeholder="Masukkan Username">
							</div>
							<div class="form-group">
							  <label>Password</label>
							  <input name="user_password" id="user_password" type="password" class="form-control" placeholder="Masukkan Password">
							</div>
							<div class="form-group">
							  <label>Nama Lengkap</label>
							  <input name="user_fullname" id="user_fullname" type="text" class="form-control" placeholder="Masukkan Nama Lengkap">
							</div>
							<div class="form-group">
							  <label>Email</label>
							  <input name="user_email" id="user_email" type="text" class="form-control" placeholder="Masukkan Email">
							</div>							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i>Add </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>NIP</th>
							<th width="150">Nama Lengkap</th>
							<th>Username</th>
							<th width="200">Usergroup</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data User pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		
		
		$(document).ready(function() { 		
			
			// Tampilkan tabel
			var oTable = $('#sample_3').DataTable( {
				responsive : true,
				//"bJQueryUI": true,				
				"aoColumns": [
					{
						"sClass": "center",
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* no */
					,{
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* aksi */
					,{"bSearchable": true}	/* NIP */
					,{
						"bSearchable": true,
						"sClass": "center"
					} /* Nama Lengkap */
					,{
						"bSearchable": true,
						"sClass": "center"
					} /* Nama Lengkap */
					,{"sClass": "center"} /* Usergroup */ 	 
								  
				],      
				"sPaginationType": "full_numbers",
				"aLengthMenu" : [10,20,30,40],
				"bRetrieve" : true,
				"bStateSave": false,
				"bProcessing": true,
				"bServerSide": true,
				"language": {
					"search": "Pencarian :",
					"sInfoEmpty": "Data Tidak Tersedia",
					"sZeroRecords": "Data Tidak Tersedia",
					"sInfo": "Jumlah Data _TOTAL_ ",
					"sInfoFiltered": " de _MAX_ registros"         
				},
				
				"sAjaxSource": "m_user_grid_ajax.php",
				"fnServerData": function ( sSource, aoData, fnCallback ) {
				  aoData.push( { "name": "aksi", "value": "table" } );		  
				  $.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				  } );          
				}
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_user_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "m_user_aksi.php?act=edit", { a: id}
					,function( data ){		
						$("#user_password").attr('readonly','readonly');
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							$("#user_id").val(data.record.USER_ID);
							$("#user_nip").val(data.record.USER_NIP);
							$("#user_name").val(data.record.USER_NAME);
							$("#user_fullname").val(data.record.USER_FULLNAME);
							$("#user_password").val(data.record.USER_PASSWORD);
							$("#user_email").val(data.record.USER_EMAIL);
							$("#lokasi_id").val(data.record.USER_POINTING_GPS);
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("m_user_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
				},"json");
								
			});
			
			function clear_form(){
				$("#user_nip").val("");
				$("#user_name").val("");
				$("#user_fullname").val("");
				$("#user_password").val("");
				$("#user_password").removeAttr("readonly");
				$("#user_email").val("");
				$("#user_id").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
		});
	</script>