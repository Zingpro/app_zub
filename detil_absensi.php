<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	
	$user_id = $_SESSION['USER_ID'];
	$user_fullname = $_SESSION['USER_FULLNAME'];
	
	$sql = "SELECT * FROM s_user WHERE USER_ID = '".$user_id."'";
	$q = mysql_query($sql);
	$r = mysql_fetch_array($q);
	
	$user_nip = $r['USER_NIP'];
	
	$sql = "SELECT * FROM t_absen WHERE absen_user_id = '".$user_id."' order by absen_id DESC";
	$q = mysql_query($sql);
	$r = mysql_fetch_array($q);
			
	$foto = $r['absen_foto'];
	$absen_date_time = $r['absen_date_time'];
	$absen_tanggal_indo = tanggal_indo($absen_date_time);
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detil Absensi : <?php //echo $r['USER_FULLNAME'];?>
        <small>Pastikan GPS Smartphone Anda Diaktifkan dengan mode tingkat presisi yang tinggi.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Absensi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Absensi Terakhir</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
		<style>
			#map
			{
				height: 200px !important;
				width: 100% !important;
				margin: 10px auto;
			}
		</style>
		
        <div class="box-body">
			<div class="row">
				<div class="col-md-4">	
					<div class="modal-body">
						<div class="form-group">
						  <label>NIP</label>
						  <input name="user_nip" id="user_nip" type="text" class="form-control" disabled value="<?php echo $user_nip;?>">
						</div>						
						<div class="form-group">
						  <label>Nama Lengkap</label>
						  <input name="user_fullname" id="user_fullname" type="text" class="form-control" disabled value="<?php echo $user_fullname; ?>">
						</div>
						<div class="form-group">
						  <label>Waktu Absen</label>
						  <input name="user_email" id="user_email" type="text" class="form-control" disabled value="<?php echo $absen_tanggal_indo; ?>">
						</div>
						<div class="form-group">
							<label>Foto</label>
							<div id="imgs">        
							</div>
						</div>
					</div>
										
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Detil Absensi 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
	
	<script>
		function genImage(strData) {
			var img = document.createElement('img');
			img.src = strData;
			return img;
		}
		
		var	$imgs = document.getElementById('imgs');
		var strData = "<?php echo $foto; ?>";
		$imgs.appendChild(genImage(strData));
	</script>
	
	