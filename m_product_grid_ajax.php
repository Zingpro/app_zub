<?php
	include("config/datatables_product.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'product_id',
		'product_code',
		'product_name',
		'product_description',
		'product_price_sale',
		'product_stock'
		);
	$sIndexColumn = "product_id";
	
	$sQuery = "SELECT
			m_product.product_id,
			m_product.product_code,
			m_product.product_name,
			m_product.product_description,
			m_product.product_price_sale,
			m_product.product_stock
			FROM
			m_product 
			WHERE 1=1 ".$where." ";
	
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('product_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		// 'delete',
		'edit'
		,'upload'
		,'detail'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>