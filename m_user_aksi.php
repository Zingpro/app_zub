<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];


function load_kab(){
	include "config/koneksi_li.php";
	$data_id = $_POST['data_id'];
	if(!empty($data_id)):
		$q = "SELECT * FROM d_regencies 
				WHERE province_id = '".$data_id."'";	
		
		$sql = mysqli_query($conn_db,$q);
		
		
		if(mysqli_num_rows($sql)>0):
			$data['msg'] = "OK";
			$data['record'] = array();
			
			while($r = mysqli_fetch_array($sql)):
				array_push($data['record'],$r);
			endwhile;
		else:
			$data['msg'] = "Anything error at fetch data";
		endif;
		echo json_encode($data);
	endif;	
}

function load_kec(){
	include "config/koneksi_li.php";
	$data_id = $_POST['data_id'];
	
	$q = "SELECT * FROM d_districts 
			WHERE regency_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = array();
		
		while($r = mysqli_fetch_array($sql)):
			array_push($data['record'],$r);
		endwhile;
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function cek_ktp($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM m_wakif 
				WHERE wakif_ktp_nomor = '".$wakif_ktp_nomor."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

function upload_foto($uploaddir = "./uploads/foto/"){
	include "config/koneksi_li.php";
	$data = array();
	 
	$error = false;
	$files = array();
	
	foreach($_FILES as $file)
	{
		$nama_file = date('Y.m.d')."_".basename($file['name']);
		if(move_uploaded_file($file['tmp_name'], $uploaddir .$nama_file))
		{
			$files[] = $uploaddir.$file['name'];
			$user_id = $_GET['id'];
			
			$q = "SELECT * FROM s_user WHERE USER_ID = '".$user_id."'";
			$sql = mysqli_query($conn_db,$q);
			$r = mysqli_fetch_array($sql);
			$old_foto = $r['USER_PROFILE_PHOTO'];
			
			if(file_exists("./uploads/foto/".$r['USER_PROFILE_PHOTO'])){
				unlink("./uploads/foto/".$r['USER_PROFILE_PHOTO']);
			}
			
			$q = "UPDATE s_user SET USER_PROFILE_PHOTO = '".$nama_file."' WHERE USER_ID = '".$user_id."'";
			mysqli_query($conn_db,$q)or die(mysql_error());
		}
		else
		{
			$error = true;
		}				
	}
	
	$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
	$data = array(
		'success' => 'OK'
		,'msg' => 'Foto Telah Di Upload'
		,'formData' => $_POST
	);	

	echo json_encode($data);
}

function post_data(){
	include "config/koneksi_li.php";
	
	$user_id = $_POST['user_id'];
	$user_nip = $_POST['user_nip'];
	$user_name = $_POST['user_name'];
	$user_fullname = $_POST['user_fullname'];
	$user_email  = $_POST['user_email'];
	$lokasi_id  = $_POST['lokasi_id'];
	$user_password  = md5($_POST['user_password']);
	$user_active = 'y';
	
	if(empty($user_id)):
		$q = "INSERT INTO s_user (
					USER_NIP
					,USER_NAME
					,USER_FULLNAME
					,USER_EMAIL
					,USERGROUP_ID
					,USER_POINTING_GPS
					,USER_PASSWORD
					,USER_ACTIVE
				) VALUES (
					'".$user_nip."'
					,'".$user_name."'
					,'".$user_fullname."'				
					,'".$user_email."'
					,'2'
					,'".$lokasi_id."'
					,'".$user_password."'
					,'".$user_active."'
				)";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE s_user SET
					USER_NIP = '".$user_nip."'
					,USER_NAME = '".$user_name."'
					,USER_FULLNAME = '".$user_fullname."'
					,USER_EMAIL = '".$user_email."'
					,USER_POINTING_GPS = '".$lokasi_id."'
					,USER_PASSWORD = '".$user_password."'
				WHERE USER_ID = '".$user_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah";
	endif;
	
	echo json_encode($data);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "DELETE FROM s_user 
			   WHERE USER_ID = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = mysql_affected_rows();
	echo json_encode($data);
}

function load_user(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM s_user 
			   WHERE USER_ID = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = "OK";
	$data['record'] = mysqli_fetch_array($sql);
	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM s_user 
			   WHERE USER_ID = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function cek_username_exist($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM s_user 
				WHERE USER_NAME = '".$username."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

function get_old_username($user_id = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM s_user 
				WHERE USER_ID = '".$user_id."'";				
	$sql = mysqli_query($conn_db,$q);
	$r = mysqli_fetch_array($sql);
	
	if(mysqli_num_rows($sql) > 0):
		return $r['USER_NAME'];	
	else:
		return "";
	endif;	
}

function update_profil(){
	include "config/koneksi_li.php";
	
	$user_id = $_POST['user_id'];
	$user_nip = $_POST['user_nip'];
	$user_name = $_POST['user_name'];
	$user_fullname = $_POST['user_fullname'];
	$user_email  = $_POST['user_email'];
	
	$user_propinsi  = $_POST['wakif_propinsi'];
	$user_kabupaten  = $_POST['wakif_kabupaten'];
	$user_kecamatan  = $_POST['wakif_kecamatan'];
	$user_an_pejabat = $_POST['user_an_pejabat'];
	
	$user_password  = empty($_POST['user_password']) ? $_POST['user_password'] : md5($_POST['user_password']);
	
	$old_user_name = get_old_username($user_id);
	
	$data['response'] = "";
	$data['cek_user'] = cek_username_exist($user_name);
	
	if($old_user_name == $user_name):
		if(empty($user_password)):
			$q = "UPDATE s_user SET
							USER_NIP = '".$user_nip."'
							,USER_FULLNAME = '".$user_fullname."'
							,USER_EMAIL = '".$user_email."'
							,user_propinsi = '".$user_propinsi."'
							,user_kabupaten = '".$user_kabupaten."'
							,user_kecamatan = '".$user_kecamatan."'
						WHERE USER_ID = '".$user_id."'";
		else:
			$q = "UPDATE s_user SET
							USER_NIP = '".$user_nip."'
							,USER_FULLNAME = '".$user_fullname."'
							,USER_EMAIL = '".$user_email."'
							,USER_PASSWORD = '".$user_password."'
							,user_propinsi = '".$user_propinsi."'
							,user_kabupaten = '".$user_kabupaten."'
							,user_kecamatan = '".$user_kecamatan."'
						WHERE USER_ID = '".$user_id."'";
			$data['response'] = "Password Berhasil dirubah";
		endif;
		$sql = mysqli_query($conn_db,$q);
				
		
		$data['msg'] = "1";
		$data['response'] .= "<br/>Data Berhasil dirubah";		
		
	else:
		if($data['cek_user']['num']>0):
			$data['msg'] = "2";
			$data['response'] = "Username sudah digunakan, update kembali username anda";
		else:
			if(empty($user_password)):
				$q = "UPDATE s_user SET
							USER_NIP = '".$user_nip."'
							,USER_NAME = '".$user_name."'
							,USER_FULLNAME = '".$user_fullname."'
							,USER_EMAIL = '".$user_email."'	
							,user_propinsi = '".$user_propinsi."'
							,user_kabupaten = '".$user_kabupaten."'
							,user_kecamatan = '".$user_kecamatan."'
						WHERE USER_ID = '".$user_id."'";
				$data['response'] .= "<br/>Username Berhasil dirubah";
			else:
				$q = "UPDATE s_user SET
							USER_NIP = '".$user_nip."'
							,USER_NAME = '".$user_name."'
							,USER_FULLNAME = '".$user_fullname."'
							,USER_EMAIL = '".$user_email."'
							,USER_PASSWORD = '".$user_password."'
							,user_propinsi = '".$user_propinsi."'
							,user_kabupaten = '".$user_kabupaten."'
							,user_kecamatan = '".$user_kecamatan."'
						WHERE USER_ID = '".$user_id."'";
				$data['response'] .= "<br/>Password Berhasil dirubah";
			endif;
			
			$sql = mysqli_query($conn_db,$q);					
			$data['msg'] = "1";
			$data['response'] .= "<br/>Data Berhasil dirubah";
			
		endif;
	endif;
	echo json_encode($data);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='update_profil'){
	update_profil();
}else if ($act=='load_user'){
	load_user();
}else if ($act=='upload_foto'){
	upload_foto();
}else if ($act=='load_kab'){
	load_kab();
}else if ($act=='load_kec'){
	load_kec();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>