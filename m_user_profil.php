<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
	$q = "SELECT * FROM s_user WHERE USER_ID = '".$user_id."'";
	$sql = mysqli_query($conn_db,$q);
	$r = mysqli_fetch_array($sql);
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen User <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan User dan Pegawai.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master User</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Profil User</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row" style="margin-top:10px;">	
				<form id="f-update" method="post" action="#">
				<div class="col-md-3">					
					<div class="form-group">
					  <label>NIP/NIK</label>
					  <input name="user_id" id="user_id" value="" type="hidden">
					  <input name="user_nip" id="user_nip" type="text" class="form-control" placeholder="Masukkan NIP" value="" >
					</div>
					<div class="form-group">
					  <label>Username</label>
					  <input name="user_name" id="user_name" type="text" class="form-control" placeholder="Masukkan Username" value="">
					</div>
					<div class="form-group">
					  <label>Password</label>
					  <input name="user_password" id="user_password" type="password" class="form-control" placeholder="Masukkan Password">
						<span><i class='fa fa-info-circle'></i> Silahkan diisi, Jika Password akan dirubah</span>
					</div>
					<div class="form-group">
					  <label>Nama Lengkap</label>
					  <input name="user_fullname" id="user_fullname" type="text" class="form-control" placeholder="Masukkan Nama Lengkap" value="">
						
					</div>
					<div class="form-group">
					  <label>Email</label>
					  <input name="user_email" id="user_email" type="text" class="form-control" placeholder="Masukkan Email" value="">
					</div>					
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  <label>Propinsi</label>
					  <select name="wakif_propinsi" id="wakif_propinsi" class="form-control select2" placeholder="Pilih Propinsi" style="width: 100%;">
						<?php
							$qs = "SELECT * FROM d_provinces";
							$sqls = mysqli_query($conn_db,$qs);
							while($r = mysqli_fetch_array($sqls)){
						?>
							<option value="<?php echo $r['id'];?>"><?php echo $r['name'];?></option>
						<?php
							}
						?>
					  </select>
					</div>
					<div class="form-group">
					  <label>Kabupaten</label>
					  <select name="wakif_kabupaten" id="wakif_kabupaten" class="form-control select2" style="width: 100%;">
						
					  </select>
					</div>
					<div class="form-group">
					  <label>Kecamatan</label>
					  <select name="wakif_kecamatan" id="wakif_kecamatan" class="form-control select2" style="width: 100%;">
						
					  </select>
					</div>
					
				</div>
				</form>
				<div class="col-md-3">
					<div class="form-group">
						<label>Foto Profil :</label>
						<div id="wrap_foto" style="border:solid 1px #ccc;">
							<img width="100%" id="user_foto_profile" src="uploads/foto/default.jpg"/>
						</div>
					</div>
					<form id="f-upload" method="post" action="#" enctype="multipart/form-data">
						<div class="form-group">
						  <label>Upload Foto Baru</label>
						  <input type="file" name="file_pendukung" id="file_pendukung">
						  <input name="media_id" id="media_id" type="hidden">
						  <p class="help-block">Pilih dokumen pendukung dengan format image.</p>
						</div>																			
					
						<button type="button" class="btn btn-primary" id="btn_submit_upload">
							<i class='fa fa-upload'></i> Upload
						</button>
						
					</form>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">	
					<button id="btn_submit" type="button" class="btn btn-primary btn-lg" >
						<i class='fa fa-save'></i>
						 Update Profil
					</button>
				</div>
			</div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Profil User pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		function loadKab(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_user_aksi.php?act=load_kab",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}						
					}
				}
			});
		}
		
		function loadKec(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_user_aksi.php?act=load_kec",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}
					}						
				}
			});
		}
		
		function initial_select(){					
			var prop = $('#wakif_propinsi').val();			
			
			$.when(loadKab(prop,"wakif_kabupaten")).done(function(){
				var kab = $('#wakif_kabupaten').val();
				loadKec(kab, "wakif_kecamatan");
			});
		}
		
		function load_user(id_user){
			$.post( "m_user_aksi.php?act=load_user", { a: id_user}
				,function( data ){								
					if(data.msg=="OK"){
						$("#user_id").val(data.record.USER_ID);
						$("#media_id").val(data.record.USER_ID);
						
						$("#user_nip").val(data.record.USER_NIP);
						$("#user_fullname").val(data.record.USER_FULLNAME);
						$("#user_name").val(data.record.USER_NAME);
						$("#user_email").val(data.record.USER_EMAIL);
						$("#user_password").val("");
						$("#user_an_pejabat").val(data.record.user_an_pejabat);
						$("#user_an_pejabat_nip").val(data.record.user_an_pejabat_nip);
						
						$("#wakif_propinsi").val(data.record.user_propinsi);
						$('#wakif_propinsi').select2().trigger('change');
						
						if(data.record.USER_PROFILE_PHOTO){
							document.getElementById('user_foto_profile').src = 'uploads/foto/'+data.record.USER_PROFILE_PHOTO;
						}
						
						$.when(
							loadKab(data.record.user_propinsi,"wakif_kabupaten")
						).done(function(){
							$("#wakif_kabupaten").val(data.record.user_kabupaten);
							$('#wakif_kabupaten').select2().trigger('change');
							$.when(									
								loadKec(data.record.user_kabupaten,"wakif_kecamatan")
							).done(function(){
								$("#wakif_kecamatan").val(data.record.user_kecamatan);
								$('#wakif_kecamatan').select2().trigger('change');
							});
						});																					
						
					}else{
						alert_custom(data.msg,"modal-info");
					}
				},"json");
		}
		
		$('#wakif_propinsi').on('select2:select', function (e) {
			var data_id = $(this).val();				
			return $.when(
				loadKab(data_id,"wakif_kabupaten")
			).done(function(){				
				loadKec($("#wakif_kabupaten").val(),"wakif_kecamatan");				
			});
		});
		
		$('#wakif_kabupaten').on('select2:select', function (e) {
			var data_id = $(this).val();
			return loadKec(data_id,"wakif_kecamatan");
		});
			
		$(document).ready(function() { 		
			initial_select();
			load_user(<?php echo $user_id;?>);
			
			var frm = $('#f-update');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				alert_custom("Loading process","modal-info");
				
				$.post("m_user_aksi.php?act=update_profil", dataform, function(data){
					$("#modal-info").modal("hide");
					
					if(data.msg == "1"){
						alert_custom(data.response,"modal-success");
					}else if(data.msg == "2"){
						alert_custom(data.response,"modal-info");
					}else{
						alert_custom(data.response,"modal-danger");
					}
				},"json");
								
			});
			
			
			$("#btn_submit").click(function(e){
				frm.submit();				
			});
									
		});
		
		$("#btn_submit_upload").click(function(e){	
			var frm_upload = $('#f-upload');
			frm_upload.submit();
			alert_custom("Uploading Process","modal-info");
		});
	</script>
	<script src="./zing_user_profil_upload_foto.js"></script>