<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen Wakaf <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data Wakaf.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master Wakaf</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Wakaf</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah Transaksi Wakaf Baru
					</button>
				</div>
			</div>
								
			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th width="100" align="center">Aksi</th>
							<th width="100" align="center">Report</th>
							<th>No. Ikrar Wakaf</th>
							<th width="100">Obyek</th>
							<th>Sertifikat</th>
							<th width="200">Alamat</th>
							<th width="200">Keperluan</th>	
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Wakaf pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
						
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{"bSearchable": true}	/* NIP */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{"sClass": "center"} /* Usergroup */ 	 
				,{"sClass": "center"} /* Usergroup */ 			  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "m_data_wakaf_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
			
		$(document).ready(function() { 		
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_wakif_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-print-w2", function(e) {
				var id = $(this).attr("lang");
				window.open("report/v_report_w2.php?id="+id,
							"_blank",
							"toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=100,width=500,height=500");		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-print-w2a", function(e) {
				var id = $(this).attr("lang");
				window.open("report/v_report_w2a.php?id="+id,
							"_blank",
							"toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=100,width=500,height=500");		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-print-wt1", function(e) {
				var id = $(this).attr("lang");
				window.open("report/v_report_wt1.php?id="+id,
							"_blank",
							"toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=100,width=500,height=500");		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "m_wakif_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							
							$("#wakif_id").val(data.record.wakif_id);
							$("#wakif_nama").val(data.record.wakif_nama);
							$("#wakif_agama").val(data.record.wakif_agama);
							$("#wakif_ktp_nomor").val(data.record.wakif_ktp_nomor);
							$("#wakif_pekerjaan").val(data.record.wakif_pekerjaan);
							$("#wakif_jabatan").val(data.record.wakif_jabatan);
							$("#wakif_alamat").val(data.record.wakif_alamat);
							$("#wakif_tempat_lahir").val(data.record.wakif_tempat_lahir);
							$("#wakif_tanggal_lahir").val(data.record.wakif_tanggal_lahir);							
							
							$("#wakif_propinsi").val(data.record.wakif_propinsi);
							$('#wakif_propinsi').select2().trigger('change');
							
							$.when(
								loadKab(data.record.wakif_propinsi,"wakif_kabupaten")
							).done(function(){
								$("#wakif_kabupaten").val(data.record.wakif_kabupaten);
								$('#wakif_kabupaten').select2().trigger('change');
								$.when(									
									loadKec(data.record.wakif_kabupaten,"wakif_kecamatan")
								).done(function(){
									$("#wakif_kecamatan").val(data.record.wakif_kecamatan);
									$('#wakif_kecamatan').select2().trigger('change');
								});
							});							
																												
							$("#wakif_kelurahan").val(data.record.wakif_kelurahan);
				
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
		});
	</script>