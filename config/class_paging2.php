<?php
class Paging2
{
// Fungsi untuk mencek halaman dan posisi data
	function cariPosisi($batas)
	{
		if(empty($_GET[halaman])){
			$posisi=0;
			$_GET[halaman]=1;
		}
		else{
			$posisi = ($_GET[halaman]-1) * $batas;
		}
		return $posisi;
	}

// Fungsi untuk menghitung total halaman
	function jumlahHalaman($jmldata, $batas)
	{
		$jmlhalaman = ceil($jmldata/$batas);
		return $jmlhalaman;
	}

// Fungsi untuk link halaman 1,2,3 ... Next, Prev, First, Last
	function navHalaman($halaman_aktif, $jmlhalaman)
	{
		$link_halaman = "";
		$id = $_GET[id];
		// Link First dan Previous
		if ($halaman_aktif > 1)
		{
			$previous = $halaman_aktif-1;
			$link_halaman .= "<a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=1&id=$id><< First</a> | ";
			$link_halaman .= "<a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$previous&id=$id>< Previous</a> | ";
		}else
		{
			$link_halaman .= "<< First | < Previous | ";
		}
		
		// Link halaman 1,2,3, ...
		// angka awal
		$link_halaman .=($halaman_aktif > 3 ? "... " : " ");//Ternary Operator
		
		for ($i=$halaman_aktif-2; $i<$halaman_aktif; $i++)
		{
			if ($i < 1)
				continue;
			$link_halaman .= "<a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$i&id=$id>$i</a> ";
		}
		
		//angka tengah
		$link_halaman .= "<b>$halaman_aktif</b> ";
		for($i=$halaman_aktif+1;$i<($halaman_aktif+10);$i++)
		{
			if ($i > $jmlhalaman)
				break;
			$link_halaman .= "<a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$i&id=$id>$i</a> ";
		}
				
		// angka akhir
		$link_halaman .= ($halaman_aktif + 2 < $jmlhalaman ? "... <a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$jmlhalaman&id=$id>$jmlhalaman</a> " :" ");

		if ($halaman_aktif < $jmlhalaman)
		{
			$next=$halaman_aktif+1;
			$link_halaman .= " | <a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$next&id=$id>Next ></a> ";
			$link_halaman .= " | <a href=$_SERVER[PHP_SELF]?module=$_GET[module]&halaman=$jmlhalaman&id=$id>Last >></a> ";
		}else{
			$link_halaman .= "| Next > | Last >>";
		}		
		
		return $link_halaman;
	}
}
?>
