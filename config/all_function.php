<?php	
		
	function cek_session(){		
		if(empty($_SESSION['USERNAME_ADMIN'])){
			header('location:login.php');
		}
	}

	function generate_dateid(){
		$str = date('YmdHis')."_".rand(1,1000);
		return $str;
	}
	
	function hari_ini(){
		$hari = date ("D");	 
		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
			break;	 
			case 'Mon':			
				$hari_ini = "Senin";
			break;	 
			case 'Tue':
				$hari_ini = "Selasa";
			break;	 
			case 'Wed':
				$hari_ini = "Rabu";
			break;	 
			case 'Thu':
				$hari_ini = "Kamis";
			break;	 
			case 'Fri':
				$hari_ini = "Jumat";
			break;	 
			case 'Sat':
				$hari_ini = "Sabtu";
			break;			
			default:
				$hari_ini = "Tidak di ketahui";		
			break;
		} 
		return "<b>" . $hari_ini . "</b>"; 
	}

	function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$tanggal = str_replace("/","-",$tanggal);
		$split_time = explode(' ', $tanggal);
		$split = explode('-', $split_time[0]);
		
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}	
		
	$encrypt_mode = true;
	
	function encmd5($data=null,$encrypt_mode=null){
		if ($encrypt_mode){
			$data = md5($data);
		}
		return $data;
	}
	
	function generate_token($id=null,$key="zing"){
		$token = md5(md5($id).md5($key));
		return $token;
	}	
	
	function checking_token($id=null, $token=null, $key="zing"){		
		$cek = md5(md5($id).md5($key));
		
		if($token == $cek){
			return true;
		}else{
			return false;
		}
	}
	
	function get_list_menu_category($encrypt_mode){
		include 'koneksi_li.php';
		$qry = "SELECT * FROM kategori WHERE IDKATEGORI>0 order by IDKATEGORI ASC";	
        
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
			$parent_id = $r['IDKATEGORI'];
			if(empty($r['ICON'])){
				$icon = '<img src="images/menu/menu_faq2.png" style="float: left;"/>';
			}
			echo "<li id='cat_".$r['IDKATEGORI']."' class='li'>";
			
			if(strtoupper($r['NAMAKATEGORI'])=='PROFIL'):
				echo "<a href='#'>";
			else:
				echo "<a href='view.php?module=".encmd5('materi_kategori',$encrypt_mode);
				echo "&id=".$parent_id;
				echo "&token=".generate_token($parent_id);
				echo "' title=''>";	
			endif;
					
			echo $r['ICON'].$r['NAMAKATEGORI'];
			echo "</a>"; 
				$q_child = "SELECT * FROM materi WHERE IDKATEGORI='".$parent_id."' AND STATUS_PUBLISHED='y' ORDER BY MATERI_DATE_CREATED DESC LIMIT 5";
				$sql_child = mysqli_query($conn_db,$q_child);
				echo (mysqli_num_rows($sql_child)>0) ? "<ul>" : "";				
				$sql_child = mysqli_query($conn_db,$q_child);
				while($r_child = mysqli_fetch_array($sql_child)):
					echo "<li class='li'>";
					echo "<a href='view.php?module=".encmd5('pembelajaran',$encrypt_mode);
					echo "&id=".$r_child['IDMATERI'];
					echo "&token=".generate_token($r_child['IDMATERI']);
					echo "' title=''>";
					echo $r_child['ICON']." ".$r_child['JUDUL'];
					echo "</a></li>";
				endwhile;
				echo (mysqli_num_rows($sql_child)>0) ? "</ul>" : "";
			echo "</li>";
        }
	}
	
	function get_menu_by_category($encrypt_mode=true,$category_id=null){
		include 'koneksi_li.php';
		
		$q_child = "SELECT * FROM materi WHERE IDKATEGORI='".$category_id."' AND STATUS_PUBLISHED='y' ORDER BY MATERI_DATE_CREATED DESC LIMIT 5";
		$sql_child = mysqli_query($conn_db,$q_child);
		
		while($r_child = mysqli_fetch_array($sql_child)):
			echo "<li class='li'>";
			echo "<a href='view.php?module=".encmd5('pembelajaran',$encrypt_mode);
			echo "&id=".$r_child['IDMATERI'];
			echo "&token=".generate_token($r_child['IDMATERI']);
			echo "' title=''>";
			echo $r_child['ICON']." ".$r_child['JUDUL'];
			echo "</a></li>";
		endwhile;		
	}
	
	function get_config_val($config_name){
		include 'koneksi_li.php';
		$qry = "SELECT * FROM s_config WHERE config_name LIKE '%".$config_name."%' ";
        $sql = mysqli_query($conn_db,$qry);
		$ff = mysqli_fetch_array($sql);
		return $ff['config_value'];		
	}
	
	function select_usergroup($name=null,$class=null,$default_val=null,$table_name='s_usergroup'){
        include 'koneksi_li.php';
        $qry = "SELECT  * FROM `".$table_name."` WHERE USERGROUP_ACTIVE='y'";
		//echo "<div class=\"form-group\">";
        echo "<select name='".$name."' id='".$name."' class=\"selectpicker\" data-live-search=\"true\" title=\"Pilihlah Usergroup \">";
        
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
			$id = $r["USERGROUP_ID"];
            $content = $r["USERGROUP_NAME"];            
            
			if($default_val==$id){
				echo "<option value='".$id."' selected>".$content."</option>"; 
			}else{
				echo "<option value='".$id."'>".$content."</option>"; 
			}            
        }        
        echo "</select>";    
    }
	
	
    function select_user($name=null,$class=null,$default_val=null,$table_name='v_user'){
        include 'koneksi_li.php';
		session_start();
		$user_id = $_SESSION['USER_ID'];
		
		$qry = "SELECT  * FROM `".$table_name."` WHERE USERGROUP_ACTIVE='y'";
		        
		//echo "<div class=\"form-group\">";
        echo "<select name='".$name."' id='".$name."' class=\"".$class." selectpicker\" data-live-search=\"true\" title=\"Pilihlah Peneliti Terkait \">";
        echo "<option value='all' selected>Tampilkan Semua</option>"; 
		
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
			$id = $r["USER_ID"];
            $content = $r["USER_FULLNAME"]." <span style='font-size:0.5em;'>(".$r["USER_NAME"].")</span>";            
            
			if($default_val==$id){
				echo "<option value='".$id."' selected>".$content."</option>"; 
			}else{
				echo "<option value='".$id."'>".$content."</option>"; 
			}            
        }        
        echo "</select>";    
    }
	
    function select_kategori($name=null,$class=null){
        include 'koneksi_li.php';
        $qry = "SELECT * FROM kategori order by IDKATEGORI ASC";
        echo "<select name='".$name."' class='".$class."'>";
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
            echo "<option value='".$r['IDKATEGORI']."'>".$r['NAMAKATEGORI']."</option>";
        }
        echo "</select>";
    }
	
	function select_media_kategori($name=null,$class=null){
        include 'koneksi_li.php';
        $qry = "SELECT * FROM tb_media_category order by media_category_id ASC";
        echo "<select name='".$name."' id='".$name."' class=\"".$class." selectpicker\" data-live-search=\"true\" title=\"Pilih Kategori Media\">";
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
            echo "<option value='".$r['media_category_id']."'>".$r['media_category_name']."</option>";
        }
        echo "</select>";
    }
	
	function select_tb($name=null,$class=null,$fieldID,$fieldDisplay,$tablename){
		include 'koneksi_li.php';
		$qry = "SELECT * FROM `".$tablename."`";
		//echo $q;
		echo "<select name='".$name."' id='".$name."' class='".$class."'>";
        $sql = mysqli_query($conn_db,$qry);
        while($r = mysqli_fetch_array($sql)){
            echo "<option value='".$r[$fieldID]."'>".$r[$fieldDisplay]."</option>";
        }
        echo "</select>";
	}
	
    function select_tahun($name=null,$class=null){
        echo "<select name='".$name."' class='".$class."'>";
                $tahun = date('Y');
                for($i=$tahun;$i>($tahun-5);$i--){
                echo "<option value='$i'>$i</option>";
                }
        echo "</select>";
    }
	
	function select_bulan($name=null,$class=null){
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
        $i = 1;
		echo "<select name='".$name."' id='".$name."' class='".$class."'>";
        foreach($bulan as $r):			
			echo "<option value='".$i."'>".$i.". ".$r."</option>";
			$i++;
		endforeach;
		
        echo "</select>";
    }
    
	function select_agama($name=null,$class=null,$default_val=null,$addon=null){
        echo "<select name='".$name."' id='".$name."' class='".$class."'>";                
			echo "<option value='Islam'>Islam</option>";  
			echo "<option value='Hindu'>Hindu</option>";  
			echo "<option value='Budha'>Budha</option>";  
			echo "<option value='Katolik'>Katolik</option>"; 
			echo "<option value='Protestan'>Protestan</option>"; 
			echo "<option value='Konghuchu'>Konghuchu</option>"; 	
        echo "</select>";
    }
		
	function select_jk($name=null,$class=null){
        echo "<select name='".$name."' id='".$name."' class='".$class."'>";                
			echo "<option value='Pria'>Pria</option>";  
			echo "<option value='Wanita'>Wanita</option>";
        echo "</select>";
    }
			
	function create_view_v_user(){
		include 'koneksi_li.php';
		$q = "CREATE VIEW IF NOT EXISTS `v_user` AS SELECT
				s_user.USER_ID,
				s_user.USER_NAME,
				s_user.USER_PASSWORD,
				s_user.USER_FULLNAME,
				s_user.USER_EMAIL,
				s_user.USER_PHONE,
				s_user.USER_ACTIVE,
				s_user.USER_PROFILE_PHOTO,
				s_user.USERGROUP_ID,
				s_user.USER_DATE_CREATED,
				s_usergroup.USERGROUP_NAME,
				s_usergroup.USERGROUP_ACTIVE
			FROM
				s_user
			LEFT JOIN s_usergroup ON s_usergroup.USERGROUP_ID = s_user.USERGROUP_ID ";
		$sql = mysqli_query($conn_db,$q);		
	}
	
	function create_view_v_materi(){
		include 'koneksi_li.php';
		$q = "
		SELECT
			materi.IDMATERI,
			materi.IDKATEGORI,
			materi.JUDUL,
			materi.ISIMATERI,
			materi.ICON,
			materi.MATERI_DATE_CREATED,
			materi.MATERI_DATE_PUBLISHED,
			materi.MATERI_DATE_UPDATED,
			materi.MATERI_CREATED_BY,
			materi.MATERI_UPDATED_BY,
			materi.STATUS_PUBLISHED,
			materi.PUBLISHED_BY,
			materi.MATERI_THUMBNAIL,
			s_user.USER_ID,
			s_user.USER_NAME,
			s_user.USER_PASSWORD,
			s_user.USER_FULLNAME,
			s_user.USER_EMAIL,
			s_user.USER_PHONE,
			s_user.USER_ACTIVE,
			s_user.USER_PROFILE_PHOTO,
			s_user.USERGROUP_ID,
			s_user.USER_DATE_CREATED,
			kategori.NAMAKATEGORI,
			kategori.DISPLAY
			FROM
			materi
			LEFT JOIN s_user ON s_user.USER_ID = materi.PUBLISHED_BY
			LEFT JOIN kategori ON kategori.IDKATEGORI = materi.IDKATEGORI  ";
		$sql = mysqli_query($conn_db,$q);	
	}
	
	function generate_image_resize_base(){
		session_start();
		include("PHP_image_resize/smart_resize_image.function.php");
		
		$web_folder = 'user';
		$home_dir = 'http://'.$_SERVER['HTTP_HOST'].'/'.$web_folder;
		$directory = './';
		
		$content = glob($directory . "{*.jpg,*.JPG,*.jpeg,*.png,*.PNG}", GLOB_BRACE);
		
		foreach($content as $f)
		{        
			$nama_file = str_replace($directory,'', $f);
			$nama_file = str_replace('\'', '\\\'', $f);
			$nama_file_porsi = str_replace(' ', '', $f);
			echo "<a href='$f'>".$nama_file."</a><br/>";					
		}
	}

	function get_pagination_page($query=null,$limit=50,$active=1){		
		include 'koneksi_li.php';
		
		$sql = mysqli_query($conn_db,$query);
		$num_row = mysqli_num_rows($sql); 

		$total_records = $num_row;  
		$total_pages = ceil($total_records / $limit); 

		$out = "<div align=\"center\">";
		$out .= "<ul class='pagination text-center' id='pagination'>";

		if(!empty($total_pages)):
			for($i=1; $i<=$total_pages; $i++):  
				if($i == $active):
	            	$out .= "<li class='active'  id=".$i.">";
	            	$out .= "<span class='per_page' lang='".$i."'>".$i."</span>";
	            	$out .= "</li>";
				else:
					$out .= "<li id=".$i."><span class='per_page' lang='".$i."'>".$i."</span></li>";
				endif;			
			endfor;
		endif;  
		$out .= "</div>";
		return $out;
	}

	function get_profile($field){
		include 'koneksi_li.php';
		session_start();
		$qry = "SELECT * FROM `s_user` WHERE USER_ID='".$_SESSION['USER_ID']."'";
        $sql = mysqli_query($conn_db,$qry);
		$r = mysqli_fetch_array($sql);
		$data = $r[$field];
		return $data ;
	}
?>