<?php
	function IntervalDays($CheckIn,$CheckOut){
		$CheckInX = explode("-", $CheckIn);
		$CheckOutX =  explode("-", $CheckOut);
		$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[2],$CheckInX[0]);
		$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[2],$CheckOutX[0]);
		$interval =($date2 - $date1)/(3600*24);
		// returns numberofdays
		return  $interval ;
	}
	
	$range_license = 200;
	$date1 = "2019-11-22";
	$date2 = date("Y-m-d");
	$selisih = IntervalDays($date1,$date2);
	
	$remaining = $range_license - $selisih;
	if($remaining < 5){
		echo "<script>alert('Your License Expired after ".$remaining." days later');</script>";
	}
	
	if($selisih >= $range_license){
		header("location : config/p_locked.php");
	}
?>
