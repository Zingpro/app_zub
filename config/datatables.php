<?php 
/**
 * DataTables CodeIgniter implementation
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Datatables CI
 * @author    Adif spidey354@gmail.com
 * @version   0.1
 * Copyright (c) 2012 Adi Dwi IF  (http://datatables.net)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
*/
class Datatables
{
	
	//aColumns
	var $aColumns = array();
	//sIndexColumn
	var $sIndexColumn = null;
	//sTable
	var $sTable = null;
	//sLimit
	var $sLimit = null;
	
	//skip_cols
	var $skipCols = array();
	
	//sFuntions
	var $sFunctions = array();
	
	//actions
	var $actions = null;
	
	/**
	* Constructor
	* 
	* @access	public
	*/	
	public function DataTables()
    {
		
	}
	
	public function params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions){
		$this->aColumns = $aColumns;
		$this->sIndexColumn = $sIndexColumn;
		$this->sTable = $sTable;
		$this->skipCols = $skipCols;
		$this->sFunctions = $sFunctions;
		$this->actions = $actions;
	}
	
	public function build_json(){
		$aColumns = $this->aColumns;
		$sIndexColumn = $this->sIndexColumn;
		$sTable = $this->sTable;
		$skipCols = $this->skipCols;
		$sFunctions = $this->sFunctions;
		$actions = $this->actions;
		
		include("koneksi.php");
		// Paging mysql_real_escape_string
		$sLimit = "";
		if ( isset( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1' ){
			$sLimit = "LIMIT ".( $_POST['iDisplayStart'] ).", ".
				( $_POST['iDisplayLength'] );
		}
		
		// Ordering
		$sOrder = "";
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
		
		// Filtering
		$sWhere = "";
		if ( $_POST['sSearch'] != "" ){
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".( $_POST['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".($_POST['sSearch_'.$i])."%' ";
			}
		}
		// SQL
		$sQuery = "
			SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM   $sTable
			$sWhere
			$sOrder
			$sLimit
		";
		//echo $sQuery;
		
		$rResult = mysql_query($sQuery );
				
		/* Data set length after filtering */
		$sQuery = "
			SELECT FOUND_ROWS() AS TOTAL
		";
		$rResultFilterTotal = mysql_query($sQuery);
		$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
		$iFilteredTotal = $aResultFilterTotal['TOTAL'];
		/* Total data set length */
		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS TOTAL
			FROM   $sTable
		";
		$rResultTotal = mysql_query( $sQuery);
		$aResultTotal = mysql_fetch_array($rResultTotal);
		$iTotal = $aResultTotal['TOTAL'];
		
		// Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		$no = 0;
		//var_dump($dtResult);
		while($aRow = mysql_fetch_array($rResult)){
			$row = array();
			$row[] = ++$no.'.'; 
			for ( $i=0 ; $i<count($aColumns) ; $i++ ){
				if ( in_array($aColumns[$i],$skipCols)){ // Check skip colums
					continue;
				}
				else if( array_key_exists($aColumns[$i],$sFunctions)){ // check exclusive function to field
					
					eval("\$row[] = ".sprintf($sFunctions[$aColumns[$i]],$aRow[ $aColumns[$i] ]));
				}				
				else if ( $aColumns[$i] != ' ' )
				{
					// Hook for special cols
					if(($aColumns[$i] == 'rtrw_order')||($aColumns[$i] == 'slide_position')||($aColumns[$i] == 'peta_position')){ // for order/urutan
						if($aRow[$aColumns[$i]] == 1){
							if($iTotal > 1){
								$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-down"></span>';
							}else{
								$row[] = $aRow[ $aColumns[$i] ];
							}							
						}
						else if($aRow[$aColumns[$i]] == $iTotal){
							$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-up"></span>';
						}
						else{
							$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-up"></span><span class="order-down"></span>';
						}
					}
					else if($aColumns[$i] == 'penelitian_file'){
						$content = "";
						if(empty($aRow[ $aColumns[$i] ])){
							$content = "Tidak Tersedia";
						}else{
							$content = "<a href='".$_SERVER['DOCUMENT_ROOT']."/admin/uploads/".$aRow[ $aColumns[$i] ]."' target='blank'><img src='images/document_pdf.png'></a>";
						}
						$row[] = $content;
					}
					else if($aColumns[$i] == 'FOTO'){
						$content = "";						
						if(empty($aRow[ $aColumns[$i] ])){
							$content = "<img class='foto_backend' src='./uploads/foto/profil.png'>";
						}else{
							$content = "<img class='foto_backend' src='./uploads/foto/".$aRow[ $aColumns[$i] ]."'>";
						}
						$row[] = $content;
					}
					else if($aColumns[$i] == 'AKTIF'){
						$content = "";							
						if(($aRow[ $aColumns[$i] ])=='1'){
							$content = "<a href='?module=user&id=".$aRow[$sIndexColumn]."&aktif=0'>[aktif]</a>";
						}else{
							$content = "<a href='?module=user&id=".$aRow[$sIndexColumn]."&aktif=1'>[non aktif]</a>";
						}
						$row[] = $content;
					}
					else{
						/* General output */
						$row[] = ($aRow[ $aColumns[$i] ]);
					}
				}
			}
			//var_dump($row);
			
			if($actions != NULL){
				$btn_actions = '';
				foreach($actions as $aksi){
					if($aksi=="delete_penelitian"):
						$btn_actions .= "<a href='deleterecord.php?module=penelitian&fieldID=penelitian_id&table=tb_penelitian&id=".$aRow[$sIndexColumn]."'><img src='images/delete.PNG'/></a>";
					elseif($aksi=="tambah_rekening"):
						$btn_actions .= "<a href='backend.php?module=rekening&id=".$aRow[$sIndexColumn]."'><img title='Tambah Rekening' src='../images/plat.gif'/></a>";						
					elseif($aksi=="delete_user"):
						$btn_actions .= '<button lang="'.$aRow[$sIndexColumn].'" lang2="'.$aRow['USER_NAME'].'" title="Delete User" class="jq-delete-user">delete</button>';						
					elseif($aksi=="edit_user"):
						$btn_actions .= "<button lang='".$aRow[$sIndexColumn]."' title='edit user' class='jq-edit-user'>edit</button>";						
					elseif($aksi=="view"):
						$btn_actions .= '<button lang="'.$aRow[$sIndexColumn].'" title="'.$aksi.'" class="jq-'.$aksi.'">detil</button>';
					elseif($aksi=="view_link"):
						$btn_actions .= '<a href="view.php?module=peneliti_profil&id='.$aRow[$sIndexColumn].'" class="linked">detil</a>';
					elseif($aksi=="upload_user_foto"):
						$btn_actions .= '<button title="Upload Foto" lang="'.$aRow[$sIndexColumn].'">Upload Foto</button>';
					else:
						$btn_actions .= $aksi;
					endif;
				}
				$row[] = $btn_actions;
				
			}
			$output['aaData'][] = $row;
		}
		//var_dump($output);
		return ($output);		
	}
	
	
}
?>