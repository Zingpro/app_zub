<?php 
/**
 * DataTables CodeIgniter implementation
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Datatables CI
 * @author    Adif spidey354@gmail.com
 * @version   0.1
 * Copyright (c) 2012 Adi Dwi IF  (http://datatables.net)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
*/
class Datatables
{
	
	//aColumns
	var $aColumns = array();
	//sIndexColumn
	var $sIndexColumn = null;
	//sTable
	var $sTable = null;
	//sLimit
	var $sLimit = null;
	
	//skip_cols
	var $skipCols = array();
	
	//sFuntions
	var $sFunctions = array();
	
	//actions
	var $actions = null;
	
	/**
	* Constructor
	* 
	* @access	public
	*/	
	public function DataTables()
    {
		
	}
	
	public function params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions){
		$this->aColumns = $aColumns;
		$this->sIndexColumn = $sIndexColumn;
		$this->sTable = $sTable;
		$this->skipCols = $skipCols;
		$this->sFunctions = $sFunctions;
		$this->actions = $actions;
	}
	
	public function build_json(){
		$aColumns = $this->aColumns;
		$sIndexColumn = $this->sIndexColumn;
		$sTable = $this->sTable;
		$skipCols = $this->skipCols;
		$sFunctions = $this->sFunctions;
		$actions = $this->actions;
		
		include("koneksi.php");
		// Paging mysql_real_escape_string
		$sLimit = "";
		if ( isset( $_POST['iDisplayStart'] ) && $_POST['iDisplayLength'] != '-1' ){
			$sLimit = "LIMIT ".( $_POST['iDisplayStart'] ).", ".
				( $_POST['iDisplayLength'] );
		}
		
		// Ordering
		$sOrder = "";
		if ( isset( $_POST['iSortCol_0'] ) ){
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_POST['iSortingCols'] ) ; $i++ ){
				if ( $_POST[ 'bSortable_'.intval($_POST['iSortCol_'.$i]) ] == "true" ){
					$sOrder .= $aColumns[ intval( $_POST['iSortCol_'.$i] ) ]."
						".( $_POST['sSortDir_'.$i] ) .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ){
				$sOrder = "";
			}
		}
		
		// Filtering
		$sWhere = "";
		if ( $_POST['sSearch'] != "" ){
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ ){
				$sWhere .= $aColumns[$i]." LIKE '%".( $_POST['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ ){
			if ( $_POST['bSearchable_'.$i] == "true" && $_POST['sSearch_'.$i] != '' ){
				if ( $sWhere == "" ){
					$sWhere = "WHERE ";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".($_POST['sSearch_'.$i])."%' ";
			}
		}
		// SQL
		$sQuery = "
			SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
			FROM   $sTable
			$sWhere
			$sOrder
			$sLimit
		";
		//echo $sQuery;
		
		$rResult = mysql_query($sQuery );
				
		/* Data set length after filtering */
		$sQuery = "
			SELECT FOUND_ROWS() AS TOTAL
		";
		$rResultFilterTotal = mysql_query($sQuery);
		$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
		$iFilteredTotal = $aResultFilterTotal['TOTAL'];
		/* Total data set length */
		$sQuery = "
			SELECT COUNT(".$sIndexColumn.") AS TOTAL
			FROM   $sTable
		";
		$rResultTotal = mysql_query( $sQuery);
		$aResultTotal = mysql_fetch_array($rResultTotal);
		$iTotal = $aResultTotal['TOTAL'];
		
		// Output
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		$no = 0;
		//var_dump($dtResult);
		while($aRow = mysql_fetch_array($rResult)){
			$row = array();
			$row[] = ++$no.'.'; 
			
			if($actions != NULL){
				$btn_actions = '';
				foreach($actions as $aksi){
					if($aksi=="delete"):
						$btn_actions .= "<button class='btn-danger btn-sm jq-delete' title='Delete Record' lang='".$aRow[$sIndexColumn]."'>
										<i class='fa fa-remove'></i></button> ";
					elseif($aksi=="edit"):
						$btn_actions .= "<button class='btn-success btn-sm jq-edit' title='Edit Record' lang='".$aRow[$sIndexColumn]."'>
										<i class='fa fa-edit'></i></button> ";						
					elseif($aksi=="upload"):
						$btn_actions .= "<button class='btn-primary btn-sm jq-upload' title='Upload Data Dukung' lang='".$aRow[$sIndexColumn]."'>
										<i class='fa fa-upload'></i></button> ";						
					else:
						$btn_actions .= $aksi;
					endif;
				}
				$row[] = $btn_actions;				
			}
			
			for ( $i=0 ; $i<count($aColumns) ; $i++ ){
				if ( in_array($aColumns[$i],$skipCols)){ // Check skip colums
					continue;
				}
				else if( array_key_exists($aColumns[$i],$sFunctions)){ // check exclusive function to field
					
					eval("\$row[] = ".sprintf($sFunctions[$aColumns[$i]],$aRow[ $aColumns[$i] ]));
				}				
				else if ( $aColumns[$i] != ' ' )
				{
					// Hook for special cols
					if(($aColumns[$i] == 'rtrw_order')||($aColumns[$i] == 'slide_position')||($aColumns[$i] == 'peta_position')){ // for order/urutan
						if($aRow[$aColumns[$i]] == 1){
							if($iTotal > 1){
								$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-down"></span>';
							}else{
								$row[] = $aRow[ $aColumns[$i] ];
							}							
						}
						else if($aRow[$aColumns[$i]] == $iTotal){
							$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-up"></span>';
						}
						else{
							$row[] = '<span class="order-text">'.$aRow[$aColumns[$i]].'</span><span class="order-up"></span><span class="order-down"></span>';
						}
					}
					else if($aColumns[$i] == 'FOTO'){
						$content = "";						
						if(empty($aRow[ $aColumns[$i] ])){
							$content = "<img class='foto_backend' src='./uploads/foto/profil.png'>";
						}else{
							$content = "<img class='foto_backend' src='./uploads/foto/".$aRow[ $aColumns[$i] ]."'>";
						}
						$row[] = $content;
					}
					else{
						/* General output */
						$row[] = ($aRow[ $aColumns[$i] ]);
					}
				}
			}
			//var_dump($row);
			
			$key_id = $aRow[$sIndexColumn];
			$dukung_q = "SELECT * FROM kegiatan_pendukung WHERE kegiatan_id='".$key_id."'";
			$dukung_sql = mysql_query($dukung_q);
			$data_addon = "";
			$loop_i = 1;
			
			if(mysql_num_rows($dukung_sql)>0):
				while($dukung_r = mysql_fetch_array($dukung_sql)):
					$data_addon .= "<div style='margin-bottom:3px;'>";
					$data_addon .= "<a target='_blank' href='force_download.php?file=".urlencode($dukung_r['pendukung_file'])."'>";
					//$data_addon .= "<a download='".($dukung_r['pendukung_file'])."' href='./uploads/pendukung/".($dukung_r['pendukung_file'])."'>";
					$data_addon .= "<i class='fa fa-file-archive-o'></i> File Pendukung ".$loop_i."</a> ";
					$data_addon .= "<span class='btn btn-xs bg-orange jq-delete-dukung' lang='".$dukung_r['pendukung_id']."'><i class='fa fa-remove '></i></span>";
					$data_addon .= "</div>";
					$loop_i++;
				endwhile;
				$row[] = $data_addon;
			else:
				$row[] = ".:still empty:.";
			endif;
			
			$output['aaData'][] = $row;
		}
		//var_dump($output);
		return ($output);		
	}
}
?>