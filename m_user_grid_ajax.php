<?php
	include("config/datatables_user.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'USER_ID',
		'USER_NIP',
		'USER_FULLNAME',
		'USER_NAME',
		'USERGROUP_NAME'
		);
	$sIndexColumn = "USER_ID";
	
	$sQuery = "SELECT `s_user`.USER_ID
				,`s_user`.USER_NIP
				,`s_user`.USER_FULLNAME
				,`s_user`.USER_NAME
				,`s_usergroup`.USERGROUP_NAME
			FROM `s_user` LEFT JOIN 
			`s_usergroup` ON `s_user`.USERGROUP_ID = `s_usergroup`.USERGROUP_ID 
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('USER_ID');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>