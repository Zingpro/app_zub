<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];

function post_data(){
	include "config/koneksi_li.php";
	session_start();
	
	$saham_id				= mysql_real_escape_string($_POST['saham_id']);
	$saham_code				= $_SESSION['USER_ID'].'.'.date('YmdHis');
	$saham_harga			= mysql_real_escape_string($_POST['saham_harga']);
	$saham_jumlah 			= mysql_real_escape_string($_POST['saham_jumlah']);
	$saham_created_date		= date('Y-m-d');
	$saham_created_by		= $_SESSION['USER_ID'];
	$saham_anggota_id		= mysql_real_escape_string($_POST['anggota_id']);
		
	if(empty($saham_id)):
		$q = "INSERT INTO t_saham(
					saham_code
					,saham_harga
					,saham_jumlah
					,saham_created_date
					,saham_created_by
					,saham_anggota_id									
				) VALUES (
					'".$saham_code."'
					,'".$saham_harga."'
					,'".$saham_jumlah."'
					,'".$saham_created_date."'
					,'".$saham_created_by."'					
					,'".$saham_anggota_id."'					
				)";
		$sql = mysqli_query($conn_db,$q);
		//echo mysql_error();
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE t_saham SET
					saham_code = '".$saham_code."'
					,saham_harga = '".$saham_harga."'
					,saham_jumlah = '".$saham_jumlah."'
					,saham_created_date = '".$saham_created_date."'
					,saham_created_by = '".$saham_created_by."'
					,saham_anggota_id = '".$saham_anggota_id."'
				WHERE saham_id = '".$saham_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah";
	endif;
	
	echo json_encode($data);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "DELETE FROM t_saham 
			   WHERE saham_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = mysql_affected_rows();
	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM t_saham 
			   WHERE saham_id = '".$data_id."'";	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function load_anggota(){
	include "config/koneksi_li.php";
		
	$q = "SELECT m_pemilik_saham.*
			,m_kelompok.kelompok_nama 
	FROM m_pemilik_saham 
	LEFT JOIN m_kelompok ON m_pemilik_saham.pemilik_saham_kelompok = m_kelompok.kelompok_id 
	ORDER BY pemilik_saham_nama ";	
	$sql = mysqli_query($conn_db,$q);
	
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = array();
		
		while($r = mysqli_fetch_array($sql)):
			array_push($data['record'],$r);
		endwhile;
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function load_data_anggota(){
	include "config/koneksi_li.php";
	
	$id = $_POST['data_id'];
	
	$q = $q = "SELECT m_pemilik_saham.*
			,m_kelompok.kelompok_nama 
		FROM m_pemilik_saham 
		LEFT JOIN m_kelompok ON m_pemilik_saham.pemilik_saham_kelompok = m_kelompok.kelompok_id 
		WHERE m_pemilik_saham.pemilik_saham_id = '".$id."'
		ORDER BY pemilik_saham_nama ";	
	$sql = mysqli_query($conn_db,$q);
	
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='load_anggota'){
	load_anggota();
}else if ($act=='load_data_anggota'){
	load_data_anggota();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>