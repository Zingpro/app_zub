$(document).ready(function() {
	// Variable to store your files
	var files;

	
	// Add events
	$('input[type=file]').on('change', prepareUpload);
	// Grab the files and set them to our variable
	function prepareUpload(event)
	{
	  files = event.target.files;
	  const allowedExtensions = [".jpg", ".jpeg", ".png", ".gif"]; // Specify your allowed extensions here

		for (let i = 0; i < files.length; i++) {
			const file = files[i];
			const fileName = file.name;
			const fileExtension = fileName.substring(fileName.lastIndexOf("."));

			if (!allowedExtensions.includes(fileExtension.toLowerCase())) {
				alert(`Invalid file type: ${fileName}. Allowed extensions are: ${allowedExtensions.join(", ")}`);
				event.target.value = ""; // Clear the file input
				return;
			}
			// Process the valid file (e.g., upload or display preview)
			// Your additional logic here...
		}
	}
	
	$('#f-upload').on('submit', uploadFiles);
	
	function uploadFiles(event)
	{
		event.stopPropagation(); // Stop stuff happening
		event.preventDefault(); // Totally stop stuff happening

		// START A LOADING SPINNER HERE

		// Create a formdata object and add the files
		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		var parent_id = $("#parent_id").val();

		$.ajax({
			url: 'm_product_aksi.php?act=upload_foto&parent_id='+parent_id,
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			success: function(data, textStatus, jqXHR)
			{
				if(data.success === 'OK')
				{
                    event.target.value = ""; // Clear the file input
					$("#modal-info").modal("hide");
					alert_custom(data.msg,"modal-success");					
				}
				else
				{
					// Handle errors here
					console.log('ERRORS: ' + data.error);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				// Handle errors here
				console.log('ERRORS: ' + textStatus);
				// STOP LOADING SPINNER
			}
		});
	}
});