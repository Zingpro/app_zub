<?php
	include("config/datatables_lokasi.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'lokasi_id',
		'lokasi_name',
		'lokasi_lat',
		'lokasi_lng'
		);
	$sIndexColumn = "lokasi_id";
	/*
	$sQuery = "SELECT penelitian_id,penelitian_judul,DATE_FORMAT(tgl_lahir,'%d-%m-%Y') as tgl_lahir,
	asal,kota_nama,FORMAT(spp,0) as spp,dosen_nama FROM v_mahasiswa WHERE mahasiswa_active='y' ".$where;
	*/
	$sQuery = "SELECT *
			FROM `m_lokasi` 
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('lokasi_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>