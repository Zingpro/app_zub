<?php
	include "config/koneksi_li.php";
	include "config/all_function.php";
	
	session_start();
	cek_session();
	
	$user_id = $_SESSION['USER_ID'];
	
	$sql = "SELECT * FROM s_user WHERE USER_ID = '".$user_id."'";
	$q = mysqli_query($conn_db,$sql);
	$r = mysqli_fetch_array($q);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo get_config_val('website_title');?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. 
	   
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  -->
	<link rel="stylesheet" href="dist/css/skins/skin-green.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	<!-- jQuery 3 -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="bower_components/bootstrap/js/tooltip.js"></script>
	<!-- DataTables -->
	<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	  
	<!-- DataTables-BS -->
	<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="plugins/fixedHeader/dataTables.fixedHeader.min.js"></script>
	<script src="plugins/responsive/dataTables.responsive.min.js"></script>
	<script src="plugins/responsive/responsive.bootstrap.min.js"></script>
	
	<!-- Select2 -->
	<link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  
	<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="plugins/fixedHeader/fixedHeader.bootstrap.min.css">
	<link rel="stylesheet" href="plugins/responsive/responsive.bootstrap.min.css">
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="v_main.php" class="logo">      
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?php echo get_config_val("website_nick");?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
		  <li class="dropdown messages-menu">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Login Time : <i class="fa fa-clock-o"></i>&nbsp;
              <span id="waktu">Load Waktu</span>
            </a>
		  </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <?php include("v_sidemenu.php");?>

  <!-- =============================================== -->
	<div class="modal modal-info fade" id="modal-info" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Info</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
		
	<div class="modal modal-danger fade" id="modal-danger" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Error</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>	
		
	<div class="modal modal-success fade" id="modal-success" style="display: none;">
	  <div class="modal-dialog modal-alert modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Success Modal</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
		
	<script>
	
		function alert_custom(str,type){
			var mymodal = $('#'+type);
			mymodal.find('.modal-body').html(str);
			mymodal.modal('show');
		}
	</script>
	
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <?php 		
		include "content.php"; 
	?>
	
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://www.facebook.com/spydz">by Zing</a>.</strong> All rights
    reserved.
  </footer>
  
</div>
<!-- ./wrapper -->

<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Number Format -->
<script src="plugins/jquery.number-2.1.3/jquery.number.js" type="text/javascript" ></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script>
	$(document).ready(function () {	
		$('.treeview').removeClass('active');
		$('#<?php echo $gm_active; ?>').addClass('active');
		
		$('.treeview-menu li').removeClass('active');
		$('.singlenav').removeClass('active');
		$('#<?php echo $cm_active; ?>').addClass('active');
		/*var refreshId = setInterval(function()
		{
			$('#waktu').load('time.php').fadeIn("slow");
		}, 3600);
		*/
		$('#waktu').load('time.php').fadeIn("slow");
		$('.sidebar-menu').tree();
		
		$('[data-toggle="tooltip"]').tooltip();
		//Date picker
		$('.datepicker').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true
		});
		$( ".pilih_tanggal" ).datepicker({
			dateFormat: "yy-mm-dd",
			changeMonth: true,
			changeYear: true
		});
		//Initialize Select2 Elements
		$('.select2').select2()
	}); 
</script>

<script type="text/javascript">
	function setModalMaxHeight(element) {
	  this.$element     = $(element);  
	  this.$content     = this.$element.find('.modal-content');
	  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
	  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
	  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
	  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
	  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
	  var maxHeight     = contentHeight - (headerHeight + footerHeight);

	  this.$content.css({
		  'overflow': 'hidden'
	  });
	  
	  this.$element
		.find('.modal-body').css({
		  'max-height': maxHeight,
		  'overflow-y': 'auto'
	  });
	}

	$('.modal').on('show.bs.modal', function() {
	  $(this).show();
	  setModalMaxHeight(this);
	});

	$(window).resize(function() {
	  if ($('.modal.in').length != 0) {
		setModalMaxHeight($('.modal.in'));
	  }
	});
</script>
<style>
	/*
	.modal-dialog {
		margin: auto;
		display: flex !important;
	}
	*/
	.modal-alert {
		z-index:99999;
	}
	.modal-dialog {
		z-index:9999;
	}
</style>
</body>
</html>
