<?php
	include("config/datatables_wakif.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'nazhir_id',
		'nazhir_nik',
		'nazhir_nama',
		'kecamatan_name',
		'nazhir_alamat'
		);
	$sIndexColumn = "nazhir_id";
	
	$sQuery = "SELECT
			m_nazhir.nazhir_id,
			m_nazhir.nazhir_nama,
			m_nazhir.nazhir_agama,
			m_nazhir.nazhir_pekerjaan,
			m_nazhir.nazhir_tanggal_lahir,
			m_nazhir.nazhir_tempat_lahir,
			m_nazhir.nazhir_jabatan,
			m_nazhir.nazhir_kewarganegaraan,
			m_nazhir.nazhir_alamat,
			m_nazhir.nazhir_nik,
			m_nazhir.nazhir_propinsi,
			m_nazhir.nazhir_kabupaten,
			m_nazhir.nazhir_kecamatan,
			m_nazhir.nazhir_created_date,
			d_districts.`name` AS kecamatan_name,
			d_regencies.`name` AS kabupaten_name,
			d_provinces.`name` AS propinsi_name,
			m_nazhir.nazhir_kelurahan,
			m_nazhir.nazhir_created_by
			FROM
			m_nazhir
			LEFT JOIN d_districts ON d_districts.id = m_nazhir.nazhir_kecamatan
			LEFT JOIN d_regencies ON d_districts.regency_id = d_regencies.id
			LEFT JOIN d_provinces ON d_regencies.province_id = d_provinces.id  
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('nazhir_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>