<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen Nazhir Badan Hukum<?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data Nazhir Badan Hukum.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master Nazhir Badan Hukum</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Nazhir Badan Hukum</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah Nazhir Badan Hukum Baru
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-pengurus" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Daftar Pengurus : <span id="daftar_pengurus_yayasan_nama"></span></h4>
						</div>
						<div class="modal-body">
							<div id="wrap-pengurus">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="modal-nazhir" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form name="f-add-nazhir" id="f-add-nazhir" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Tambah Pengurus : <span id="pengurus_yayasan_nama"></span></h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>Nazhir terdaftar</label>
							  <select name="pengurus_nazhir_id" id="pengurus_nazhir_id" class="form-control select2" placeholder="Pilih yang sesuai" style="width: 100%;">
								
							  </select>
							</div>
							<div class="form-group">
							  <label>Jabatan dalam badan Hukum</label>
							  <input name="pengurus_jabatan" id="pengurus_jabatan" type="text" class="form-control" placeholder="Jabatan dalam Badan Hukum">
							  <input name="pengurus_yayasan_id" id="pengurus_yayasan_id" type="hidden">
							</div>							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit_pengurus"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>	
			  </div>
			</div>
			
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Nazhir Badan Hukum Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>No. Akta Notaris</label>
							  <input name="yayasan_id" id="yayasan_id" type="hidden">
							  <input name="yayasan_akta_notaris" id="yayasan_akta_notaris" type="text" class="form-control" placeholder="Masukkan Nomor Akta Notaris">
							</div>
							<div class="form-group">
							  <label>Nama Badan Hukum</label>
							  <input name="yayasan_nama" id="yayasan_nama" type="text" class="form-control" placeholder="Masukkan Nama Badan Hukum">
							</div>							
							<div class="form-group">
							  <label>Pimpinan Pusat berkedudukan di Alamat</label>
							  <textarea class="form-control" rows="3" name="yayasan_alamat" id="yayasan_alamat" placeholder="Lokasi Badan Hukum ..."></textarea>
							</div>
							<div class="form-group">
							  <label>Kelurahan/Desa</label>
							  <textarea class="form-control" rows="3" name="yayasan_kelurahan" id="yayasan_kelurahan" placeholder="Lokasi Badan Hukum ..."></textarea>
							</div>
							<div class="form-group">
							  <label>Cabang/Ranting/Perwakilan</label>
							  <select name="yayasan_tipe" id="yayasan_tipe" class="form-control" placeholder="Pilih yang sesuai" style="width: 100%;">
								<option value="Pusat">Pusat</option>
								<option value="Cabang">Cabang</option>
								<option value="Ranting">Ranting</option>
								<option value="Perwakilan">Perwakilan</option>
							  </select>
							</div>
							<div class="form-group">
							  <label>Deskripsi Kegiatan Sosial</label>
							  <textarea class="form-control" rows="3" name="yayasan_kegiatan_sosial" id="yayasan_kegiatan_sosial" placeholder="Deskripsikan Kegiatan Sosial Badan Hukum..."></textarea>
							</div>
							<div class="form-group">
							  <label>Deskripsi Kegiatan Kemanusiaan</label>
							  <textarea class="form-control" rows="3" name="yayasan_kegiatan_kemanusiaan" id="yayasan_kegiatan_kemanusiaan" placeholder="Deskripsikan Kegiatan Kemanusiaan Badan Hukum..."></textarea>
							</div>
							<div class="form-group">
							  <label>Deskripsi Kegiatan Keagamaan</label>
							  <textarea class="form-control" rows="3" name="yayasan_kegiatan_keagamaan" id="yayasan_kegiatan_keagamaan" placeholder="Deskripsikan Kegiatan Keagamaan Badan Hukum..."></textarea>
							</div>
							<div class="form-group">
							  <label>Tanggal Berdiri Badan Hukum</label>
							  <input name="yayasan_tanggal_berdiri" id="yayasan_tanggal_berdiri" type="text" class="form-control datepicker" >
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>Akte Notaris</th>
							<th width="150">Nama Badan Hukum</th>
							<th>Alamat</th>
							<th width="200">Tipe</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Nazhir Badan Hukum pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
						
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{"bSearchable": true}	/* NIP */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{"sClass": "center"} /* Usergroup */ 	 
							  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "m_nazhir_yayasan_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
		
		function load_nazhir(wrap_out)
		{					
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_nazhir",
				data: { 'data_id': 'b'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].nazhir_id + '">' + data.record[i].nazhir_nik+ '\n | '+data.record[i].nazhir_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function delete_pengurus(id,yayasan_id){
			if(confirm("Yakin akan menghapus data ini : ")){
				return $.ajax({
					type: "POST",
					url: "m_nazhir_yayasan_aksi.php?act=delete_pengurus",
					data: { 'data_id': id  },
					dataType: "json",
					success: function(data){
						if(data.msg=="OK"){
							alert( "Transaksi Berhasil");
							
							var datasend = {'yayasan_id':yayasan_id};
							$("#wrap-pengurus").load("m_nazhir_yayasan_aksi.php?act=detil_pengurus",datasend);
							
						}else{
							alert(data.msg);
						}						
					}
				});
			}
		}
		
		$(document).ready(function() { 	
			load_nazhir("pengurus_nazhir_id");
												
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_nazhir_yayasan_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");				
				$.post( "m_nazhir_yayasan_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							
							$("#yayasan_id").val(data.record.yayasan_id);
							$("#yayasan_nama").val(data.record.yayasan_nama);							
							$("#yayasan_alamat").val(data.record.yayasan_alamat);
							$("#yayasan_tipe").val(data.record.yayasan_tipe);
							$("#yayasan_akta_notaris").val(data.record.yayasan_akta_notaris);
							$("#yayasan_kegiatan_keagamaan").val(data.record.yayasan_kegiatan_keagamaan);
							$("#yayasan_kegiatan_sosial").val(data.record.yayasan_kegiatan_sosial);
							$("#yayasan_kegiatan_kemanusiaan").val(data.record.yayasan_kegiatan_kemanusiaan);
							$("#yayasan_tanggal_berdiri").val(data.record.yayasan_tanggal_berdiri);
							$("#yayasan_kelurahan").val(data.record.yayasan_kelurahan);
										
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-detil-pengurus", function(e) {
				var id = $(this).attr("lang");
				var yayasan_name = $(this).attr("lang2");
				$("#daftar_pengurus_yayasan_nama").text(yayasan_name);
				$("#modal-pengurus").modal("show");
				
				var datasend = {'yayasan_id':id};
				$("#wrap-pengurus").load("m_nazhir_yayasan_aksi.php?act=detil_pengurus",datasend);
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-print-pengesahan", function(e) {
				var id = $(this).attr("lang");
				window.open("report/v_report_w5a.php?id="+id,
							"_blank",
							"toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=100,width=500,height=500");
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-add-pengurus", function(e) {
				$("#modal-nazhir").modal("show");
				var id = $(this).attr("lang");
				var yayasan_name = $(this).attr("lang2");
				$("#pengurus_yayasan_id").val(id);
				$("#pengurus_yayasan_nama").text(yayasan_name);
			});
			
			var frm2 = $('#f-add-nazhir');			
			frm2.submit(function (e) {
				e.preventDefault();				
				var dataform = frm2.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("m_nazhir_yayasan_aksi.php?act=add_pengurus", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
					clear_form_pengurus();
				},"json");								
			});
			
			$("#btn_submit_pengurus").click(function(e){
				$("#modal-nazhir").modal("hide");							
				frm2.submit();				
				clear_form_pengurus();
				oTable.ajax.reload();
			});
			
			function clear_form_pengurus(){
				$("#pengurus_jabatan").val("");
				$("#pengurus_yayasan_id").val("");
				$("#pengurus_yayasan_nama").text("");
			}
			
			var frm = $('#f-add');			
			frm.submit(function (e) {
				e.preventDefault();				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("m_nazhir_yayasan_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");								
			});
			
			function clear_form(){
				$("#yayasan_id").val("");
				$("#yayasan_nama").val("");							
				$("#yayasan_alamat").val("");
				$("#yayasan_tipe").val("");
				$("#yayasan_akta_notaris").val("");
				$("#yayasan_kegiatan_keagamaan").val("");
				$("#yayasan_kegiatan_sosial").val("");
				$("#yayasan_kegiatan_kemanusiaan").val("");
				$("#yayasan_tanggal_berdiri").val("");	
				$("#yayasan_kelurahan").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
		});
	</script>
	<style>
		.detil_pengurus_table td{
			padding : 5px;
			margin : 5px;
			border: solid 1px #000;
		}
		.detil_pengurus_table th{
			padding : 5px;
			margin : 5px;
			border: solid 1px #000;
		}
		.detil_pengurus_table{
			border: solid 1px #000;
			padding : 5px;
			margin : 5px;
		}
	</style>