<?php
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	include "config/all_function.php";
?>
<!DOCTYPE html>
<html>
<head>
	
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo get_config_val('website_title');?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">

<!-- =============================================== -->
<div class="modal modal-warning fade" id="modal-info" style="display: none;">
	  <div class="modal-dialog modal-sm">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">×</span></button>
			<h4 class="modal-title">Info</h4>
		  </div>
		  <div class="modal-body">
			<p>One fine body…</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	
<script>
	function alert_custom(str,type){
		var mymodal = $('#'+type);
		mymodal.find('.modal-body').html(str);
		mymodal.modal('show');
	}
</script>
	
<div class="login-box">
  <div class="login-logo">
	<!--
	<img src="assets/img/simakaf_logo.png" width="150"/>
	<img src="assets/img/siwak-bp_logo.png" width="200"/>
	<img src="assets/img/silkaf_logo.png" width="200"/>
	-->
	<img src="assets/img/logo-ikhmal.png" width="360"/>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h4 class="login-box-msg">Pendaftaran Akun</h4>

    <form id="flogin" action="registrasi_aksi.php" method="post">
      <div class="form-group has-feedback">
        <label>Isikan NIK/NIP anda (*)</label>
        <input name="user_nik" id="user_nik" type="text" class="form-control" placeholder="NIP/NIK">
        <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Isikan Nama lengkap anda (*)</label>
        <input name="user_fullname" id="user_fullname" type="text" class="form-control" placeholder="Nama lengkap">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Alamat Domisili/Alamat Tinggal</label>
        <textarea name="user_address" id="user_address" class="form-control" placeholder="Alamat anda"></textarea>
        <span class="glyphicon glyphicon-location form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Password untuk Login (*)</label>
        <input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password untuk login">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
      </div>
      <div class="form-group has-feedback">
        <label>Ulangi Password anda</label>
        <input name="user_password2" id="user_password2" type="password" class="form-control" placeholder="Ulangi Password Anda">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Nomor Handphone (WA yang aktif) (*)</label>
        <input name="user_phone" id="user_phone" type="text" class="form-control" placeholder="Nomor Handphone (WA)">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <?php
        $a = rand(0,10);
        $b = rand(0,10);
        $c = $a + $b;
      ?>
      <div class="form-group has-feedback">
        <label>Jawab pertanyaan ini : </label><span class="chaptcha"> <?php echo $a." + ".$b; ?> = ?</span>

        <input name="chaptcha" id="chaptcha" type="text" class="form-control" placeholder="isi dengan hasil yang benar">
        <span class="glyphicon glyphicon-knight form-control-feedback"></span>
      </div>
      <div class="row">        
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Daftar</button>
        </div>
        <div class="col-xs-4">
            <a href="login.php" class="btn btn-warning btn-block btn-flat">Login</a>
        </div>
        <!-- /.col -->
      </div>
    </form>
    
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<style>
    .chaptcha{
        color:red;
        font-weight:bold;
    }
</style>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
$(function () {
	var frm = $('#flogin');
	
    function clear_input(){
        $("#user_nik").val("");
        $("#user_fullname").val("");
        $("#user_password").val("");
        $("#user_password2").val("");
        $("#user_phone").val("");
    }

	frm.submit(function (e) {
		e.preventDefault();
		
		var dataform = frm.serializeArray();				
		//data.push({name: 'wordlist', value: wordlist});

		$.post("registrasi_aksi.php?a=<?php echo $c;?>", dataform, function(data){
			if(data.status == "OK"){				
                clear_input();
                alert("Data Registrasi Berhasil");
                location.reload();
			}else{
				alert_custom(data.msg,"modal-info");
			}
		},"json");
						
	});
		
	$('input').iCheck({
	  checkboxClass: 'icheckbox_square-blue',
	  radioClass: 'iradio_square-blue',
	  increaseArea: '20%' /* optional */
	});
});
</script>
</body>
</html>
