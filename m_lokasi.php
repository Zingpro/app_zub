<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];
	 
	$sql = "SELECT * FROM t_absen order by absen_id DESC";
	$q = mysql_query($sql);
	$r = mysql_fetch_array($q);
			
	$foto = $r['absen_foto'];
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Lokasi Pointing Absen <?php //echo $r['USER_FULLNAME'];?>
        <small>Pastikan GPS Smartphone Anda Diaktifkan dengan mode tingkat presisi yang tinggi.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="#">Lokasi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Master Lokasi</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
		<style>
			#map-add
			{
				height: 400px !important;
				width: 100% !important;
				margin: 10px auto;
			}
		</style>
		
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						Tambah Lokasi
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="m_lokasi_aksi.php?act=add">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Penambahan Lokasi Pointing Baru</h4>
						</div>
						<div class="modal-body">
							<div id="map-add"></div>
							<div class="form-group">
								<button id="myLoc" type="button" class="btn btn-primary btn-lg" >
									<i class='fa fa-map-pin'></i>
									Lokasi Sekarang
								</button>
							</div>
							<div class="form-group">
							  <label>Nama Lokasi</label>
							  <input name="lokasi_name" id="lokasi_name" type="text" class="form-control" placeholder="Masukkan Nama Lokasi">
							</div>
							<div class="form-group">
							  <label>Lattitude</label>
							  <input name="lokasi_lat" id="lokasi_lat" type="text" class="form-control" placeholder="Masukkan Lattitude Lokasi">
							</div>
							<div class="form-group">
							  <label>Longitude</label>
							  <input name="lokasi_lng" id="lokasi_lng" type="text" class="form-control" placeholder="Masukkan Longitude Lokasi">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit">Add Location</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">	
				<div class="col-md-12">	
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>Lokasi Nama</th>
							<th width="150">Lattitude</th>
							<th width="200">Longitude</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Detil Absensi 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
	<script>
		// Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, markerq, infoWindow;
	  var pos_u;
	  			  
      function initMap() {
		map = new google.maps.Map(document.getElementById('map-add'), {
			//masjid agung
			//{lat: -7.026314, lng:112.745792 }
			//Kemenag
			//{lat: -7.044617, lng: 112.738019}
		  center: {lat: -7.044617, lng: 112.738019},
		  zoom: 12
		});
		
		markerq = new google.maps.Marker({
				  position: {lat: -7.044617, lng: 112.738019},
				  map: map,
				  title: 'Lokasi Baru',
				  icon: 'assets/icon_marker/default.png'
				});
		
		
		google.maps.event.addListener(map, "dblclick", function (e) {
			//lat and lng is available in e object
			var latLng = e.latLng;
			markerq.setMap(null);
						
			markerq = new google.maps.Marker({
				  position: latLng,
				  map: map,
				  title: 'Lokasi Baru',
				  icon: 'assets/icon_marker/default.png'
				});
				
			console.log("Latitude :"+latLng.lat());
			console.log("Longitude :"+latLng.lng());
			
			document.getElementById('lokasi_lat').value = latLng.lat();
			document.getElementById('lokasi_lng').value = latLng.lng();			
		});
	  }
	  
	  
	  function lokasi_sekarang(){
		// Try HTML5 geolocation.
        if (navigator.geolocation) {
			  navigator.geolocation.getCurrentPosition(function(position) {
				pos_u = {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				};
							  
				markerq.setMap(null);
						
				markerq = new google.maps.Marker({
					position: pos_u,
					map: map,
					title: 'Lokasi Anda',
					icon: 'assets/icon_marker/default.png'
				});
				
				map.setCenter(pos_u);
				
				$('#lokasi_lng').val(pos_u.lng);
				$('#lokasi_lat').val(pos_u.lat);
				
			  }, function() {
					handleLocationError(true, infoWindow, map.getCenter());
			  });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
	  }
	  
	  function handleLocationError(browserHasGeolocation, infoWindow, pos_u) {
		infoWindow.setPosition(pos_u);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your browser doesn\'t support geolocation.');
		infoWindow.open(map);
      }
	  
	  
	</script>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnYAUXXlgmPkHYfjrKQnVnDfnHBV7aCHA&callback=initMap"
		async defer>
	</script>  
	
	<script>
		$(document).ready(function() { 		
			
			// Tampilkan tabel
			var oTable = $('#sample_3').DataTable( {
				responsive : true,
				//"bJQueryUI": true,				
				"aoColumns": [
					{
						"sClass": "center",
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* no */
					,{
						"bSearchable": false,
						"bVisible":true,
						"bSortable":false
					}/* aksi */
					,null	/* Lokasi */
					,{"sClass": "center"} /* Lat */ 		  
					,{"sClass": "center"} /* Long */ 	 
								  
				],      
				"sPaginationType": "full_numbers",
				"aLengthMenu" : [10,20,30,40],
				"bRetrieve" : true,
				"bStateSave": false,
				"bProcessing": true,
				"bServerSide": true,
				"language": {
					"search": "Pencarian :",
					"sInfoEmpty": "Data Tidak Tersedia",
					"sZeroRecords": "Data Tidak Tersedia",
					"sInfo": "Jumlah Data _TOTAL_ ",
					"sInfoFiltered": " de _MAX_ registros"         
				},
				
				"sAjaxSource": "m_lokasi_grid_ajax.php",
				"fnServerData": function ( sSource, aoData, fnCallback ) {
				  aoData.push( { "name": "aksi", "value": "table" } );		  
				  $.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				  } );          
				}
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_lokasi_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();

				$.ajax({
					type: frm.attr('method'),
					url: frm.attr('action'),
					data: frm.serialize(),
					success: function (rs) {						
						alert(rs.response);					
					},
					error: function (data) {
						console.log('An error occurred.');
					},
				});
			});
			
			function clear_form(){
				$("#lokasi_name").val("");
				$("#lokasi_lat").val("");
				$("#lokasi_lng").val("");
			}
			
			$("#btn_submit").click(function(e){
				frm.submit();
				$("#modal-default").modal("hide");
				clear_form();
				oTable.ajax.reload();
			});
			
			$("#myLoc").click(function(e){
				lokasi_sekarang();
			});
		});
	</script>