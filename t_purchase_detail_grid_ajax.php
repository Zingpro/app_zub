<?php
	include("config/datatables_product.php");
	
	$where = "";
        
	$purchase_id = $_POST['purchase_id'];
	$where = (empty($purchase_id))? "" : " AND purchasedetail_purchase_id='".$purchase_id."' ";
	
	$aColumns = array( 
		'purchasedetail_id',
        'product_code',
        'product_name',
		'purchasedetail_count',
		'purchasedetail_price',
        'purchasedetail_subtotal',
		'item_status'
		);
	$sIndexColumn = "purchasedetail_id";
	
	$sQuery = "SELECT
                t_purchasedetail.purchasedetail_id,                
                CONCAT(m_product.product_id,'|',m_product.product_code) as product_code,
                m_product.product_name,
                t_purchasedetail.purchasedetail_count,
                t_purchasedetail.purchasedetail_price,
                t_purchasedetail.purchasedetail_subtotal,
				t_purchase.purchase_status as item_status
			FROM t_purchasedetail
            LEFT JOIN m_product ON m_product.product_id = t_purchasedetail.purchasedetail_product_id 
			LEFT JOIN t_purchase ON t_purchase.purchase_id = t_purchasedetail.purchasedetail_purchase_id 
			WHERE 1=1 ".$where." ";
	// echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('purchasedetail_id','item_status');
	
	//untuk format
	$sFunctions = array();

	$actions = array(
		'delete_detil'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>