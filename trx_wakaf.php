<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi Wakaf <?php //echo $r['USER_FULLNAME'];?>
        <small>Pendataan (Akta Ikrar) Wakaf.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Transaksi Wakaf</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Form Pendataan Ikrar Wakaf</h3>
			  <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
						title="Collapse">
				  <i class="fa fa-minus"></i>
				  </button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
				  <i class="fa fa-times"></i>
				  </button>
			  </div>
			</div>
			
			<div class="box-body">
				<div class="row">
					<form id="f-add" method="post" action="#">
					<div class="col-md-12">
						<h3>Inputkan secara lengkap data Ikrar Wakaf</h3>
						
						  <div class="row">
							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-graduation-cap"></i>

								  <h3 class="box-title">Data Wakif</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">								  
								  <div class="callout callout-success">									
									<h4><i class="icon fa fa-info-circle"></i> Pilih wakif pemberi wakaf</h4>
									pencarian menggunakan nomor KTP.
								  </div>
									Pilih Wakif : <select name="wakif_id" id="wakif_id" class="form-control select2" placeholder="Pilih Wakif" style="width: 100%;">
									</select>
									<br/>
									<br/>
									<div class="callout">
										<table class="dtp">
											<tr><td>Nama Lengkap</td><td>:</td><td><span id="wakif_nama"></span></td></tr>
											<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td><span id="wakif_tanggal_lahir"></span></td></tr>
											<tr><td>Agama</td><td>:</td><td><span id="wakif_agama"></span></td></tr>
											<tr><td>Pekerjaan</td><td>:</td><td><span id="wakif_pekerjaan"></span></td></tr>
											<tr><td>Tempat Tinggal</td><td>:</td><td><span id="wakif_alamat"></span></td></tr>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->

							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-bank"></i>
								  <h3 class="box-title">Data Nazhir</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">								  
								  <div class="callout callout-info">
									<h4><i class="icon fa fa-info-circle"></i> Pilih Nazhir penerima wakaf</h4>
									pencarian menggunakan nomor KTP.
								  </div>
								    <div class="checkbox">
									  <label>
										<input id="wakaf_yayasan_check" name="wakaf_yayasan_check" type="checkbox" value="nazhir yayasan"> Centang Jika Nazhir Badan Hukum
									  </label>
									</div>
									Pilih Yayasan : 
								    <select name="wakaf_yayasan_id" id="wakaf_yayasan_id" class="form-control select2" placeholder="Pilih Yayasan Nazhir" style="width: 100%;">
									</select>
									<hr/>
									Pilih Nazhir : 
									<select name="nazhir_id" id="nazhir_id" class="form-control select2" placeholder="Pilih Nazhir" style="width: 100%;">
									</select>
									<br/>
									<br/>
									<div class="callout">
										<table class="dtp">
											<tr><td>Nama Lengkap</td><td>:</td><td><span id="nazhir_nama"></span></td></tr>
											<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td><span id="nazhir_tanggal_lahir"></span></td></tr>
											<tr><td>Agama</td><td>:</td><td><span id="nazhir_agama"></span></td></tr>
											<tr><td>Pekerjaan</td><td>:</td><td><span id="nazhir_pekerjaan"></span></td></tr>
											<tr><td>Tempat Tinggal</td><td>:</td><td><span id="nazhir_alamat"></span></td></tr>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->
						  </div>
						  
						  <div class="row">
							<div class="col-md-12">
								<div class="box box-default">
									<div class="box-header with-border">
									  <i class="fa fa-bank"></i>
									  <h3 class="box-title">Obyek Wakaf</h3>
									</div>
									<div class="box-body">
										<div class="row">
											<div class="col-md-6">
												<div class="callout">
													<div class="form-group">
													  <label>Nomor Akta Ikrar Wakaf</label>
													  <input type="text" class="form-control" name="wakaf_niw" id="wakaf_niw" placeholder="Nomor Akta Ikrar Wakaf ...">
													</div>
													<div class="form-group">
													  <label>Berupa</label>
													  <input type="text" class="form-control" name="wakaf_obyek" id="wakaf_obyek" placeholder="Jenis obyek wakaf ...">
													</div>
													<div class="form-group">
													  <label>No. Sertifikat</label>
													  <input type="text" class="form-control" name="wakaf_sertifikat" id="wakaf_sertifikat" placeholder="Nomor Sertifikat ...">
													</div>
													<div class="form-group">
													  <label>Lokasi/Alamat</label>
													  <textarea class="form-control" rows="3" name="wakaf_alamat" id="wakaf_alamat" placeholder="Lokasi obyek wakaf ..."></textarea>
													</div>
													
													<div class="form-group">
													  <label>Propinsi</label>
													  <select name="wakaf_propinsi" id="wakaf_propinsi" class="form-control select2" placeholder="Pilih Propinsi" style="width: 100%;">
														<?php
															$qs = "SELECT * FROM d_provinces";
															$sqls = mysql_query($qs);
															while($r = mysql_fetch_array($sqls)){
														?>
															<option value="<?php echo $r['id'];?>"><?php echo $r['name'];?></option>
														<?php
															}
														?>
													  </select>
													</div>
													<div class="form-group">
													  <label>Kabupaten</label>
													  <select name="wakaf_kabupaten" id="wakaf_kabupaten" class="form-control select2" style="width: 100%;">
													  </select>
													</div>
													<div class="form-group">
													  <label>Kecamatan</label>
													  <select name="wakaf_kecamatan" id="wakaf_kecamatan" class="form-control select2" style="width: 100%;">
													  </select>
													</div>
													<div class="form-group">
													  <label>Kelurahan/Desa</label>
													  <input type="text" class="form-control" name="wakaf_kelurahan" id="wakaf_kelurahan" placeholder="Desa/Kelurahan ...">
													</div>
													<div class="form-group">
													  <label>Untuk Keperluan</label>
													  <textarea class="form-control" rows="3" name="wakaf_keperluan" id="wakaf_keperluan" placeholder="Tuliskan Keperluan ..."></textarea>
													</div>
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="callout">
													<div class="form-group">
													  <label>Obyek Panjang</label>
													  <input type="text" class="form-control" name="wakaf_lahan_panjang" id="wakaf_lahan_panjang" placeholder="Panjang Lahan ...">
													</div>
													<div class="form-group">
													  <label>Obyek Lebar</label>
													  <input type="text" class="form-control" name="wakaf_lahan_lebar" id="wakaf_lahan_lebar" placeholder="Lebar Lahan ...">
													</div>
													<div class="form-group">
													  <label>Obyek Luas</label>
													  <input type="text" class="form-control" name="wakaf_lahan_luas" id="wakaf_lahan_luas" placeholder="Luas Lahan ...">
													</div>
												</div>
												<div class="callout">
													<div class="form-group">
													  <label>Batas Timur</label>
													  <input type="text" class="form-control" name="wakaf_batas_timur" id="wakaf_batas_timur" placeholder="batas timur ...">
													</div>
													<div class="form-group">
													  <label>Batas Barat</label>
													  <input type="text" class="form-control" name="wakaf_batas_barat" id="wakaf_batas_barat" placeholder="batas barat ...">
													</div>
													<div class="form-group">
													  <label>Batas Selatan</label>
													  <input type="text" class="form-control" name="wakaf_batas_selatan" id="wakaf_batas_selatan" placeholder="batas selatan ...">
													</div>
													<div class="form-group">
													  <label>Batas Utara</label>
													  <input type="text" class="form-control" name="wakaf_batas_utara" id="wakaf_batas_utara" placeholder="batas utara ...">
													</div>
												</div>
																								
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						  
						  <div class="row">
							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-thumbs-o-up"></i>

								  <h3 class="box-title">Data Saksi 1</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">								  
								  <div class="callout callout-warning">									
									<h4><i class="icon fa fa-male"></i> Pilih saksi pertama</h4>
									pencarian menggunakan nomor KTP.
								  </div>
									Pilih Saksi 1 :
									<select name="saksi_id_1" id="saksi_id_1" class="form-control select2" placeholder="Pilih Wakif" style="width: 100%;">
									</select>
									<br/>
									<br/>
									<div class="callout">
										<table class="dtp">
											<tr><td>Nama Lengkap</td><td>:</td><td><span id="saksi_1_nama"></span></td></tr>
											<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td><span id="saksi_1_tanggal_lahir"></span></td></tr>
											<tr><td>Agama</td><td>:</td><td><span id="saksi_1_agama"></span></td></tr>
											<tr><td>Pekerjaan</td><td>:</td><td><span id="saksi_1_pekerjaan"></span></td></tr>
											<tr><td>Tempat Tinggal</td><td>:</td><td><span id="saksi_1_alamat"></span></td></tr>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->

							<div class="col-md-6">
							  <div class="box box-default">
								<div class="box-header with-border">
								  <i class="fa fa-thumbs-o-up"></i>
								  <h3 class="box-title">Data Saksi 2</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">								  
								  <div class="callout callout-warning">
									<h4><i class="icon fa fa-male"></i> Pilih saksi kedua</h4>
									pencarian menggunakan nomor KTP.
								  </div>
									Pilih Saksi 2 :
									<select name="saksi_id_2" id="saksi_id_2" class="form-control select2" placeholder="Pilih Nazhir" style="width: 100%;">
									</select>
									<br/>
									<br/>
									<div class="callout">
										<table class="dtp">
											<tr><td>Nama Lengkap</td><td>:</td><td><span id="saksi_2_nama"></span></td></tr>
											<tr><td>Tempat, Tanggal Lahir</td><td>:</td><td><span id="saksi_2_tanggal_lahir"></span></td></tr>
											<tr><td>Agama</td><td>:</td><td><span id="saksi_2_agama"></span></td></tr>
											<tr><td>Pekerjaan</td><td>:</td><td><span id="saksi_2_pekerjaan"></span></td></tr>
											<tr><td>Tempat Tinggal</td><td>:</td><td><span id="saksi_2_alamat"></span></td></tr>
										</table>
									</div>
								</div>
								<!-- /.box-body -->
							  </div>
							  <!-- /.box -->
							</div>
							<!-- /.col -->
						  </div>
						  
							<div class="col-md-12">
								<center>
									<button type="button" class="btn btn-primary" id="btn_submit">
										<i class='fa fa-save'></i> Simpan 
									</button>
								</center>
							</div>
					</div>
					</form>
				</div>
			</div>
        <!-- /.box-body -->
        <div class="box-footer">
          Form Pendataan Ikrar Wakaf
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
	
	<style>
		.dtp td{
			padding : 3px;
		}
	</style>
	
	<script>
		function loadKab(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_saksi_aksi.php?act=load_kab",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}						
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function loadKec(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_saksi_aksi.php?act=load_kec",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function initial_select(){					
			var prop = $('#wakaf_propinsi').val();			
			
			$.when(loadKab(prop,"wakaf_kabupaten")).done(function(){
				var kab = $('#wakaf_kabupaten').val();
				loadKec("1101", "wakaf_kecamatan");
			});
		}
		
		function load_wakif(wrap_out)
		{			
			$('#'+wrap_out).empty();
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_wakif",
				data: { 'data_id': 'a'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].wakif_id + '">' + data.record[i].wakif_ktp_nomor+ '\n | '+data.record[i].wakif_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_data_wakif(id)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_data_wakif",
				data: { 'data_id': id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						$("#wakif_nama").text(data.record.wakif_nama);
						$("#wakif_tanggal_lahir").text(data.record.wakif_tanggal_lahir);
						$("#wakif_agama").text(data.record.wakif_agama);
						$("#wakif_pekerjaan").text(data.record.wakif_pekerjaan);
						$("#wakif_alamat").text(data.record.wakif_alamat);
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_nazhir(wrap_out)
		{					
			$('#'+wrap_out).empty();
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_nazhir",
				data: { 'data_id': 'b'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].nazhir_id + '">' + data.record[i].nazhir_nik+ '\n | '+data.record[i].nazhir_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function load_nazhir_yayasan(wrap_out)
		{				
			var yayasan_id = $('#wakaf_yayasan_id').val();
			$('#'+wrap_out).empty();
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_nazhir_yayasan",
				data: { 'data_id': yayasan_id },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].nazhir_id + '">' + data.record[i].nazhir_nik+ '\n | '+data.record[i].nazhir_nama+'</option>');
						}
					}else{
						$('#'+wrap_out).append('<option value="0">** Belum Memiliki Pengurus **</option>');
					}						
				}
			});
		}
		
		function load_saksi(wrap_out)
		{			
			$('#'+wrap_out).empty();
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_saksi",
				data: { 'data_id': 'b'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].saksi_id + '">' + data.record[i].saksi_nik+ '\n | '+data.record[i].saksi_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function load_data_nazhir(id)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_data_nazhir",
				data: { 'data_id': id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						$("#nazhir_nama").text(data.record.nazhir_nama);
						$("#nazhir_tanggal_lahir").text(data.record.nazhir_tanggal_lahir);
						$("#nazhir_agama").text(data.record.nazhir_agama);
						$("#nazhir_pekerjaan").text(data.record.nazhir_pekerjaan);
						$("#nazhir_alamat").text(data.record.nazhir_alamat);
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_data_saksi_1(id)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_data_saksi",
				data: { 'data_id': id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						$("#saksi_1_nama").text(data.record.saksi_nama);
						$("#saksi_1_tanggal_lahir").text(data.record.saksi_tanggal_lahir);
						$("#saksi_1_agama").text(data.record.saksi_agama);
						$("#saksi_1_pekerjaan").text(data.record.saksi_pekerjaan);
						$("#saksi_1_alamat").text(data.record.saksi_alamat);
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_data_saksi_2(id)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_data_saksi",
				data: { 'data_id': id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						$("#saksi_2_nama").text(data.record.saksi_nama);
						$("#saksi_2_tanggal_lahir").text(data.record.saksi_tanggal_lahir);
						$("#saksi_2_agama").text(data.record.saksi_agama);
						$("#saksi_2_pekerjaan").text(data.record.saksi_pekerjaan);
						$("#saksi_2_alamat").text(data.record.saksi_alamat);
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function load_yayasan(wrap_out)
		{						
			return $.ajax({
				type: "POST",
				url: "trx_wakaf_aksi.php?act=load_yayasan",
				data: { 'data_id': 'b'  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].yayasan_id + '">' + data.record[i].yayasan_akta_notaris+ '\n | '+data.record[i].yayasan_nama+'</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function initial_load(){			
			$.when(
				load_wakif("wakif_id"),
				load_nazhir("nazhir_id"),
				load_saksi("saksi_id_1"),
				load_saksi("saksi_id_2"),
				load_yayasan("wakaf_yayasan_id")
			).done(function(){
				var data_id = $('#wakif_id').val();
				load_data_wakif(data_id);
				
				data_id = $('#nazhir_id').val();
				load_data_nazhir(data_id);
				
				data_id = $('#saksi_id_1').val();
				load_data_saksi_1(data_id);
				
				data_id = $('#saksi_id_2').val();
				load_data_saksi_2(data_id);
			});			
		}
		
		$(document).ready(function() { 		
			initial_load();
			initial_select();
			
			$('#wakaf_yayasan_check').change(function() {
				// this will contain a reference to the checkbox   
				if ($(this).is(":checked")) {
					// the checkbox is now checked 
					load_nazhir_yayasan("nazhir_id");
				} else {
					// the checkbox is now no longer checked
					load_nazhir("nazhir_id");
				}
			});
			
			$('#wakaf_yayasan_id').on('select2:select', function (e) {
				var data_id = $(this).val();				
				if ($('#wakaf_yayasan_check').is(":checked")) {
					// the checkbox is now checked 
					load_nazhir_yayasan("nazhir_id");
				}
			});
			
			$('#wakaf_propinsi').on('select2:select', function (e) {
				var data_id = $(this).val();				
				return $.when(
					loadKab(data_id,"wakaf_kabupaten")
				).done(function(){				
					loadKec($("#wakaf_kabupaten").val(),"wakaf_kecamatan");				
				});
			});
			
			$('#wakaf_kabupaten').on('select2:select', function (e) {
				var data_id = $(this).val();
				loadKec(data_id,"wakaf_kecamatan");
			});
			
			$('#nazhir_id').on('select2:select', function (e) {
				var data_id = $(this).val();
				load_data_nazhir(data_id);
			});
			
			$('#wakif_id').on('select2:select', function (e) {
				var data_id = $(this).val();
				load_data_wakif(data_id);
			});
			
			$('#saksi_id_1').on('select2:select', function (e) {
				var data_id = $(this).val();
				load_data_saksi_1(data_id);
			});
			
			$('#saksi_id_2').on('select2:select', function (e) {
				var data_id = $(this).val();
				load_data_saksi_2(data_id);
			});
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("trx_wakaf_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
					
				},"json");
								
			});
			
			$("#btn_submit").click(function(e){						
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			function clear_form(){				
				$("#wakaf_id").val("");
				$("#wakaf_niw").val("");
				$("#wakaf_obyek").val("");
				$("#wakaf_sertifikat").val("");
				$("#wakaf_alamat").val("");
				$("#wakaf_kecamatan").val("");
				$("#wakaf_kelurahan").val("");
				$("#wakaf_kabupaten").val("");
				$("#wakaf_propinsi").val("");
				$("#wakaf_lahan_luas").val("");
				$("#wakaf_lahan_lebar").val("");
				$("#wakaf_lahan_panjang").val("");
				$("#wakaf_batas_timur").val("");
				$("#wakaf_batas_barat").val("");
				$("#wakaf_batas_selatan").val("");
				$("#wakaf_batas_utara").val("");
				$("#wakaf_keperluan").val("");
			}
		});
	</script>