<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen Nazhir <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data Nazhir.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master Nazhir</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Nazhir</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah Nazhir Baru
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Nazhir Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>No. KTP</label>
							  <input name="nazhir_id" id="nazhir_id" type="hidden">
							  <input name="nazhir_nik" id="nazhir_nik" type="text" class="form-control" placeholder="Masukkan Nomor KTP">
							</div>
							<div class="form-group">
							  <label>Nama Lengkap (sesuai KTP)</label>
							  <input name="nazhir_nama" id="nazhir_nama" type="text" class="form-control" placeholder="Masukkan Nama Wakif">
							</div>
							<div class="form-group">
							  <label>Agama</label>
							  <!--
							  <input name="nazhir_agama" id="nazhir_agama" type="text" class="form-control" placeholder="Agama Wakif">
							  -->
							  <?php select_agama("nazhir_agama","form-control",null,"placeholder='Agama Wakif'"); ?>
							</div>
							<div class="form-group">
							  <label>Tempat, Tanggal Lahir</label>
							  <input name="nazhir_tempat_lahir" id="nazhir_tempat_lahir" type="text" class="form-control" placeholder="Tempat Kelahiran">
							  <input name="nazhir_tanggal_lahir" id="nazhir_tanggal_lahir" type="text" placeholder="Tanggal Kelahiran" class="form-control pull-right datepicker">
							</div>
							<div class="form-group">
							  <label>Alamat</label>
							  <input name="nazhir_alamat" id="nazhir_alamat" type="text" class="form-control" placeholder="Masukkan Alamat">
							</div>
							<div class="form-group">
							  <label>Propinsi</label>
							  <select name="nazhir_propinsi" id="nazhir_propinsi" class="form-control select2" placeholder="Pilih Propinsi" style="width: 100%;">
								<?php
									$qs = "SELECT * FROM d_provinces";
									$sqls = mysql_query($qs);
									while($r = mysql_fetch_array($sqls)){
								?>
									<option value="<?php echo $r['id'];?>"><?php echo $r['name'];?></option>
								<?php
									}
								?>
							  </select>
							</div>
							<div class="form-group">
							  <label>Kabupaten</label>
							  <select name="nazhir_kabupaten" id="nazhir_kabupaten" class="form-control select2" style="width: 100%;">
								
							  </select>
							</div>
							<div class="form-group">
							  <label>Kecamatan</label>
							  <select name="nazhir_kecamatan" id="nazhir_kecamatan" class="form-control select2" style="width: 100%;">
								
							  </select>
							</div>
							<div class="form-group">
							  <label>Kelurahan/Desa</label>
							  <input name="nazhir_kelurahan" id="nazhir_kelurahan" type="text" class="form-control" placeholder="Masukkan Kelurahan/Desa">
							</div>
							<!--
							<div class="form-group">
							  <label>Pekerjaan</label>
							  <input name="nazhir_pekerjaan" id="nazhir_pekerjaan" type="text" class="form-control" placeholder="Masukkan Pekerjaan Wakif">
							</div>
							<div class="form-group">
							  <label>Jabatan</label>
							  <input name="nazhir_jabatan" id="nazhir_jabatan" type="text" class="form-control" placeholder="Masukkan Jabatan dalam Pekerjaan Wakif">
							</div>
							-->
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th>No</th>
							<th align="center">Aksi</th>
							<th>No. KTP</th>
							<th width="150">Nama</th>
							<th>Kecamatan</th>
							<th width="200">Alamat</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data Wakif pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		
		function loadKab(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_nazhir_aksi.php?act=load_kab",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}						
					}else{
						alert_custom(data.msg,"modal-danger");
					}
				}
			});
		}
		
		function loadKec(data_id, wrap_out)
		{
			$('#'+wrap_out).empty();			
			return $.ajax({
				type: "POST",
				url: "m_nazhir_aksi.php?act=load_kec",
				data: { 'data_id': data_id  },
				dataType: "json",
				success: function(data){
					if(data.msg=="OK"){
						// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
						for(var i = 0; i < data.record.length; i++) {
							$('#'+wrap_out).append('<option value="' + data.record[i].id + '">' + data.record[i].name+ '</option>');
						}
					}else{
						alert_custom(data.msg,"modal-danger");
					}						
				}
			});
		}
		
		function initial_select(){					
			var prop = $('#nazhir_propinsi').val();			
			
			$.when(loadKab(prop,"nazhir_kabupaten")).done(function(){
				var kab = $('#nazhir_kabupaten').val();
				loadKec("1101", "nazhir_kecamatan");
			});
		}
		
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{"bSearchable": true}	/* NIP */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */
				,{"sClass": "center"} /* Usergroup */ 	 
							  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "m_nazhir_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
		
		$(document).ready(function() { 		
			
			$('#nazhir_propinsi').on('select2:select', function (e) {
				var data_id = $(this).val();				
				return $.when(
					loadKab(data_id,"nazhir_kabupaten")
				).done(function(){				
					loadKec($("#nazhir_kabupaten").val(),"nazhir_kecamatan");				
				});
			});
			
			$('#nazhir_kabupaten').on('select2:select', function (e) {
				var data_id = $(this).val();
				loadKec(data_id,"nazhir_kecamatan");
			});
			
			initial_select();
						
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_nazhir_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "m_nazhir_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							
							$("#nazhir_id").val(data.record.nazhir_id);
							$("#nazhir_nama").val(data.record.nazhir_nama);
							$("#nazhir_agama").val(data.record.nazhir_agama);
							$("#nazhir_nik").val(data.record.nazhir_nik);
							$("#nazhir_alamat").val(data.record.nazhir_alamat);
							$("#nazhir_tempat_lahir").val(data.record.nazhir_tempat_lahir);
							$("#nazhir_tanggal_lahir").val(data.record.nazhir_tanggal_lahir);
							
							$.when(
								loadKab(data.record.nazhir_propinsi,"nazhir_kabupaten")
							).done(function(){
								$("#nazhir_kabupaten").val(data.record.nazhir_kabupaten);
								$('#nazhir_kabupaten').select2().trigger('change');
								$.when(									
									loadKec(data.record.nazhir_kabupaten,"nazhir_kecamatan")
								).done(function(){
									$("#nazhir_kecamatan").val(data.record.nazhir_kecamatan);
									$('#nazhir_kecamatan').select2().trigger('change');
								});
							});
							
							$("#nazhir_kelurahan").val(data.record.nazhir_kelurahan);
										
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("m_nazhir_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");
								
			});
			
			function clear_form(){
				$("#nazhir_id").val("");
				$("#nazhir_nama").val("");
				$("#nazhir_agama").val("");
				$("#nazhir_nik").val("");
				$("#nazhir_alamat").val("");
				$("#nazhir_tempat_lahir").val("");
				$("#nazhir_tanggal_lahir").val("");
				$("#nazhir_kelurahan").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
		});
	</script>