<?php
    function cek_nik($nik){
        include "config/koneksi_li.php";

        $q = "SELECT * FROM m_pemilik_saham WHERE pemilik_saham_nik = '".$nik."'";
        $sql = mysqli_query($conn_db,$q);
        $num = mysqli_num_rows($sql);
        if($num>0){
            return false;
        }else{
            return true;
        }
    }

    function registrasi($chaptcha_verif){
        include "config/koneksi_li.php";
        include "config/all_function.php";

        $user_password = mysqli_real_escape_string($conn_db,htmlspecialchars(md5($_POST['user_password'])));
        $user_password2 = mysqli_real_escape_string($conn_db,htmlspecialchars(md5($_POST['user_password2'])));
        $user_nik = mysqli_real_escape_string($conn_db,htmlspecialchars($_POST['user_nik']));
        $user_fullname = mysqli_real_escape_string($conn_db,htmlspecialchars($_POST['user_fullname']));
        $user_address = mysqli_real_escape_string($conn_db,htmlspecialchars($_POST['user_address']));
        $user_phone = mysqli_real_escape_string($conn_db,htmlspecialchars($_POST['user_phone']));
        $chaptcha = mysqli_real_escape_string($conn_db,htmlspecialchars($_POST['chaptcha']));
        
        if($chaptcha_verif == $chaptcha){
            if ($user_password <> $user_password2){
                session_start();
                $data['msg'] = "Registrasi Gagal\nPengulangan Password tidak sama";
                $data['status'] = "ERROR";
                return json_encode($data);
            }else{
                if(empty($user_fullname) || empty($user_nik) || empty($user_phone) || empty($user_password) ):
                    $data['msg'] = "Bertanda Bintang Wajib diisi";
                    $data['status'] = "ERROR";
                    return json_encode($data);
                else:
                    if(cek_nik($user_nik)):
                        $gen_id = generate_dateid();
                        $q = "INSERT INTO m_pemilik_saham (pemilik_saham_id,pemilik_saham_nama
                                ,pemilik_saham_nik
                                ,pemilik_saham_hp
                                ,pemilik_saham_alamat
                                ,pemilik_saham_password
                                ,pemilik_saham_created_by) 
                            VALUES('".$gen_id."'
                                ,'".$user_fullname."'
                                ,'".$user_nik."'
                                ,'".$user_phone."'
                                ,'".$user_address."'
                                ,'".$user_password."'
                                ,'public');";
                        
                        $sql = mysqli_query($conn_db,$q);	
                        $data['status'] = "OK";
                        $data['msg'] = "Pendaftaran Sudah diterima";
                        return json_encode($data);
                    else:
                        $data['msg'] = "NIK sudah didaftarkan";
                        $data['status'] = "ERROR";
                        return json_encode($data);
                    endif;
                endif;
            }
        }else{
            $data['msg'] = "Jawaban Aritmetika Anda Salah";
            $data['status'] = "ERROR";
            return json_encode($data);
        }
    }

    $c = $_GET['a'];

    header('Content-Type: application/json; charset=utf-8');
    echo registrasi($c);

?>