<?php
	include("config/datatables_product.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'product_id',
        'product_code',
        'product_name',
		'product_stock',
		'product_price_sale',
		);
	$sIndexColumn = "product_id";
	
	$sQuery = "SELECT
                product_id,
                CONCAT(m_product.product_id,'|',m_product.product_code) as product_code,
                m_product.product_name,
                m_product.product_stock,
                m_product.product_price_sale 
			FROM
			    m_product
			WHERE 1=1 ".$where." ";
	// echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('product_id');
	
	//untuk format
	$sFunctions = array();

	$actions = array(
		'radio'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>