<?php

include "config/koneksi_li.php";
include "config/all_function.php";
error_reporting(E_ALL);

$act = $_GET['act'];

function post_data(){
	include "config/koneksi_li.php";
	
	$digiflazz_host = $_POST['digiflazz_host'];
	$digiflazz_username = $_POST['digiflazz_username'];
	$digiflazz_apikey = $_POST['digiflazz_apikey'];
		
    $q = "UPDATE s_config SET config_value ='".$digiflazz_host."' WHERE config_name = 'digiflazz_host'";
    $sql = mysqli_query($conn_db,$q);

    $q = "UPDATE s_config SET config_value ='".$digiflazz_username."' WHERE config_name = 'digiflazz_username'";
    $sql = mysqli_query($conn_db,$q);

    $q = "UPDATE s_config SET config_value ='".$digiflazz_apikey."' WHERE config_name = 'digiflazz_apikey'";
    $sql = mysqli_query($conn_db,$q);

    $data['msg'] = "OK";
    $data['response'] = "Data Berhasil diupdate";
	
	
	echo json_encode($data);
}

function request_ceksaldo(){
	include "config/koneksi_li.php";

	$end_point 		= "https://api.digiflazz.com/v1/cek-saldo";
	$digi_user 		= get_config_val("digiflazz_username");
	$digi_apikey	= get_config_val("digiflazz_apikey");
	$digi_sign		= md5($digi_user.$digi_apikey."depo");

	$postParameter = json_encode(
		array(
			"cmd" 		=> "deposit",
			"username" 	=> $digi_user,
			"sign"		=> $digi_sign
		)
	);
		
	$curlHandle = curl_init($end_point);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postParameter);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
	
	$curlResponse = curl_exec($curlHandle);
	curl_close($curlHandle);

	echo json_encode($curlResponse);
}

function to_grid(){
	include("config/datatables.php");
	$aColumns = array( 
		'buyer_sku_code',
		'product_name',
		'price',
		'category'
		);
	$sIndexColumn = "buyer_sku_code";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('purchasedetail_id','item_status');
	
	//untuk format
	$sFunctions = array();

	$actions = array(
		'delete_detil'
		);
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
}

function request_pricelist(){
	include "config/koneksi_li.php";

	$end_point 		= "https://api.digiflazz.com/v1/price-list";
	$digi_user 		= get_config_val("digiflazz_username");
	$digi_apikey	= get_config_val("digiflazz_apikey");
	$digi_sign		= md5($digi_user.$digi_apikey."depo");

	$postParameter = json_encode(
		array(
			"cmd" 		=> "prepaid",
			"username" 	=> $digi_user,
			"sign"		=> $digi_sign
		)
	);
		
	$curlHandle = curl_init($end_point);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postParameter);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
	
	$curlResponse = curl_exec($curlHandle);
	curl_close($curlHandle);

	echo json_encode($curlResponse);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM s_user 
			   WHERE USER_ID = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}


if ($act=='save_data'){
	post_data();
}elseif ($act=='request_ceksaldo'){
	request_ceksaldo();
}elseif ($act=='request_listharga'){
	request_pricelist();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>