<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi.php";
	$user_id = $_SESSION['USER_ID'];
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen <?php //echo $r['USER_FULLNAME'];?>
        <small>Pekerjaan harian.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="#">Kerja Harian</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pekerjaan Harian</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah Pekerjaan
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form Kegiatan</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>Tanggal Kegiatan</label>
							  <input name="user_id" id="user_id" type="hidden">
							  <input name="kegiatan_date" id="kegiatan_date" type="text" class="form-control" placeholder="Masukkan Tanggal">
							</div>
							<div class="form-group">
							  <label>Deskripsi Kegiatan</label>
							  <textarea name="kegiatan_description" id="kegiatan_description" cols="5" rows="4" class="form-control" placeholder="Masukkan deskripsi kegiatan">
								</textarea>
							</div>							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i>Add </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
			
			<div class="modal fade" id="modal-upload" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-upload" method="post" action="#" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Upload Dokumen Pendukung</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label for="exampleInputFile">File input</label>
							  <input type="file" name="file_pendukung" id="file_pendukung">
							  <input name="media_id" id="media_id" type="hidden">
							  <p class="help-block">Pilih dokumen pendukung dengan format image atau pdf.</p>
							</div>																			
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit_upload"><i class='fa fa-save'></i>Upload</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
			
			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th width="80">No</th>
							<th width="80" align="center">Aksi</th>
							<th width="100">Tanggal</th>
							<th >Deskripsi</th>
							<th >File Pendukung</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data User pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* no */
				,{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* aksi */
				,{"bSearchable": true}	/* NIP */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* Nama Lengkap */				
				,{"sClass": "center"} /* Usergroup */ 	 
							  
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "kerja_harian_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
		
		function reloadTable(){
			oTable.ajax.reload();
		}
		
		
		$(document).ready(function() { 		
			//Date picker
			$('#kegiatan_date').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "kerja_harian_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
							oTable.ajax.reload();
						},"json");	
				}		
			});
									
			$( "#sample_3 tbody" ).on( "click",".jq-upload", function(e) {
				var id = $(this).attr("lang");
				$("#media_id").val(id);
				$("#modal-upload").modal("show");
			});
						
			$("#btn_submit_upload").click(function(e){
				$("#modal-upload").modal("hide");
				var frm_upload = $('#f-upload');
				frm_upload.submit();
				alert_custom("Loading Process","modal-info");
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-delete-dukung", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus pendukung ini : ")){
					$.post( "kerja_harian_aksi.php?act=delete_dukung", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
							oTable.ajax.reload();
						},"json");
				}		
			});
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("kerja_harian_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");
								
			});
			
			function clear_form(){
				$("#kegiatan_date").val("");
				$("#kegiatan_description").val("");				
			}
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
			});
		});
	</script>
	<style>
		.jq-delete-dukung:hover{
			cursor : pointer;
		}
	</style>
	<script src="./zing_upload.js"></script>
	<!-- bootstrap datepicker -->
	<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	