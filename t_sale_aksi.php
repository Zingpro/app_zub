<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];

function post_data(){	
	include "config/koneksi_li.php";
	session_start();
	
	$sale_id			= mysqli_real_escape_string($conn_db,$_POST['sale_id']);
	$sale_description	= mysqli_real_escape_string($conn_db,$_POST['sale_description']);
	$sale_date			= mysqli_real_escape_string($conn_db,$_POST['sale_date']);
	
	$created_by = $_SESSION['USER_ID'];
    $created_at = date('Y-m-d H:i:s');
	
	if(empty($sale_id)):
        $sale_id = date('YmdHis')."_".rand(1,200);

		$q = "INSERT INTO t_sale (
					sale_id					
					,sale_description
					,sale_status
					,sale_created_at
					,sale_created_by
					,sale_date
				) VALUES (
					'".$sale_id."'
					,'".$sale_description."'				
					,'new'
					,'".$created_at."'
					,'".$created_by."'
					,'".$sale_date."'
				)";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE t_sale SET
					sale_description    = '".$sale_description."'
					,sale_date    = '".$sale_date."'				
				WHERE sale_id   = '".$sale_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah";
	endif;
	
	echo json_encode($data);
}

function check_stock($product_id){
	include "config/koneksi_li.php";
	$q = "SELECT * FROM m_product WHERE product_id = '".$product_id."'";
	$sql = mysqli_query($conn_db,$q);
	$r = mysqli_fetch_array($sql);

	return $r['product_stock'];
}

function addcart(){	
	include "config/koneksi_li.php";
	session_start();
	
	$saledetail_id			= date("YmdHis")."_".rand(1,200);
	$saledetail_sale_id	    = mysqli_real_escape_string($conn_db,$_POST['saledetail_sale_id']);
	$saledetail_product_id	= mysqli_real_escape_string($conn_db,$_POST['saledetail_product_id']);
	$saledetail_count		= mysqli_real_escape_string($conn_db,$_POST['saledetail_count']);
	$saledetail_price		= mysqli_real_escape_string($conn_db,$_POST['saledetail_product_price']);
	$saledetail_description	= mysqli_real_escape_string($conn_db,$_POST['saledetail_description']);
	$saledetail_subtotal	= $saledetail_price * $saledetail_count;
	
	$created_by = $_SESSION['USER_ID'];
    $created_at = date('Y-m-d H:i:s');
	
	if( $saledetail_count < check_stock($saledetail_product_id) ):
		if(!empty($saledetail_sale_id)):
			$saledetail_id = date('YmdHis')."_".rand(1,200);

			$q = "INSERT INTO t_saledetail (
						saledetail_id
						,saledetail_sale_id					
						,saledetail_product_id
						,saledetail_count
						,saledetail_price
						,saledetail_subtotal
						,saledetail_description
					) VALUES (
						'".$saledetail_id."'
						,'".$saledetail_sale_id."'
						,'".$saledetail_product_id."'				
						,'".$saledetail_count."'
						,'".$saledetail_price."'
						,'".$saledetail_subtotal."'
						,'".$saledetail_description."'
					)";
			$sql = mysqli_query($conn_db,$q);
			
			$data['msg'] = "OK";
			$data['response'] = "Data Berhasil ditambahkan";					
		endif;
	else:
		$data['msg'] = "ERROR";
		$data['response'] = "Stock Tidak Tersedia : ".check_stock($saledetail_product_id);
	endif;
	
	$q = "SELECT SUM(saledetail_subtotal) AS saledetail_sum 
			FROM t_saledetail 
			WHERE saledetail_sale_id='".$saledetail_sale_id."'";
	$sql = mysqli_query($conn_db,$q);	
	$rec = mysqli_fetch_array($sql);
	$data['saledetail_sum'] = $rec['saledetail_sum'];

	echo json_encode($data);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM t_saledetail WHERE saledetail_sale_id = '".$data_id."'";
	$sql = mysqli_query($conn_db,$q);
	$num = mysqli_num_rows($sql);

	if($num > 0){
		$data['msg'] = "Maaf sale memiliki detil item";
	}else{
		$q = "DELETE FROM t_sale 
				   WHERE sale_id = '".$data_id."'";		
		$sql = mysqli_query($conn_db,$q);
		$data['msg'] = "Transaksi Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	}

	echo json_encode($data);
}

function data_delete_detail(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];	
	$saledetail_sale_id = $_POST['sale_id'];

	$q = "DELETE FROM t_saledetail 
				WHERE saledetail_id = '".$data_id."'";		
	$sql = mysqli_query($conn_db,$q);

	$q = "SELECT SUM(saledetail_subtotal) AS saledetail_sum 
				FROM t_saledetail 
				WHERE saledetail_sale_id='".$saledetail_sale_id."'";
	$sql = mysqli_query($conn_db,$q);	
	$rec = mysqli_fetch_array($sql);

	$data['msg'] = "OK";
	$data['response'] = "Transaksi Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	$data['saledetail_sum'] = $rec['saledetail_sum'];

	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['sale_id'];
	
	$q = "SELECT * FROM t_sale 
			   WHERE sale_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function cek_ktp($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM t_sale 
				WHERE sale_nik = '".$sale_nik."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

function sale_post(){
	include "config/koneksi_li.php";
	session_start();
	
	$saledetail_sale_id	= mysqli_real_escape_string($conn_db,$_POST['saledetail_sale_id2']);
	$saledetail_payment_amount	= mysqli_real_escape_string($conn_db,$_POST['saledetail_payment_amount']);

	$q = "SELECT SUM(saledetail_subtotal) AS saledetail_sum 
				FROM t_saledetail 
				WHERE saledetail_sale_id='".$saledetail_sale_id."'";
	$sql = mysqli_query($conn_db,$q);	
	$rec = mysqli_fetch_array($sql);
	
	$money_changes = $rec['saledetail_sum'] - $saledetail_payment_amount;

	$q = "UPDATE t_sale SET
				sale_total    = '".$rec['saledetail_sum']."'
				,sale_status    = 'post'
				,sale_payment_amount = '".$saledetail_payment_amount."'	
				,sale_money_changes = '".$money_changes."'				
			WHERE sale_id   = '".$saledetail_sale_id."'";
	// echo $q;
	$sql = mysqli_query($conn_db,$q);

	$data['msg'] = "OK";
	$data['response'] = "Posting Data Berhasil";

	echo json_encode($data);
}

function reset_status(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
		
	$q = "UPDATE t_sale SET sale_status='new' 
				WHERE sale_id = '".$data_id."'";		
	$sql = mysqli_query($conn_db,$q);
	$data['msg'] = "Reset Berhasil : Record modified ".mysqli_affected_rows($conn_db);
	
	echo json_encode($data);
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='delete_detail'){
	data_delete_detail();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='addcart'){
	addcart();
}else if ($act=='sale_post'){
	sale_post();
}else if ($act=='reset_status'){
	reset_status();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>