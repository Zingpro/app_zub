<?php
	include("config/datatables_wakaf.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'wakaf_id',
		'wakaf_niw',
		'wakaf_obyek',
		'wakaf_sertifikat',
		'wakaf_alamat',
		'wakaf_keperluan'
		);
	$sIndexColumn = "wakaf_id";
	
	$sQuery = "SELECT *
			FROM m_wakaf 
			WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('wakaf_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
		
	$actions_report = array(	
		'print_wt1'
		,'print_w2'
		,'print_w2a'		
		);
	
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions,$actions_report);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>