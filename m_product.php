<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen product <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data product.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master product</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data product</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 Tambah product Baru
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form product Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
							  <label>Product Code</label>
							  <input name="product_id" id="product_id" type="hidden">
							  <input name="product_code" id="product_code" type="text" class="form-control" placeholder="Masukkan Kode">
							</div>
							<div class="form-group">
							  <label>Product Name</label>
							  <input name="product_name" id="product_name" type="text" class="form-control" placeholder="Masukkan Nama">
							</div>
							<div class="form-group">
							  <label>Product Description</label>
							  <input name="product_description" id="product_description" type="text" class="form-control" placeholder="Masukkan Deskripsi">
							</div>
							<div class="form-group">
							  <label>Product Price Sale</label>
							  <input name="product_price_sale" id="product_price_sale" type="numeric" class="form-control" value="0">
							</div>
							<div class="form-group">
							  <label>Product Stock</label>
							  <input name="product_stock" id="product_stock" type="text" class="form-control" value="0">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
							<th width="50" >No</th>
							<th width="80" align="center">Aksi</th>
							<th>Code</th>
							<th>Product Name</th>
							<th>Description</th>
							<th width="100">Price</th>
							<th width="80">Stock</th>						
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data product pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->


    </section>
		<div class="modal fade" id="modal-uploadfile" style="display: none;">
			<div class="modal-dialog">
			<div class="modal-content">
				<form id="f-upload" method="post" action="#">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span></button>
						<h4 class="modal-title">Pilih File yang berekstensi (jpg/png/gif) </h4>
					</div>
					<div class="modal-body">							
						<div class="form-group">
							<label>Pilih File</label>
							<input type="file" name="file_pendukung" id="file_pendukung">
							<input type="hidden" name="parent_id" id="parent_id">
						</div>	
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="btn_submit_upload">
							<i class='fa fa-upload'></i> Upload
						</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	<script>
						
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 1. no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 2. aksi */
				,{"bSearchable": true}	/* 3. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 4. */
				,{
					"bSearchable": true,
					"sClass": "center"
				} /* 5. */
				,{"sClass": "text-right"} /* 6. */ 	 
				,{"sClass": "text-right"} /* 7. */ 	 
			],      
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "m_product_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
			
		$(document).ready(function() {

			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				//var label = $(this).attr("lang2");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "m_product_aksi.php?act=delete", { a: id}
						,function( data ){
							alert( "Transaksi Berhasil : Record modified " +data.msg);
						},"json");			
					oTable.ajax.reload();			
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-upload", function(e) {
				var id = $(this).attr("lang");
				$("#parent_id").val(id);
				$("#modal-uploadfile").modal("show");
			});
			

			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "m_product_aksi.php?act=edit", { a: id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");							
							$("#product_id").val(data.record.product_id);
							$("#product_name").val(data.record.product_name);
							$("#product_description").val(data.record.product_description);
							$("#product_price_sale").val(data.record.product_price_sale);
							$("#product_stock").val(data.record.product_stock);
				
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});
			
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("m_product_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");
							
			});
			
			function clear_form(){
				$("#product_id").val("");
				$("#product_nama").val("");
				$("#product_agama").val("");
				$("#product_ktp_nomor").val("");
				$("#product_pekerjaan").val("");
				$("#product_jabatan").val("");
				$("#product_alamat").val("");
				$("#product_tempat_lahir").val("");
				$("#product_tanggal_lahir").val("");
				$("#product_kelurahan").val("");
			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
			$("#btn_submit_upload").click(function(e){	
				var frm_upload = $('#f-upload');
				frm_upload.submit();
				$("#modal-uploadfile").modal("hide");
				alert_custom("Uploading Process","modal-info");
			});
		});
	</script>
	<script src="./zing_upload_product_foto.js"></script>