<?php	
	session_start();
	cek_session();
	error_reporting(E_ALL);
	include "config/koneksi_li.php";
	$user_id = $_SESSION['USER_ID'];
	
?>
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Managemen purchase <?php //echo $r['USER_FULLNAME'];?>
        <small>Pengelolaan Data purchase.</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Master purchase</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
        
        <div class="col-md-4">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
              <?php
                $q = "SELECT SUM(purchase_total) AS total 
							,SUM(purchase_payment_amount) AS payment_amount 
							,SUM(purchase_total - purchase_payment_amount) AS cash_diff 
                    FROM t_purchase";
                $sql = mysqli_query($conn_db,$q);	
                $rec = mysqli_fetch_array($sql);
              ?>
              <sup style="font-size: 16px">Rp. </sup>
              <span id="purchasedetail_sum"><?php echo number_format($rec['total'],2);?></span>              
              </h3>
              <p>Total Purchase</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>            
          </div>
        </div>

		<div class="col-md-4">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>
              <sup style="font-size: 16px">Rp. </sup>
              <span id="saledetail_sum"><?php echo number_format($rec['payment_amount'],2);?></span>              
              </h3>
              <p>Total Payment - Out</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>            
          </div>
        </div>

		<div class="col-md-4">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
              <sup style="font-size: 16px">Rp. </sup>
              <span id="sale_diff"><?php echo number_format($rec['cash_diff'],2);?></span>              
              </h3>
              <p>Total Cash Different</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>            
          </div>
        </div>
                
      </div>
      <!-- /.row -->
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data purchase</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i>
			  </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
			  </button>
          </div>
        </div>
				
        <div class="box-body">
			<div class="row">
				<div class="col-md-3">	
					<button type="button" id="btn_new_purchase" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-default">
						<i class='fa fa-plus'></i>
						 New purchase
					</button>
				</div>
			</div>
			
			<div class="modal fade" id="modal-default" style="display: none;">
			  <div class="modal-dialog">
				<div class="modal-content">
					<form id="f-add" method="post" action="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">×</span></button>
							<h4 class="modal-title">Form purchase Baru</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Purchase Date:</label>
								<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right datepicker" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d'); ?>" name="purchase_date" id="purchase_date">
								</div>
							<!-- /.input group -->
							</div>
							<div class="form-group">
							  <label>Purchase Description</label>
							  <input name="purchase_id" id="purchase_id" type="hidden">
							  <input name="purchase_description" id="purchase_description" type="text" class="form-control" placeholder="Purchase at ...">
							</div>					
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btn_submit"><i class='fa fa-save'></i> Simpan </button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			  </div>
			  <!-- /.modal-dialog -->
			</div>
		
			<div class="row" style="margin-top:10px;">	
				
				<div class="col-md-12">
					<table class="table table-striped table-hover table-bordered" id="sample_3"
							 width="100%" cellspacing="0" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
						  <tr>
						  	<th width="40">No</th>
							<th align="center" width="150">Aksi</th>
							<th width="100">Purchase ID</th>
                            <th width="60">Date</th>
							<th>Description</th>
							<th>Total</th>
							<th>Payment</th>
							<th width="50">Status</th>							
						  </tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>			
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Data purchase pada Sistem 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
		
	<script>
										
		// Tampilkan tabel
		var oTable = $('#sample_3').DataTable( {
			responsive : true,
			//"bJQueryUI": true,				
			"aoColumns": [
				{
					"sClass": "center",
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false
				}/* 1. no */
				,{
					"bSearchable": false,
					"bVisible":true,
					"bSortable":false,
					"sClass": "text-center"
				}/* 2. aksi */
				,{"bSearchable": true}	/* 3. */
				,{
					"bSearchable": true,
					"sClass": "text-center"
				} /* 4. */
				,{
					"bSearchable": true,
					"sClass": "text-left"
				} /* 5. */
				,{"sClass": "text-right"} /* 6. */ 	
				,{"sClass": "text-right"} /* 7. */ 	 
				,{"sClass": "text-center"} /* 8. */  		  
			],   
			"order": [[ 3, "desc" ]],   
			"sPaginationType": "full_numbers",
			"aLengthMenu" : [10,20,30,40],
			"bRetrieve" : true,
			"bStateSave": false,
			"bProcessing": true,
			"bServerSide": true,
			"language": {
				"search": "Pencarian :",
				"sInfoEmpty": "Data Tidak Tersedia",
				"sZeroRecords": "Data Tidak Tersedia",
				"sInfo": "Jumlah Data _TOTAL_ ",
				"sInfoFiltered": " de _MAX_ registros"         
			},
			
			"sAjaxSource": "t_purchase_grid_ajax.php",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
			  aoData.push( { "name": "aksi", "value": "table" } );		  
			  $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			  } );          
			}
		});
			
		$(document).ready(function() { 		
			//Date picker
			$('#purchase_date').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			})

			$( "#sample_3 tbody" ).on( "click",".jq-delete", function(e) {
				var id = $(this).attr("lang");
				if(confirm("Yakin akan menghapus data ini : ")){
					$.post( "t_purchase_aksi.php?act=delete", { a: id}
						,function( data ){
							alert_custom(data.msg,"modal-info");									
							oTable.ajax.reload();
						},"json");				
				}		
			});

			$( "#sample_3 tbody" ).on( "click",".jq-reset", function(e) {
				var id = $(this).attr("lang");
				if(confirm("Yakin akan reset status data ini : ")){
					$.post( "t_purchase_aksi.php?act=reset_status", { a: id}
						,function( data ){
							alert_custom(data.msg,"modal-info");
							oTable.ajax.reload();	
						},"json");							
				}		
			});
			
			$( "#sample_3 tbody" ).on( "click",".jq-edit", function(e) {
				var id = $(this).attr("lang");
				
				$.post( "t_purchase_aksi.php?act=edit", { "purchase_id" : id}
					,function( data ){								
						if(data.msg=="OK"){
							$("#modal-default").modal("show");
							$("#purchase_description").val(data.record.purchase_description);
							$("#purchase_id").val(data.record.purchase_id);
							$("#purchase_date").val(data.record.purchase_date);
							oTable.ajax.reload();
						}else{
							alert_custom(data.msg,"modal-info");
						}
					},"json");				
			});

            $( "#sample_3 tbody" ).on( "click",".jq-detail", function(e) {
				var id = $(this).attr("lang");
				document.location.href = "v_main.php?module=<?php echo md5("t_purchase_detail_trans"); ?>&pid="+id;		
			});			
			
			var frm = $('#f-add');
			
			frm.submit(function (e) {
				e.preventDefault();
				
				var dataform = frm.serializeArray();				
				//data.push({name: 'wordlist', value: wordlist});
				
				alert_custom("Loading Process","modal-info");	
				$.post("t_purchase_aksi.php?act=save_data", dataform, function(data){
					$("#modal-info").modal("hide");
					alert_custom(data.response,"modal-success");
					oTable.ajax.reload();
				},"json");
							
			});
			
			function clear_form(){

			}
			
			$("#modal-default").on("hidden.bs.modal", function () {
				// put your default event here
				clear_form();
			});
			
			$("#btn_submit").click(function(e){
				$("#modal-default").modal("hide");							
				frm.submit();				
				clear_form();
				oTable.ajax.reload();
			});
			
		});
	</script>