<?php

include "config/koneksi_li.php";
include "config/all_function.php";

$act = $_GET['act'];

function post_data(){
	
	include "config/koneksi_li.php";
	session_start();
	
	$pemilik_saham_id				= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_id']);
	$pemilik_saham_nama 			= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_nama']);
	$pemilik_saham_nik 				= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_nik']);
	$pemilik_saham_alamat  			= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_alamat']);
	$pemilik_saham_propinsi			= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_propinsi']);
	$pemilik_saham_kabupaten		= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_kabupaten']);
	$pemilik_saham_kecamatan		= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_kecamatan']);
	$pemilik_saham_kelurahan		= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_kelurahan']);
	$pemilik_saham_hp				= mysqli_real_escape_string($conn_db,$_POST['pemilik_saham_hp']);
	
	$created_by = $_SESSION['USER_ID'];	
	
	if(empty($pemilik_saham_id)):
		$q = "INSERT INTO m_pemilik_saham (
					pemilik_saham_nama					
					,pemilik_saham_nik
					,pemilik_saham_alamat
					,pemilik_saham_propinsi
					,pemilik_saham_kabupaten
					,pemilik_saham_kecamatan
					,pemilik_saham_kelurahan
					,pemilik_saham_created_at
					,pemilik_saham_created_by
				) VALUES (
					'".$pemilik_saham_nama."'
					,'".$pemilik_saham_nik."'				
					,'".$pemilik_saham_alamat."'
					,'".$pemilik_saham_tempat_lahir."'
					,'".$pemilik_saham_tanggal_lahir."'
					,'".$pemilik_saham_propinsi."'
					,'".$pemilik_saham_kabupaten."'
					,'".$pemilik_saham_kecamatan."'
					,'".$pemilik_saham_kelurahan."'
					,'".date('Y-m-d')."'
					,'".$created_by."'
				)";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil ditambahkan";
	else:
		$q = "UPDATE m_pemilik_saham SET
					pemilik_saham_nama 				= '".$pemilik_saham_nama."'
					,pemilik_saham_nik 				= '".$pemilik_saham_nik."'
					,pemilik_saham_alamat 			= '".$pemilik_saham_alamat."'
					,pemilik_saham_propinsi 		= '".$pemilik_saham_propinsi."'
					,pemilik_saham_kabupaten 		= '".$pemilik_saham_kabupaten."'
					,pemilik_saham_kecamatan 		= '".$pemilik_saham_kecamatan."'
					,pemilik_saham_kelurahan 		= '".$pemilik_saham_kelurahan."'	
					,pemilik_saham_hp 				= '".$pemilik_saham_hp."'					
				WHERE pemilik_saham_id 				= '".$pemilik_saham_id."'";
		$sql = mysqli_query($conn_db,$q);
	
		$data['msg'] = "OK";
		$data['response'] = "Data Berhasil dirubah ";
	endif;
	
	echo json_encode($data);
}

function generate_user(){
	include "config/koneksi_li.php";
	$data_id = $_POST['member_id'];
	
	$q = "UPDATE m_pemilik_saham SET pemilik_saham_mobile_active = '1' WHERE pemilik_saham_id = '".$data_id."'";
	$sql = mysqli_query($conn_db,$q);
	
	$end_point 		= "http://kopsyah-ikhmal.com:8212/users";
	$access_token	= get_config_val("admin_jwt_token");

	$headers = array(
		'Authorization: Bearer ' . $access_token,
		'Content-Type: application/json',
	);	

	$postParameter = json_encode(
		array(
			"fullname" 	=> $_POST['mob_fullname'],
			"username" 	=> $_POST['mob_hp'],
			"password"	=> $_POST['mob_hp'],
			"memberId" => $_POST['member_id']
		)
	);		
	//print_r($postParameter);

	$curlHandle = curl_init($end_point);
	curl_setopt($curlHandle, CURLOPT_POST, true);
	curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postParameter);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
	
	$curlResponse = curl_exec($curlHandle);
	curl_close($curlHandle);

	echo json_encode($curlResponse);
}

function data_delete(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM t_saham WHERE saham_pemilik_saham_id = '".$data_id."'";
	$sql = mysqli_query($conn_db,$q);
	$num = mysqli_num_rows($sql);
	if($num > 0){
		$data['msg'] = "Maaf pemilik_saham memiliki saham aktif";
	}else{
		$q = "DELETE FROM m_pemilik_saham 
				   WHERE pemilik_saham_id = '".$data_id."'";		
		$sql = mysqli_query($conn_db,$q);
		$data['msg'] = "Transaksi Berhasil : Record modified ".mysql_affected_rows();
	}
	echo json_encode($data);
}

function data_edit(){
	include "config/koneksi_li.php";
	$data_id = $_POST['a'];
	
	$q = "SELECT * FROM m_pemilik_saham 
			   WHERE pemilik_saham_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = mysqli_fetch_array($sql);
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function load_kab(){
	include "config/koneksi_li.php";
	$data_id = $_POST['data_id'];
	
	$q = "SELECT * FROM d_regencies 
			   WHERE province_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = array();
		
		while($r = mysqli_fetch_array($sql)):
			array_push($data['record'],$r);
		endwhile;
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function load_kec(){
	include "config/koneksi_li.php";
	$data_id = $_POST['data_id'];
	
	$q = "SELECT * FROM d_districts 
			WHERE regency_id = '".$data_id."'";	
	
	$sql = mysqli_query($conn_db,$q);
	
	
	if(mysqli_num_rows($sql)>0):
		$data['msg'] = "OK";
		$data['record'] = array();
		
		while($r = mysqli_fetch_array($sql)):
			array_push($data['record'],$r);
		endwhile;
	else:
		$data['msg'] = "Anything error at fetch data";
	endif;
	
	echo json_encode($data);
}

function cek_ktp($username = null){
	include "config/koneksi_li.php";
	
	$q = "SELECT * FROM m_pemilik_saham 
				WHERE pemilik_saham_nik = '".$pemilik_saham_nik."'";				
	$sql = mysqli_query($conn_db,$q);
	
	$data['num'] = mysqli_num_rows($sql);
	$data['q'] = $q;
	
	return $data;
}

if ($act=='save_data'){
	post_data();
}else if ($act=='delete'){
	data_delete();
}else if ($act=='edit'){
	data_edit();
}else if ($act=='load_kab'){
	load_kab();
}else if ($act=='load_kec'){
	load_kec();
}else if ($act=='genuser'){
	generate_user();
}
else{
	$data = array('msg' => 'Module Tidak Tersedia');
	echo json_encode($data);
}

?>