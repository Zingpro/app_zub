<?php
if(isset($_GET["file"])){
	// Get parameters
    
	$file = urldecode($_GET["file"]); // Decode URL-encoded string
    $filepath = "./uploads/pendukung/".$file;
    
	// Process download
	if(file_exists($filepath)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filepath));
		//ob_clean();
		flush(); // Flush system output buffer
		readfile($filepath);
		//@unlink($filepath);
	}    
	else{
		echo "File does not exist.";
	}
}
?>