<?php
	include("config/datatables_pemilik_saham.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'pemilik_saham_id',
		'pemilik_saham_nik',
		'pemilik_saham_nama',
		'kecamatan_name',
		'pemilik_saham_alamat',
		'pemilik_saham_hp'
		);
	$sIndexColumn = "pemilik_saham_id";
	
	$sQuery = "SELECT 
			m_pemilik_saham.pemilik_saham_id,
			m_pemilik_saham.pemilik_saham_hp,
			m_pemilik_saham.pemilik_saham_nama,
			m_pemilik_saham.pemilik_saham_nik,
			m_pemilik_saham.pemilik_saham_kecamatan,
			m_pemilik_saham.pemilik_saham_alamat,
			d_districts.`name` AS kecamatan_name,
			d_regencies.`name` AS kabupaten_name,
			d_provinces.`name` AS propinsi_name
			FROM
			m_pemilik_saham
			LEFT JOIN d_districts ON d_districts.id = m_pemilik_saham.pemilik_saham_kecamatan
			LEFT JOIN d_regencies ON d_districts.regency_id = d_regencies.id
			LEFT JOIN d_provinces ON d_regencies.province_id = d_provinces.id  
			WHERE 1=1 ".$where." ";
	//echo $sQuery;
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('pemilik_saham_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		);
	array_push($actions,'generate_user');
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	header('Content-Type: application/json');
	echo json_encode($json);
?>