<?php
	include("config/datatables_yayasan.php");
	
	$where = "";
        
	//$cb_bidang = $this->input->post('cb_bidang',true);
	//$where = (empty($cb_bidang))? "" : " AND dokumen_bidang_id='".$cb_bidang."' ";
	
	$aColumns = array( 
		'yayasan_id',
		'yayasan_akta_notaris',
		'yayasan_nama',
		'yayasan_alamat',
		'yayasan_tipe'
		);
	$sIndexColumn = "yayasan_id";
	
	$sQuery = "SELECT
			*
			FROM
			m_yayasan WHERE 1=1 ".$where." ";
	$sTable = "("
			.$sQuery
			. ") as X";
	//$skipCols = array();		
	$skipCols = array('yayasan_id');
	
	//untuk format
	$sFunctions = array(
					'berita_created_date' => "date('d/m/Y',strtotime('%s'));"
				);

	$actions = array(
		'delete'
		,'edit'
		,'pengurus_add'
		,'pengurus_detil'
		,'print_pengesahan'
		);
		
	$grid = new datatables();	
	$grid->params($aColumns,$sIndexColumn,$sTable,$skipCols,$sFunctions,$actions);		
	$json = $grid->build_json();
	//print_r($json);
	echo json_encode($json);
?>