
<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
			<?php
				if(!empty(get_profile('USER_PROFILE_PHOTO'))){
					$foto_profil = "uploads/foto/".get_profile('USER_PROFILE_PHOTO');
				}else{
					$foto_profil = "uploads/foto/default.jpg";
				}
			?>
          <img src="<?php echo $foto_profil; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $r['USER_FULLNAME']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigasi Utama</li>
		<?php if($_SESSION['USERGROUP_ID']=="1"): //usergroup admin ?>
		    <li id="gm_system" class="treeview" >
          <a href="#">
            <i class="fa fa-gears"></i> <span>System</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">			
            <li id="cm_user"><a href="v_main.php?module=<?php echo md5("m_user"); ?>"><i class="fa fa-group"></i> Data Pengguna</a></li>
            <li id="cm_import"><a href="v_main.php?module=<?php echo md5("m_import"); ?>"><i class="fa fa-upload"></i> Import Data</a></li>
            <li id="cm_import"><a href="v_main.php?module=<?php echo md5("m_import"); ?>"><i class="fa fa-download"></i> Export Data</a></li>
            <?php if($_SESSION['USERGROUP_ID']<>"2"): ?>
            <li id="cm_digiflazz"><a href="v_main.php?module=<?php echo md5("s_digiflazz"); ?>"><i class="fa fa-gear"></i> Digiflazz Config</a></li>
            <?php endif; // Menu Admin ?>
          </ul>
        </li>
		<?php endif; // Menu Admin ?>
		
        <li id="gm_dashboard" class="treeview" >
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
			<li id="cm_home"><a href="v_main.php?module=<?php echo md5("home"); ?>"><i class="fa fa-home"></i> Home</a></li>
			<li id="cm_profil" ><a href="v_main.php?module=<?php echo md5("m_user_profil"); ?>"><i class="fa fa-file-text-o"></i> Profil</a></li>			
          </ul>
        </li> 

        <li id="gm_verificator" class="treeview" >
          <a href="#">
            <i class="fa fa-gears"></i> <span>Verificator</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">			
            <li id="cm_verif_deposit"><a href="v_main.php?module=<?php echo md5("verif_deposit"); ?>"><i class="fa fa-money"></i> Verifikasi Deposit</a></li>
            <li id="cm_verif_swajib"><a href="v_main.php?module=<?php echo md5("verif_swajib"); ?>"><i class="fa fa-book"></i> Verifikasi Simpanan Wajib</a></li>
            <li id="cm_verif_pinjaman"><a href="v_main.php?module=<?php echo md5("verif_pinjaman"); ?>"><i class="fa fa-dollar"></i> Verifikasi Pinjaman</a></li>
            <li id="cm_verif_bayarpinjaman"><a href="v_main.php?module=<?php echo md5("verif_bayarpinjaman"); ?>"><i class="fa fa-money"></i> Verifikasi Pembayaran</a></li>
		      </ul>
        </li>

		<li id="gm_datamaster" class="treeview" >
          <a href="#">
            <i class="fa fa-list"></i> <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="cm_product" ><a href="v_main.php?module=<?php echo md5("m_product"); ?>"><i class="fa fa-list-alt"></i> Master Product</a></li>					
            <li id="cm_pemiliksaham"><a href="v_main.php?module=<?php echo md5("m_pemilik_saham"); ?>"><i class="fa fa-google-wallet"></i> Data Anggota</a></li>
            <li id="cm_datasaham">
              <a href="#" ><i class="fa fa-circle-o"></i> Data Saham</a>
            </li>
		      </ul>
    </li>	

    <li id="gm_simpanan" class="treeview" >
          <a href="#">
            <i class="fa fa-money"></i> <span>Simpanan Anggota</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="cm_simpanan_pokok" ><a href="v_main.php?module=<?php echo md5("t_simpanan_pokok"); ?>"><i class="fa fa-suitcase"></i> Simpanan Pokok</a></li>					
            <li id="cm_simpanan_wajib"><a href="v_main.php?module=<?php echo md5("t_simpanan_wajib"); ?>"><i class="fa fa-suitcase"></i> Simpanan Wajib</a></li>
            <li id="cm_simpanan_sukarela">
              <a href="v_main.php?module=<?php echo md5("t_simpanan_sukarela"); ?>">
              <i class="fa fa-suitcase"></i> Simpanan Sukarela</a>
            </li>
		      </ul>
    </li>	
    <li id="gm_piutang" class="treeview">
      <a href="#">
        <i class="fa fa-money"></i>
        <span>Piutang Kopsyah</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="cm_potongan" ><a href="v_main.php?module=<?php echo md5("t_potongan"); ?>"><i class="fa fa-suitcase"></i> Potongan Anggota</a></li>		
      </ul>
    </li>
    <li id="gm_statistik" class="treeview">
      <a href="#">
        <i class="fa fa-pie-chart"></i>
        <span>Statistik</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
              
      </ul>
    </li>
		
        <!-- <li id="cm_trxsaham" class="singlenav">
          <a href="v_main.php?module=<?php echo md5("transaksi_saham"); ?>">
            <i class="fa fa-calendar"></i> <span>Transaksi Saham</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">
				        <i class="fa fa-check"></i>
			        </small>
            </span>
          </a>
        </li> -->

        <li id="cm_sisagaji" class="singlenav">
          <a href="v_main.php?module=<?php echo md5("sisa_gaji"); ?>">
            <i class="fa fa-money"></i> <span>Update Sisa Gaji</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">
				        <i class="fa fa-check"></i>
			        </small>
            </span>
          </a>
        </li>
		
		<li class="header">POS Transaction</li>
		<li id="gm_purchase" class="treeview">
          <a href="#">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Purchase</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo $cm_purchase_transaction; ?> >
				<a href="v_main.php?module=<?php echo md5("t_purchase"); ?>" ><i class="fa fa-circle-o"></i> Purchase Transaction</a>
			</li>			
          </ul>
        </li>

		<li id="gm_sale" class="treeview">
          <a href="#">
            <i class="fa fa-sellsy"></i>
            <span>Sale</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo $cm_sale_transaction; ?> >
				<a href="v_main.php?module=<?php echo md5("t_sale"); ?>" ><i class="fa fa-circle-o"></i> Sale Transaction</a>
			</li>			
          </ul>
        </li>

        <li class="header">Sistem</li>
        <li><a href="logout.php"><i class="fa fa-power-off text-red"></i> <span>Logout</span></a></li>
        <!--
		<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
		-->
	  </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <script>
	$(document).ready(function(){
		
	});
  </script>